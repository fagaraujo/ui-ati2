import type { Meta, StoryObj } from '@storybook/react';

import { SimpleSpan } from '../components/SimpleSpan';

// More on how to set up stories at: https://storybook.js.org/docs/react/writing-stories/introduction
const meta = {
    title: 'Componentes/SimpleSpan',
    
    component: SimpleSpan,
    tags: ['autodocs'],
    argTypes: {
        text: { description: "Texto para exibição no elemento" },
    },
} satisfies Meta<typeof SimpleSpan>;

export default meta;
type Story = StoryObj<typeof meta>;

// More on writing stories with args: https://storybook.js.org/docs/react/writing-stories/args
export const Modelo1: Story = {
    args: {
        text: "Texto personalizado 1",
    },
};

