import type { Meta, StoryObj } from '@storybook/react';

import { SimpleButton } from '../components/SimpleButton';

// More on how to set up stories at: https://storybook.js.org/docs/react/writing-stories/introduction
const meta = {
    title: 'Componentes/SimpleButton',
    component: SimpleButton,
    tags: ['autodocs'],
    argTypes: {
        caption: { description: "Caption para o botão" },
        onClick: { description: "Event Handler do botão" }
    },
} satisfies Meta<typeof SimpleButton>;

export default meta;
type Story = StoryObj<typeof meta>;

// More on writing stories with args: https://storybook.js.org/docs/react/writing-stories/args
export const Modelo1: Story = {
    args: {
        caption: "Modelo 1",
        onClick() {
            alert("Clicado");
        },
    },
};

export const Modelo2: Story = {
    args: {
        caption: "Modelo 2",
        onClick() {
            alert("Clicado");
        },
    },
};
