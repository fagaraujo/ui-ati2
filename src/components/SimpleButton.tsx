import { Button } from "@mui/material";

export interface SimpleButtonProps {
    caption: string;
    onClick?: ()=>void;
}
export const SimpleButton = (props: SimpleButtonProps) => {
    return <Button onClick={props.onClick}>{props.caption}</Button>
}