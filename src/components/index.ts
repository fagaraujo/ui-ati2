export type { SimpleSpanProps } from './SimpleSpan';
export { SimpleSpan } from './SimpleSpan';
export type { SimpleButtonProps } from './SimpleButton';
export { SimpleButton } from './SimpleButton';