export interface SimpleSpanProps {
    text: string;
}
export const SimpleSpan = (props: SimpleSpanProps) => {
    return <span>{props.text}</span>
}