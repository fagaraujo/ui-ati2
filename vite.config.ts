import { resolve } from 'node:path'

import react from '@vitejs/plugin-react'
import { defineConfig } from 'vite'
import dts from 'vite-plugin-dts'
// import EsLint from 'vite-plugin-linter'
// const { EsLinter, linterPlugin } = EsLint
import tsConfigPaths from 'vite-tsconfig-paths'
const packageJson = require('./package.json');

// https://vitejs.dev/config/
export default defineConfig((configEnv) => ({
  plugins: [
    react(),
    tsConfigPaths(),
    // linterPlugin({
    //   include: ['./src}/**/*.{ts,tsx}'],
    //   linters: [new EsLinter({ configEnv })],
    // }),
    dts({
      include: ['src'],
    }),
  ],
  build: {
    lib: {
      entry: resolve("src", 'components/index.ts'),
      name: 'ReactViteLibrary',
      formats: ['es', 'umd'],
      fileName: (format) => `ui-ati2.${format}.js`,
    },
    rollupOptions: {
      external: ["react", "react-dom"],
    },
  },
}))