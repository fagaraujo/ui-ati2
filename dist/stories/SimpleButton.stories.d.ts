import type { StoryObj } from '@storybook/react';
declare const meta: {
    title: string;
    component: (props: import("../components/SimpleButton").SimpleButtonProps) => import("react/jsx-runtime").JSX.Element;
    tags: string[];
    argTypes: {
        caption: {
            description: string;
        };
        onClick: {
            description: string;
        };
    };
};
export default meta;
type Story = StoryObj<typeof meta>;
export declare const Modelo1: Story;
export declare const Modelo2: Story;
