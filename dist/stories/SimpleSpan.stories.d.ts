import type { StoryObj } from '@storybook/react';
declare const meta: {
    title: string;
    component: (props: import("../components/SimpleSpan").SimpleSpanProps) => import("react/jsx-runtime").JSX.Element;
    tags: string[];
    argTypes: {
        text: {
            description: string;
        };
    };
};
export default meta;
type Story = StoryObj<typeof meta>;
export declare const Modelo1: Story;
