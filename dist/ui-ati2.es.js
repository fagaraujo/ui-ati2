import * as P from "react";
import He, { forwardRef as si, useContext as ci, Children as ui, isValidElement as Br, cloneElement as Yr } from "react";
function li(e) {
  return e && e.__esModule && Object.prototype.hasOwnProperty.call(e, "default") ? e.default : e;
}
var $t = { exports: {} }, xr = {};
/**
 * @license React
 * react-jsx-runtime.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var hn;
function fi() {
  if (hn)
    return xr;
  hn = 1;
  var e = He, r = Symbol.for("react.element"), t = Symbol.for("react.fragment"), n = Object.prototype.hasOwnProperty, o = e.__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED.ReactCurrentOwner, i = { key: !0, ref: !0, __self: !0, __source: !0 };
  function a(c, u, f) {
    var d, p = {}, h = null, E = null;
    f !== void 0 && (h = "" + f), u.key !== void 0 && (h = "" + u.key), u.ref !== void 0 && (E = u.ref);
    for (d in u)
      n.call(u, d) && !i.hasOwnProperty(d) && (p[d] = u[d]);
    if (c && c.defaultProps)
      for (d in u = c.defaultProps, u)
        p[d] === void 0 && (p[d] = u[d]);
    return { $$typeof: r, type: c, key: h, ref: E, props: p, _owner: o.current };
  }
  return xr.Fragment = t, xr.jsx = a, xr.jsxs = a, xr;
}
var Tr = {};
/**
 * @license React
 * react-jsx-runtime.development.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var yn;
function di() {
  return yn || (yn = 1, process.env.NODE_ENV !== "production" && function() {
    var e = He, r = Symbol.for("react.element"), t = Symbol.for("react.portal"), n = Symbol.for("react.fragment"), o = Symbol.for("react.strict_mode"), i = Symbol.for("react.profiler"), a = Symbol.for("react.provider"), c = Symbol.for("react.context"), u = Symbol.for("react.forward_ref"), f = Symbol.for("react.suspense"), d = Symbol.for("react.suspense_list"), p = Symbol.for("react.memo"), h = Symbol.for("react.lazy"), E = Symbol.for("react.offscreen"), g = Symbol.iterator, m = "@@iterator";
    function y(s) {
      if (s === null || typeof s != "object")
        return null;
      var T = g && s[g] || s[m];
      return typeof T == "function" ? T : null;
    }
    var S = e.__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED;
    function w(s) {
      {
        for (var T = arguments.length, R = new Array(T > 1 ? T - 1 : 0), V = 1; V < T; V++)
          R[V - 1] = arguments[V];
        C("error", s, R);
      }
    }
    function C(s, T, R) {
      {
        var V = S.ReactDebugCurrentFrame, G = V.getStackAddendum();
        G !== "" && (T += "%s", R = R.concat([G]));
        var ce = R.map(function(K) {
          return String(K);
        });
        ce.unshift("Warning: " + T), Function.prototype.apply.call(console[s], console, ce);
      }
    }
    var _ = !1, l = !1, k = !1, N = !1, pe = !1, ue;
    ue = Symbol.for("react.module.reference");
    function I(s) {
      return !!(typeof s == "string" || typeof s == "function" || s === n || s === i || pe || s === o || s === f || s === d || N || s === E || _ || l || k || typeof s == "object" && s !== null && (s.$$typeof === h || s.$$typeof === p || s.$$typeof === a || s.$$typeof === c || s.$$typeof === u || // This needs to include all possible module reference object
      // types supported by any Flight configuration anywhere since
      // we don't know which Flight build this will end up being used
      // with.
      s.$$typeof === ue || s.getModuleId !== void 0));
    }
    function ae(s, T, R) {
      var V = s.displayName;
      if (V)
        return V;
      var G = T.displayName || T.name || "";
      return G !== "" ? R + "(" + G + ")" : R;
    }
    function me(s) {
      return s.displayName || "Context";
    }
    function oe(s) {
      if (s == null)
        return null;
      if (typeof s.tag == "number" && w("Received an unexpected object in getComponentNameFromType(). This is likely a bug in React. Please file an issue."), typeof s == "function")
        return s.displayName || s.name || null;
      if (typeof s == "string")
        return s;
      switch (s) {
        case n:
          return "Fragment";
        case t:
          return "Portal";
        case i:
          return "Profiler";
        case o:
          return "StrictMode";
        case f:
          return "Suspense";
        case d:
          return "SuspenseList";
      }
      if (typeof s == "object")
        switch (s.$$typeof) {
          case c:
            var T = s;
            return me(T) + ".Consumer";
          case a:
            var R = s;
            return me(R._context) + ".Provider";
          case u:
            return ae(s, s.render, "ForwardRef");
          case p:
            var V = s.displayName || null;
            return V !== null ? V : oe(s.type) || "Memo";
          case h: {
            var G = s, ce = G._payload, K = G._init;
            try {
              return oe(K(ce));
            } catch {
              return null;
            }
          }
        }
      return null;
    }
    var ie = Object.assign, le = 0, se, be, fe, $e, x, O, B;
    function L() {
    }
    L.__reactDisabledLog = !0;
    function A() {
      {
        if (le === 0) {
          se = console.log, be = console.info, fe = console.warn, $e = console.error, x = console.group, O = console.groupCollapsed, B = console.groupEnd;
          var s = {
            configurable: !0,
            enumerable: !0,
            value: L,
            writable: !0
          };
          Object.defineProperties(console, {
            info: s,
            log: s,
            warn: s,
            error: s,
            group: s,
            groupCollapsed: s,
            groupEnd: s
          });
        }
        le++;
      }
    }
    function Y() {
      {
        if (le--, le === 0) {
          var s = {
            configurable: !0,
            enumerable: !0,
            writable: !0
          };
          Object.defineProperties(console, {
            log: ie({}, s, {
              value: se
            }),
            info: ie({}, s, {
              value: be
            }),
            warn: ie({}, s, {
              value: fe
            }),
            error: ie({}, s, {
              value: $e
            }),
            group: ie({}, s, {
              value: x
            }),
            groupCollapsed: ie({}, s, {
              value: O
            }),
            groupEnd: ie({}, s, {
              value: B
            })
          });
        }
        le < 0 && w("disabledDepth fell below zero. This is a bug in React. Please file an issue.");
      }
    }
    var M = S.ReactCurrentDispatcher, F;
    function z(s, T, R) {
      {
        if (F === void 0)
          try {
            throw Error();
          } catch (G) {
            var V = G.stack.trim().match(/\n( *(at )?)/);
            F = V && V[1] || "";
          }
        return `
` + F + s;
      }
    }
    var D = !1, U;
    {
      var he = typeof WeakMap == "function" ? WeakMap : Map;
      U = new he();
    }
    function v(s, T) {
      if (!s || D)
        return "";
      {
        var R = U.get(s);
        if (R !== void 0)
          return R;
      }
      var V;
      D = !0;
      var G = Error.prepareStackTrace;
      Error.prepareStackTrace = void 0;
      var ce;
      ce = M.current, M.current = null, A();
      try {
        if (T) {
          var K = function() {
            throw Error();
          };
          if (Object.defineProperty(K.prototype, "props", {
            set: function() {
              throw Error();
            }
          }), typeof Reflect == "object" && Reflect.construct) {
            try {
              Reflect.construct(K, []);
            } catch (Be) {
              V = Be;
            }
            Reflect.construct(s, [], K);
          } else {
            try {
              K.call();
            } catch (Be) {
              V = Be;
            }
            s.call(K.prototype);
          }
        } else {
          try {
            throw Error();
          } catch (Be) {
            V = Be;
          }
          s();
        }
      } catch (Be) {
        if (Be && V && typeof Be.stack == "string") {
          for (var q = Be.stack.split(`
`), Ce = V.stack.split(`
`), ge = q.length - 1, xe = Ce.length - 1; ge >= 1 && xe >= 0 && q[ge] !== Ce[xe]; )
            xe--;
          for (; ge >= 1 && xe >= 0; ge--, xe--)
            if (q[ge] !== Ce[xe]) {
              if (ge !== 1 || xe !== 1)
                do
                  if (ge--, xe--, xe < 0 || q[ge] !== Ce[xe]) {
                    var Ae = `
` + q[ge].replace(" at new ", " at ");
                    return s.displayName && Ae.includes("<anonymous>") && (Ae = Ae.replace("<anonymous>", s.displayName)), typeof s == "function" && U.set(s, Ae), Ae;
                  }
                while (ge >= 1 && xe >= 0);
              break;
            }
        }
      } finally {
        D = !1, M.current = ce, Y(), Error.prepareStackTrace = G;
      }
      var rr = s ? s.displayName || s.name : "", mn = rr ? z(rr) : "";
      return typeof s == "function" && U.set(s, mn), mn;
    }
    function Ee(s, T, R) {
      return v(s, !1);
    }
    function $(s) {
      var T = s.prototype;
      return !!(T && T.isReactComponent);
    }
    function Se(s, T, R) {
      if (s == null)
        return "";
      if (typeof s == "function")
        return v(s, $(s));
      if (typeof s == "string")
        return z(s);
      switch (s) {
        case f:
          return z("Suspense");
        case d:
          return z("SuspenseList");
      }
      if (typeof s == "object")
        switch (s.$$typeof) {
          case u:
            return Ee(s.render);
          case p:
            return Se(s.type, T, R);
          case h: {
            var V = s, G = V._payload, ce = V._init;
            try {
              return Se(ce(G), T, R);
            } catch {
            }
          }
        }
      return "";
    }
    var De = Object.prototype.hasOwnProperty, Ge = {}, Ir = S.ReactDebugCurrentFrame;
    function Ze(s) {
      if (s) {
        var T = s._owner, R = Se(s.type, s._source, T ? T.type : null);
        Ir.setExtraStackFrame(R);
      } else
        Ir.setExtraStackFrame(null);
    }
    function yr(s, T, R, V, G) {
      {
        var ce = Function.call.bind(De);
        for (var K in s)
          if (ce(s, K)) {
            var q = void 0;
            try {
              if (typeof s[K] != "function") {
                var Ce = Error((V || "React class") + ": " + R + " type `" + K + "` is invalid; it must be a function, usually from the `prop-types` package, but received `" + typeof s[K] + "`.This often happens because of typos such as `PropTypes.function` instead of `PropTypes.func`.");
                throw Ce.name = "Invariant Violation", Ce;
              }
              q = s[K](T, K, V, R, null, "SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED");
            } catch (ge) {
              q = ge;
            }
            q && !(q instanceof Error) && (Ze(G), w("%s: type specification of %s `%s` is invalid; the type checker function must return `null` or an `Error` but returned a %s. You may have forgotten to pass an argument to the type checker creator (arrayOf, instanceOf, objectOf, oneOf, oneOfType, and shape all require an argument).", V || "React class", R, K, typeof q), Ze(null)), q instanceof Error && !(q.message in Ge) && (Ge[q.message] = !0, Ze(G), w("Failed %s type: %s", R, q.message), Ze(null));
          }
      }
    }
    var vr = Array.isArray;
    function gr(s) {
      return vr(s);
    }
    function ht(s) {
      {
        var T = typeof Symbol == "function" && Symbol.toStringTag, R = T && s[Symbol.toStringTag] || s.constructor.name || "Object";
        return R;
      }
    }
    function Qe(s) {
      try {
        return We(s), !1;
      } catch {
        return !0;
      }
    }
    function We(s) {
      return "" + s;
    }
    function Mr(s) {
      if (Qe(s))
        return w("The provided key is an unsupported type %s. This value must be coerced to a string before before using it here.", ht(s)), We(s);
    }
    var qe = S.ReactCurrentOwner, yt = {
      key: !0,
      ref: !0,
      __self: !0,
      __source: !0
    }, W, br, Er;
    Er = {};
    function jr(s) {
      if (De.call(s, "ref")) {
        var T = Object.getOwnPropertyDescriptor(s, "ref").get;
        if (T && T.isReactWarning)
          return !1;
      }
      return s.ref !== void 0;
    }
    function sn(s) {
      if (De.call(s, "key")) {
        var T = Object.getOwnPropertyDescriptor(s, "key").get;
        if (T && T.isReactWarning)
          return !1;
      }
      return s.key !== void 0;
    }
    function Go(s, T) {
      if (typeof s.ref == "string" && qe.current && T && qe.current.stateNode !== T) {
        var R = oe(qe.current.type);
        Er[R] || (w('Component "%s" contains the string ref "%s". Support for string refs will be removed in a future major release. This case cannot be automatically converted to an arrow function. We ask you to manually fix this case by using useRef() or createRef() instead. Learn more about using refs safely here: https://reactjs.org/link/strict-mode-string-ref', oe(qe.current.type), s.ref), Er[R] = !0);
      }
    }
    function Ho(s, T) {
      {
        var R = function() {
          W || (W = !0, w("%s: `key` is not a prop. Trying to access it will result in `undefined` being returned. If you need to access the same value within the child component, you should pass it as a different prop. (https://reactjs.org/link/special-props)", T));
        };
        R.isReactWarning = !0, Object.defineProperty(s, "key", {
          get: R,
          configurable: !0
        });
      }
    }
    function Xo(s, T) {
      {
        var R = function() {
          br || (br = !0, w("%s: `ref` is not a prop. Trying to access it will result in `undefined` being returned. If you need to access the same value within the child component, you should pass it as a different prop. (https://reactjs.org/link/special-props)", T));
        };
        R.isReactWarning = !0, Object.defineProperty(s, "ref", {
          get: R,
          configurable: !0
        });
      }
    }
    var Jo = function(s, T, R, V, G, ce, K) {
      var q = {
        // This tag allows us to uniquely identify this as a React Element
        $$typeof: r,
        // Built-in properties that belong on the element
        type: s,
        key: T,
        ref: R,
        props: K,
        // Record the component responsible for creating this element.
        _owner: ce
      };
      return q._store = {}, Object.defineProperty(q._store, "validated", {
        configurable: !1,
        enumerable: !1,
        writable: !0,
        value: !1
      }), Object.defineProperty(q, "_self", {
        configurable: !1,
        enumerable: !1,
        writable: !1,
        value: V
      }), Object.defineProperty(q, "_source", {
        configurable: !1,
        enumerable: !1,
        writable: !1,
        value: G
      }), Object.freeze && (Object.freeze(q.props), Object.freeze(q)), q;
    };
    function Zo(s, T, R, V, G) {
      {
        var ce, K = {}, q = null, Ce = null;
        R !== void 0 && (Mr(R), q = "" + R), sn(T) && (Mr(T.key), q = "" + T.key), jr(T) && (Ce = T.ref, Go(T, G));
        for (ce in T)
          De.call(T, ce) && !yt.hasOwnProperty(ce) && (K[ce] = T[ce]);
        if (s && s.defaultProps) {
          var ge = s.defaultProps;
          for (ce in ge)
            K[ce] === void 0 && (K[ce] = ge[ce]);
        }
        if (q || Ce) {
          var xe = typeof s == "function" ? s.displayName || s.name || "Unknown" : s;
          q && Ho(K, xe), Ce && Xo(K, xe);
        }
        return Jo(s, q, Ce, G, V, qe.current, K);
      }
    }
    var vt = S.ReactCurrentOwner, cn = S.ReactDebugCurrentFrame;
    function er(s) {
      if (s) {
        var T = s._owner, R = Se(s.type, s._source, T ? T.type : null);
        cn.setExtraStackFrame(R);
      } else
        cn.setExtraStackFrame(null);
    }
    var gt;
    gt = !1;
    function bt(s) {
      return typeof s == "object" && s !== null && s.$$typeof === r;
    }
    function un() {
      {
        if (vt.current) {
          var s = oe(vt.current.type);
          if (s)
            return `

Check the render method of \`` + s + "`.";
        }
        return "";
      }
    }
    function Qo(s) {
      {
        if (s !== void 0) {
          var T = s.fileName.replace(/^.*[\\\/]/, ""), R = s.lineNumber;
          return `

Check your code at ` + T + ":" + R + ".";
        }
        return "";
      }
    }
    var ln = {};
    function ei(s) {
      {
        var T = un();
        if (!T) {
          var R = typeof s == "string" ? s : s.displayName || s.name;
          R && (T = `

Check the top-level render call using <` + R + ">.");
        }
        return T;
      }
    }
    function fn(s, T) {
      {
        if (!s._store || s._store.validated || s.key != null)
          return;
        s._store.validated = !0;
        var R = ei(T);
        if (ln[R])
          return;
        ln[R] = !0;
        var V = "";
        s && s._owner && s._owner !== vt.current && (V = " It was passed a child from " + oe(s._owner.type) + "."), er(s), w('Each child in a list should have a unique "key" prop.%s%s See https://reactjs.org/link/warning-keys for more information.', R, V), er(null);
      }
    }
    function dn(s, T) {
      {
        if (typeof s != "object")
          return;
        if (gr(s))
          for (var R = 0; R < s.length; R++) {
            var V = s[R];
            bt(V) && fn(V, T);
          }
        else if (bt(s))
          s._store && (s._store.validated = !0);
        else if (s) {
          var G = y(s);
          if (typeof G == "function" && G !== s.entries)
            for (var ce = G.call(s), K; !(K = ce.next()).done; )
              bt(K.value) && fn(K.value, T);
        }
      }
    }
    function ri(s) {
      {
        var T = s.type;
        if (T == null || typeof T == "string")
          return;
        var R;
        if (typeof T == "function")
          R = T.propTypes;
        else if (typeof T == "object" && (T.$$typeof === u || // Note: Memo only checks outer props here.
        // Inner props are checked in the reconciler.
        T.$$typeof === p))
          R = T.propTypes;
        else
          return;
        if (R) {
          var V = oe(T);
          yr(R, s.props, "prop", V, s);
        } else if (T.PropTypes !== void 0 && !gt) {
          gt = !0;
          var G = oe(T);
          w("Component %s declared `PropTypes` instead of `propTypes`. Did you misspell the property assignment?", G || "Unknown");
        }
        typeof T.getDefaultProps == "function" && !T.getDefaultProps.isReactClassApproved && w("getDefaultProps is only used on classic React.createClass definitions. Use a static property named `defaultProps` instead.");
      }
    }
    function ti(s) {
      {
        for (var T = Object.keys(s.props), R = 0; R < T.length; R++) {
          var V = T[R];
          if (V !== "children" && V !== "key") {
            er(s), w("Invalid prop `%s` supplied to `React.Fragment`. React.Fragment can only have `key` and `children` props.", V), er(null);
            break;
          }
        }
        s.ref !== null && (er(s), w("Invalid attribute `ref` supplied to `React.Fragment`."), er(null));
      }
    }
    function pn(s, T, R, V, G, ce) {
      {
        var K = I(s);
        if (!K) {
          var q = "";
          (s === void 0 || typeof s == "object" && s !== null && Object.keys(s).length === 0) && (q += " You likely forgot to export your component from the file it's defined in, or you might have mixed up default and named imports.");
          var Ce = Qo(G);
          Ce ? q += Ce : q += un();
          var ge;
          s === null ? ge = "null" : gr(s) ? ge = "array" : s !== void 0 && s.$$typeof === r ? (ge = "<" + (oe(s.type) || "Unknown") + " />", q = " Did you accidentally export a JSX literal instead of a component?") : ge = typeof s, w("React.jsx: type is invalid -- expected a string (for built-in components) or a class/function (for composite components) but got: %s.%s", ge, q);
        }
        var xe = Zo(s, T, R, G, ce);
        if (xe == null)
          return xe;
        if (K) {
          var Ae = T.children;
          if (Ae !== void 0)
            if (V)
              if (gr(Ae)) {
                for (var rr = 0; rr < Ae.length; rr++)
                  dn(Ae[rr], s);
                Object.freeze && Object.freeze(Ae);
              } else
                w("React.jsx: Static children should always be an array. You are likely explicitly calling React.jsxs or React.jsxDEV. Use the Babel transform instead.");
            else
              dn(Ae, s);
        }
        return s === n ? ti(xe) : ri(xe), xe;
      }
    }
    function ni(s, T, R) {
      return pn(s, T, R, !0);
    }
    function oi(s, T, R) {
      return pn(s, T, R, !1);
    }
    var ii = oi, ai = ni;
    Tr.Fragment = n, Tr.jsx = ii, Tr.jsxs = ai;
  }()), Tr;
}
process.env.NODE_ENV === "production" ? $t.exports = fi() : $t.exports = di();
var Me = $t.exports;
const Yu = (e) => /* @__PURE__ */ Me.jsx("span", { children: e.text }), pi = {
  black: "#000",
  white: "#fff"
}, Or = pi, mi = {
  50: "#ffebee",
  100: "#ffcdd2",
  200: "#ef9a9a",
  300: "#e57373",
  400: "#ef5350",
  500: "#f44336",
  600: "#e53935",
  700: "#d32f2f",
  800: "#c62828",
  900: "#b71c1c",
  A100: "#ff8a80",
  A200: "#ff5252",
  A400: "#ff1744",
  A700: "#d50000"
}, tr = mi, hi = {
  50: "#f3e5f5",
  100: "#e1bee7",
  200: "#ce93d8",
  300: "#ba68c8",
  400: "#ab47bc",
  500: "#9c27b0",
  600: "#8e24aa",
  700: "#7b1fa2",
  800: "#6a1b9a",
  900: "#4a148c",
  A100: "#ea80fc",
  A200: "#e040fb",
  A400: "#d500f9",
  A700: "#aa00ff"
}, nr = hi, yi = {
  50: "#e3f2fd",
  100: "#bbdefb",
  200: "#90caf9",
  300: "#64b5f6",
  400: "#42a5f5",
  500: "#2196f3",
  600: "#1e88e5",
  700: "#1976d2",
  800: "#1565c0",
  900: "#0d47a1",
  A100: "#82b1ff",
  A200: "#448aff",
  A400: "#2979ff",
  A700: "#2962ff"
}, or = yi, gi = {
  50: "#e1f5fe",
  100: "#b3e5fc",
  200: "#81d4fa",
  300: "#4fc3f7",
  400: "#29b6f6",
  500: "#03a9f4",
  600: "#039be5",
  700: "#0288d1",
  800: "#0277bd",
  900: "#01579b",
  A100: "#80d8ff",
  A200: "#40c4ff",
  A400: "#00b0ff",
  A700: "#0091ea"
}, ir = gi, bi = {
  50: "#e8f5e9",
  100: "#c8e6c9",
  200: "#a5d6a7",
  300: "#81c784",
  400: "#66bb6a",
  500: "#4caf50",
  600: "#43a047",
  700: "#388e3c",
  800: "#2e7d32",
  900: "#1b5e20",
  A100: "#b9f6ca",
  A200: "#69f0ae",
  A400: "#00e676",
  A700: "#00c853"
}, ar = bi, Ei = {
  50: "#fff3e0",
  100: "#ffe0b2",
  200: "#ffcc80",
  300: "#ffb74d",
  400: "#ffa726",
  500: "#ff9800",
  600: "#fb8c00",
  700: "#f57c00",
  800: "#ef6c00",
  900: "#e65100",
  A100: "#ffd180",
  A200: "#ffab40",
  A400: "#ff9100",
  A700: "#ff6d00"
}, _r = Ei, xi = {
  50: "#fafafa",
  100: "#f5f5f5",
  200: "#eeeeee",
  300: "#e0e0e0",
  400: "#bdbdbd",
  500: "#9e9e9e",
  600: "#757575",
  700: "#616161",
  800: "#424242",
  900: "#212121",
  A100: "#f5f5f5",
  A200: "#eeeeee",
  A400: "#bdbdbd",
  A700: "#616161"
}, Ti = xi;
function _i(e, r) {
  return process.env.NODE_ENV === "production" ? () => null : function(...n) {
    return e(...n) || r(...n);
  };
}
function j() {
  return j = Object.assign ? Object.assign.bind() : function(e) {
    for (var r = 1; r < arguments.length; r++) {
      var t = arguments[r];
      for (var n in t)
        Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
    }
    return e;
  }, j.apply(this, arguments);
}
function sr(e) {
  return e !== null && typeof e == "object" && e.constructor === Object;
}
function ho(e) {
  if (!sr(e))
    return e;
  const r = {};
  return Object.keys(e).forEach((t) => {
    r[t] = ho(e[t]);
  }), r;
}
function Ye(e, r, t = {
  clone: !0
}) {
  const n = t.clone ? j({}, e) : e;
  return sr(e) && sr(r) && Object.keys(r).forEach((o) => {
    o !== "__proto__" && (sr(r[o]) && o in e && sr(e[o]) ? n[o] = Ye(e[o], r[o], t) : t.clone ? n[o] = sr(r[o]) ? ho(r[o]) : r[o] : n[o] = r[o]);
  }), n;
}
var Pt = { exports: {} }, Dr = { exports: {} }, J = {};
/** @license React v16.13.1
 * react-is.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var vn;
function Si() {
  if (vn)
    return J;
  vn = 1;
  var e = typeof Symbol == "function" && Symbol.for, r = e ? Symbol.for("react.element") : 60103, t = e ? Symbol.for("react.portal") : 60106, n = e ? Symbol.for("react.fragment") : 60107, o = e ? Symbol.for("react.strict_mode") : 60108, i = e ? Symbol.for("react.profiler") : 60114, a = e ? Symbol.for("react.provider") : 60109, c = e ? Symbol.for("react.context") : 60110, u = e ? Symbol.for("react.async_mode") : 60111, f = e ? Symbol.for("react.concurrent_mode") : 60111, d = e ? Symbol.for("react.forward_ref") : 60112, p = e ? Symbol.for("react.suspense") : 60113, h = e ? Symbol.for("react.suspense_list") : 60120, E = e ? Symbol.for("react.memo") : 60115, g = e ? Symbol.for("react.lazy") : 60116, m = e ? Symbol.for("react.block") : 60121, y = e ? Symbol.for("react.fundamental") : 60117, S = e ? Symbol.for("react.responder") : 60118, w = e ? Symbol.for("react.scope") : 60119;
  function C(l) {
    if (typeof l == "object" && l !== null) {
      var k = l.$$typeof;
      switch (k) {
        case r:
          switch (l = l.type, l) {
            case u:
            case f:
            case n:
            case i:
            case o:
            case p:
              return l;
            default:
              switch (l = l && l.$$typeof, l) {
                case c:
                case d:
                case g:
                case E:
                case a:
                  return l;
                default:
                  return k;
              }
          }
        case t:
          return k;
      }
    }
  }
  function _(l) {
    return C(l) === f;
  }
  return J.AsyncMode = u, J.ConcurrentMode = f, J.ContextConsumer = c, J.ContextProvider = a, J.Element = r, J.ForwardRef = d, J.Fragment = n, J.Lazy = g, J.Memo = E, J.Portal = t, J.Profiler = i, J.StrictMode = o, J.Suspense = p, J.isAsyncMode = function(l) {
    return _(l) || C(l) === u;
  }, J.isConcurrentMode = _, J.isContextConsumer = function(l) {
    return C(l) === c;
  }, J.isContextProvider = function(l) {
    return C(l) === a;
  }, J.isElement = function(l) {
    return typeof l == "object" && l !== null && l.$$typeof === r;
  }, J.isForwardRef = function(l) {
    return C(l) === d;
  }, J.isFragment = function(l) {
    return C(l) === n;
  }, J.isLazy = function(l) {
    return C(l) === g;
  }, J.isMemo = function(l) {
    return C(l) === E;
  }, J.isPortal = function(l) {
    return C(l) === t;
  }, J.isProfiler = function(l) {
    return C(l) === i;
  }, J.isStrictMode = function(l) {
    return C(l) === o;
  }, J.isSuspense = function(l) {
    return C(l) === p;
  }, J.isValidElementType = function(l) {
    return typeof l == "string" || typeof l == "function" || l === n || l === f || l === i || l === o || l === p || l === h || typeof l == "object" && l !== null && (l.$$typeof === g || l.$$typeof === E || l.$$typeof === a || l.$$typeof === c || l.$$typeof === d || l.$$typeof === y || l.$$typeof === S || l.$$typeof === w || l.$$typeof === m);
  }, J.typeOf = C, J;
}
var Z = {};
/** @license React v16.13.1
 * react-is.development.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var gn;
function Ri() {
  return gn || (gn = 1, process.env.NODE_ENV !== "production" && function() {
    var e = typeof Symbol == "function" && Symbol.for, r = e ? Symbol.for("react.element") : 60103, t = e ? Symbol.for("react.portal") : 60106, n = e ? Symbol.for("react.fragment") : 60107, o = e ? Symbol.for("react.strict_mode") : 60108, i = e ? Symbol.for("react.profiler") : 60114, a = e ? Symbol.for("react.provider") : 60109, c = e ? Symbol.for("react.context") : 60110, u = e ? Symbol.for("react.async_mode") : 60111, f = e ? Symbol.for("react.concurrent_mode") : 60111, d = e ? Symbol.for("react.forward_ref") : 60112, p = e ? Symbol.for("react.suspense") : 60113, h = e ? Symbol.for("react.suspense_list") : 60120, E = e ? Symbol.for("react.memo") : 60115, g = e ? Symbol.for("react.lazy") : 60116, m = e ? Symbol.for("react.block") : 60121, y = e ? Symbol.for("react.fundamental") : 60117, S = e ? Symbol.for("react.responder") : 60118, w = e ? Symbol.for("react.scope") : 60119;
    function C(v) {
      return typeof v == "string" || typeof v == "function" || // Note: its typeof might be other than 'symbol' or 'number' if it's a polyfill.
      v === n || v === f || v === i || v === o || v === p || v === h || typeof v == "object" && v !== null && (v.$$typeof === g || v.$$typeof === E || v.$$typeof === a || v.$$typeof === c || v.$$typeof === d || v.$$typeof === y || v.$$typeof === S || v.$$typeof === w || v.$$typeof === m);
    }
    function _(v) {
      if (typeof v == "object" && v !== null) {
        var Ee = v.$$typeof;
        switch (Ee) {
          case r:
            var $ = v.type;
            switch ($) {
              case u:
              case f:
              case n:
              case i:
              case o:
              case p:
                return $;
              default:
                var Se = $ && $.$$typeof;
                switch (Se) {
                  case c:
                  case d:
                  case g:
                  case E:
                  case a:
                    return Se;
                  default:
                    return Ee;
                }
            }
          case t:
            return Ee;
        }
      }
    }
    var l = u, k = f, N = c, pe = a, ue = r, I = d, ae = n, me = g, oe = E, ie = t, le = i, se = o, be = p, fe = !1;
    function $e(v) {
      return fe || (fe = !0, console.warn("The ReactIs.isAsyncMode() alias has been deprecated, and will be removed in React 17+. Update your code to use ReactIs.isConcurrentMode() instead. It has the exact same API.")), x(v) || _(v) === u;
    }
    function x(v) {
      return _(v) === f;
    }
    function O(v) {
      return _(v) === c;
    }
    function B(v) {
      return _(v) === a;
    }
    function L(v) {
      return typeof v == "object" && v !== null && v.$$typeof === r;
    }
    function A(v) {
      return _(v) === d;
    }
    function Y(v) {
      return _(v) === n;
    }
    function M(v) {
      return _(v) === g;
    }
    function F(v) {
      return _(v) === E;
    }
    function z(v) {
      return _(v) === t;
    }
    function D(v) {
      return _(v) === i;
    }
    function U(v) {
      return _(v) === o;
    }
    function he(v) {
      return _(v) === p;
    }
    Z.AsyncMode = l, Z.ConcurrentMode = k, Z.ContextConsumer = N, Z.ContextProvider = pe, Z.Element = ue, Z.ForwardRef = I, Z.Fragment = ae, Z.Lazy = me, Z.Memo = oe, Z.Portal = ie, Z.Profiler = le, Z.StrictMode = se, Z.Suspense = be, Z.isAsyncMode = $e, Z.isConcurrentMode = x, Z.isContextConsumer = O, Z.isContextProvider = B, Z.isElement = L, Z.isForwardRef = A, Z.isFragment = Y, Z.isLazy = M, Z.isMemo = F, Z.isPortal = z, Z.isProfiler = D, Z.isStrictMode = U, Z.isSuspense = he, Z.isValidElementType = C, Z.typeOf = _;
  }()), Z;
}
var bn;
function yo() {
  return bn || (bn = 1, process.env.NODE_ENV === "production" ? Dr.exports = Si() : Dr.exports = Ri()), Dr.exports;
}
/*
object-assign
(c) Sindre Sorhus
@license MIT
*/
var Et, En;
function Ci() {
  if (En)
    return Et;
  En = 1;
  var e = Object.getOwnPropertySymbols, r = Object.prototype.hasOwnProperty, t = Object.prototype.propertyIsEnumerable;
  function n(i) {
    if (i == null)
      throw new TypeError("Object.assign cannot be called with null or undefined");
    return Object(i);
  }
  function o() {
    try {
      if (!Object.assign)
        return !1;
      var i = new String("abc");
      if (i[5] = "de", Object.getOwnPropertyNames(i)[0] === "5")
        return !1;
      for (var a = {}, c = 0; c < 10; c++)
        a["_" + String.fromCharCode(c)] = c;
      var u = Object.getOwnPropertyNames(a).map(function(d) {
        return a[d];
      });
      if (u.join("") !== "0123456789")
        return !1;
      var f = {};
      return "abcdefghijklmnopqrst".split("").forEach(function(d) {
        f[d] = d;
      }), Object.keys(Object.assign({}, f)).join("") === "abcdefghijklmnopqrst";
    } catch {
      return !1;
    }
  }
  return Et = o() ? Object.assign : function(i, a) {
    for (var c, u = n(i), f, d = 1; d < arguments.length; d++) {
      c = Object(arguments[d]);
      for (var p in c)
        r.call(c, p) && (u[p] = c[p]);
      if (e) {
        f = e(c);
        for (var h = 0; h < f.length; h++)
          t.call(c, f[h]) && (u[f[h]] = c[f[h]]);
      }
    }
    return u;
  }, Et;
}
var xt, xn;
function Vt() {
  if (xn)
    return xt;
  xn = 1;
  var e = "SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED";
  return xt = e, xt;
}
var Tt, Tn;
function vo() {
  return Tn || (Tn = 1, Tt = Function.call.bind(Object.prototype.hasOwnProperty)), Tt;
}
var _t, _n;
function Oi() {
  if (_n)
    return _t;
  _n = 1;
  var e = function() {
  };
  if (process.env.NODE_ENV !== "production") {
    var r = Vt(), t = {}, n = vo();
    e = function(i) {
      var a = "Warning: " + i;
      typeof console < "u" && console.error(a);
      try {
        throw new Error(a);
      } catch {
      }
    };
  }
  function o(i, a, c, u, f) {
    if (process.env.NODE_ENV !== "production") {
      for (var d in i)
        if (n(i, d)) {
          var p;
          try {
            if (typeof i[d] != "function") {
              var h = Error(
                (u || "React class") + ": " + c + " type `" + d + "` is invalid; it must be a function, usually from the `prop-types` package, but received `" + typeof i[d] + "`.This often happens because of typos such as `PropTypes.function` instead of `PropTypes.func`."
              );
              throw h.name = "Invariant Violation", h;
            }
            p = i[d](a, d, u, c, null, r);
          } catch (g) {
            p = g;
          }
          if (p && !(p instanceof Error) && e(
            (u || "React class") + ": type specification of " + c + " `" + d + "` is invalid; the type checker function must return `null` or an `Error` but returned a " + typeof p + ". You may have forgotten to pass an argument to the type checker creator (arrayOf, instanceOf, objectOf, oneOf, oneOfType, and shape all require an argument)."
          ), p instanceof Error && !(p.message in t)) {
            t[p.message] = !0;
            var E = f ? f() : "";
            e(
              "Failed " + c + " type: " + p.message + (E ?? "")
            );
          }
        }
    }
  }
  return o.resetWarningCache = function() {
    process.env.NODE_ENV !== "production" && (t = {});
  }, _t = o, _t;
}
var St, Sn;
function wi() {
  if (Sn)
    return St;
  Sn = 1;
  var e = yo(), r = Ci(), t = Vt(), n = vo(), o = Oi(), i = function() {
  };
  process.env.NODE_ENV !== "production" && (i = function(c) {
    var u = "Warning: " + c;
    typeof console < "u" && console.error(u);
    try {
      throw new Error(u);
    } catch {
    }
  });
  function a() {
    return null;
  }
  return St = function(c, u) {
    var f = typeof Symbol == "function" && Symbol.iterator, d = "@@iterator";
    function p(x) {
      var O = x && (f && x[f] || x[d]);
      if (typeof O == "function")
        return O;
    }
    var h = "<<anonymous>>", E = {
      array: S("array"),
      bigint: S("bigint"),
      bool: S("boolean"),
      func: S("function"),
      number: S("number"),
      object: S("object"),
      string: S("string"),
      symbol: S("symbol"),
      any: w(),
      arrayOf: C,
      element: _(),
      elementType: l(),
      instanceOf: k,
      node: I(),
      objectOf: pe,
      oneOf: N,
      oneOfType: ue,
      shape: me,
      exact: oe
    };
    function g(x, O) {
      return x === O ? x !== 0 || 1 / x === 1 / O : x !== x && O !== O;
    }
    function m(x, O) {
      this.message = x, this.data = O && typeof O == "object" ? O : {}, this.stack = "";
    }
    m.prototype = Error.prototype;
    function y(x) {
      if (process.env.NODE_ENV !== "production")
        var O = {}, B = 0;
      function L(Y, M, F, z, D, U, he) {
        if (z = z || h, U = U || F, he !== t) {
          if (u) {
            var v = new Error(
              "Calling PropTypes validators directly is not supported by the `prop-types` package. Use `PropTypes.checkPropTypes()` to call them. Read more at http://fb.me/use-check-prop-types"
            );
            throw v.name = "Invariant Violation", v;
          } else if (process.env.NODE_ENV !== "production" && typeof console < "u") {
            var Ee = z + ":" + F;
            !O[Ee] && // Avoid spamming the console because they are often not actionable except for lib authors
            B < 3 && (i(
              "You are manually calling a React.PropTypes validation function for the `" + U + "` prop on `" + z + "`. This is deprecated and will throw in the standalone `prop-types` package. You may be seeing this warning due to a third-party PropTypes library. See https://fb.me/react-warning-dont-call-proptypes for details."
            ), O[Ee] = !0, B++);
          }
        }
        return M[F] == null ? Y ? M[F] === null ? new m("The " + D + " `" + U + "` is marked as required " + ("in `" + z + "`, but its value is `null`.")) : new m("The " + D + " `" + U + "` is marked as required in " + ("`" + z + "`, but its value is `undefined`.")) : null : x(M, F, z, D, U);
      }
      var A = L.bind(null, !1);
      return A.isRequired = L.bind(null, !0), A;
    }
    function S(x) {
      function O(B, L, A, Y, M, F) {
        var z = B[L], D = se(z);
        if (D !== x) {
          var U = be(z);
          return new m(
            "Invalid " + Y + " `" + M + "` of type " + ("`" + U + "` supplied to `" + A + "`, expected ") + ("`" + x + "`."),
            { expectedType: x }
          );
        }
        return null;
      }
      return y(O);
    }
    function w() {
      return y(a);
    }
    function C(x) {
      function O(B, L, A, Y, M) {
        if (typeof x != "function")
          return new m("Property `" + M + "` of component `" + A + "` has invalid PropType notation inside arrayOf.");
        var F = B[L];
        if (!Array.isArray(F)) {
          var z = se(F);
          return new m("Invalid " + Y + " `" + M + "` of type " + ("`" + z + "` supplied to `" + A + "`, expected an array."));
        }
        for (var D = 0; D < F.length; D++) {
          var U = x(F, D, A, Y, M + "[" + D + "]", t);
          if (U instanceof Error)
            return U;
        }
        return null;
      }
      return y(O);
    }
    function _() {
      function x(O, B, L, A, Y) {
        var M = O[B];
        if (!c(M)) {
          var F = se(M);
          return new m("Invalid " + A + " `" + Y + "` of type " + ("`" + F + "` supplied to `" + L + "`, expected a single ReactElement."));
        }
        return null;
      }
      return y(x);
    }
    function l() {
      function x(O, B, L, A, Y) {
        var M = O[B];
        if (!e.isValidElementType(M)) {
          var F = se(M);
          return new m("Invalid " + A + " `" + Y + "` of type " + ("`" + F + "` supplied to `" + L + "`, expected a single ReactElement type."));
        }
        return null;
      }
      return y(x);
    }
    function k(x) {
      function O(B, L, A, Y, M) {
        if (!(B[L] instanceof x)) {
          var F = x.name || h, z = $e(B[L]);
          return new m("Invalid " + Y + " `" + M + "` of type " + ("`" + z + "` supplied to `" + A + "`, expected ") + ("instance of `" + F + "`."));
        }
        return null;
      }
      return y(O);
    }
    function N(x) {
      if (!Array.isArray(x))
        return process.env.NODE_ENV !== "production" && (arguments.length > 1 ? i(
          "Invalid arguments supplied to oneOf, expected an array, got " + arguments.length + " arguments. A common mistake is to write oneOf(x, y, z) instead of oneOf([x, y, z])."
        ) : i("Invalid argument supplied to oneOf, expected an array.")), a;
      function O(B, L, A, Y, M) {
        for (var F = B[L], z = 0; z < x.length; z++)
          if (g(F, x[z]))
            return null;
        var D = JSON.stringify(x, function(he, v) {
          var Ee = be(v);
          return Ee === "symbol" ? String(v) : v;
        });
        return new m("Invalid " + Y + " `" + M + "` of value `" + String(F) + "` " + ("supplied to `" + A + "`, expected one of " + D + "."));
      }
      return y(O);
    }
    function pe(x) {
      function O(B, L, A, Y, M) {
        if (typeof x != "function")
          return new m("Property `" + M + "` of component `" + A + "` has invalid PropType notation inside objectOf.");
        var F = B[L], z = se(F);
        if (z !== "object")
          return new m("Invalid " + Y + " `" + M + "` of type " + ("`" + z + "` supplied to `" + A + "`, expected an object."));
        for (var D in F)
          if (n(F, D)) {
            var U = x(F, D, A, Y, M + "." + D, t);
            if (U instanceof Error)
              return U;
          }
        return null;
      }
      return y(O);
    }
    function ue(x) {
      if (!Array.isArray(x))
        return process.env.NODE_ENV !== "production" && i("Invalid argument supplied to oneOfType, expected an instance of array."), a;
      for (var O = 0; O < x.length; O++) {
        var B = x[O];
        if (typeof B != "function")
          return i(
            "Invalid argument supplied to oneOfType. Expected an array of check functions, but received " + fe(B) + " at index " + O + "."
          ), a;
      }
      function L(A, Y, M, F, z) {
        for (var D = [], U = 0; U < x.length; U++) {
          var he = x[U], v = he(A, Y, M, F, z, t);
          if (v == null)
            return null;
          v.data && n(v.data, "expectedType") && D.push(v.data.expectedType);
        }
        var Ee = D.length > 0 ? ", expected one of type [" + D.join(", ") + "]" : "";
        return new m("Invalid " + F + " `" + z + "` supplied to " + ("`" + M + "`" + Ee + "."));
      }
      return y(L);
    }
    function I() {
      function x(O, B, L, A, Y) {
        return ie(O[B]) ? null : new m("Invalid " + A + " `" + Y + "` supplied to " + ("`" + L + "`, expected a ReactNode."));
      }
      return y(x);
    }
    function ae(x, O, B, L, A) {
      return new m(
        (x || "React class") + ": " + O + " type `" + B + "." + L + "` is invalid; it must be a function, usually from the `prop-types` package, but received `" + A + "`."
      );
    }
    function me(x) {
      function O(B, L, A, Y, M) {
        var F = B[L], z = se(F);
        if (z !== "object")
          return new m("Invalid " + Y + " `" + M + "` of type `" + z + "` " + ("supplied to `" + A + "`, expected `object`."));
        for (var D in x) {
          var U = x[D];
          if (typeof U != "function")
            return ae(A, Y, M, D, be(U));
          var he = U(F, D, A, Y, M + "." + D, t);
          if (he)
            return he;
        }
        return null;
      }
      return y(O);
    }
    function oe(x) {
      function O(B, L, A, Y, M) {
        var F = B[L], z = se(F);
        if (z !== "object")
          return new m("Invalid " + Y + " `" + M + "` of type `" + z + "` " + ("supplied to `" + A + "`, expected `object`."));
        var D = r({}, B[L], x);
        for (var U in D) {
          var he = x[U];
          if (n(x, U) && typeof he != "function")
            return ae(A, Y, M, U, be(he));
          if (!he)
            return new m(
              "Invalid " + Y + " `" + M + "` key `" + U + "` supplied to `" + A + "`.\nBad object: " + JSON.stringify(B[L], null, "  ") + `
Valid keys: ` + JSON.stringify(Object.keys(x), null, "  ")
            );
          var v = he(F, U, A, Y, M + "." + U, t);
          if (v)
            return v;
        }
        return null;
      }
      return y(O);
    }
    function ie(x) {
      switch (typeof x) {
        case "number":
        case "string":
        case "undefined":
          return !0;
        case "boolean":
          return !x;
        case "object":
          if (Array.isArray(x))
            return x.every(ie);
          if (x === null || c(x))
            return !0;
          var O = p(x);
          if (O) {
            var B = O.call(x), L;
            if (O !== x.entries) {
              for (; !(L = B.next()).done; )
                if (!ie(L.value))
                  return !1;
            } else
              for (; !(L = B.next()).done; ) {
                var A = L.value;
                if (A && !ie(A[1]))
                  return !1;
              }
          } else
            return !1;
          return !0;
        default:
          return !1;
      }
    }
    function le(x, O) {
      return x === "symbol" ? !0 : O ? O["@@toStringTag"] === "Symbol" || typeof Symbol == "function" && O instanceof Symbol : !1;
    }
    function se(x) {
      var O = typeof x;
      return Array.isArray(x) ? "array" : x instanceof RegExp ? "object" : le(O, x) ? "symbol" : O;
    }
    function be(x) {
      if (typeof x > "u" || x === null)
        return "" + x;
      var O = se(x);
      if (O === "object") {
        if (x instanceof Date)
          return "date";
        if (x instanceof RegExp)
          return "regexp";
      }
      return O;
    }
    function fe(x) {
      var O = be(x);
      switch (O) {
        case "array":
        case "object":
          return "an " + O;
        case "boolean":
        case "date":
        case "regexp":
          return "a " + O;
        default:
          return O;
      }
    }
    function $e(x) {
      return !x.constructor || !x.constructor.name ? h : x.constructor.name;
    }
    return E.checkPropTypes = o, E.resetWarningCache = o.resetWarningCache, E.PropTypes = E, E;
  }, St;
}
var Rt, Rn;
function $i() {
  if (Rn)
    return Rt;
  Rn = 1;
  var e = Vt();
  function r() {
  }
  function t() {
  }
  return t.resetWarningCache = r, Rt = function() {
    function n(a, c, u, f, d, p) {
      if (p !== e) {
        var h = new Error(
          "Calling PropTypes validators directly is not supported by the `prop-types` package. Use PropTypes.checkPropTypes() to call them. Read more at http://fb.me/use-check-prop-types"
        );
        throw h.name = "Invariant Violation", h;
      }
    }
    n.isRequired = n;
    function o() {
      return n;
    }
    var i = {
      array: n,
      bigint: n,
      bool: n,
      func: n,
      number: n,
      object: n,
      string: n,
      symbol: n,
      any: n,
      arrayOf: o,
      element: n,
      elementType: n,
      instanceOf: o,
      node: n,
      objectOf: o,
      oneOf: o,
      oneOfType: o,
      shape: o,
      exact: o,
      checkPropTypes: t,
      resetWarningCache: r
    };
    return i.PropTypes = i, i;
  }, Rt;
}
if (process.env.NODE_ENV !== "production") {
  var Pi = yo(), ki = !0;
  Pt.exports = wi()(Pi.isElement, ki);
} else
  Pt.exports = $i()();
var Ai = Pt.exports;
const b = /* @__PURE__ */ li(Ai);
function Ni(e) {
  const {
    prototype: r = {}
  } = e;
  return !!r.isReactComponent;
}
function Ii(e, r, t, n, o) {
  const i = e[r], a = o || r;
  if (i == null || // When server-side rendering React doesn't warn either.
  // This is not an accurate check for SSR.
  // This is only in place for emotion compat.
  // TODO: Revisit once https://github.com/facebook/react/issues/20047 is resolved.
  typeof window > "u")
    return null;
  let c;
  return typeof i == "function" && !Ni(i) && (c = "Did you accidentally provide a plain function component instead?"), c !== void 0 ? new Error(`Invalid ${n} \`${a}\` supplied to \`${t}\`. Expected an element type that can hold a ref. ${c} For more information see https://mui.com/r/caveat-with-refs-guide`) : null;
}
const Mi = _i(b.elementType, Ii);
function lr(e) {
  let r = "https://mui.com/production-error/?code=" + e;
  for (let t = 1; t < arguments.length; t += 1)
    r += "&args[]=" + encodeURIComponent(arguments[t]);
  return "Minified MUI error #" + e + "; visit " + r + " for the full message.";
}
var kt = { exports: {} }, Q = {};
/**
 * @license React
 * react-is.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var Cn;
function ji() {
  if (Cn)
    return Q;
  Cn = 1;
  var e = Symbol.for("react.element"), r = Symbol.for("react.portal"), t = Symbol.for("react.fragment"), n = Symbol.for("react.strict_mode"), o = Symbol.for("react.profiler"), i = Symbol.for("react.provider"), a = Symbol.for("react.context"), c = Symbol.for("react.server_context"), u = Symbol.for("react.forward_ref"), f = Symbol.for("react.suspense"), d = Symbol.for("react.suspense_list"), p = Symbol.for("react.memo"), h = Symbol.for("react.lazy"), E = Symbol.for("react.offscreen"), g;
  g = Symbol.for("react.module.reference");
  function m(y) {
    if (typeof y == "object" && y !== null) {
      var S = y.$$typeof;
      switch (S) {
        case e:
          switch (y = y.type, y) {
            case t:
            case o:
            case n:
            case f:
            case d:
              return y;
            default:
              switch (y = y && y.$$typeof, y) {
                case c:
                case a:
                case u:
                case h:
                case p:
                case i:
                  return y;
                default:
                  return S;
              }
          }
        case r:
          return S;
      }
    }
  }
  return Q.ContextConsumer = a, Q.ContextProvider = i, Q.Element = e, Q.ForwardRef = u, Q.Fragment = t, Q.Lazy = h, Q.Memo = p, Q.Portal = r, Q.Profiler = o, Q.StrictMode = n, Q.Suspense = f, Q.SuspenseList = d, Q.isAsyncMode = function() {
    return !1;
  }, Q.isConcurrentMode = function() {
    return !1;
  }, Q.isContextConsumer = function(y) {
    return m(y) === a;
  }, Q.isContextProvider = function(y) {
    return m(y) === i;
  }, Q.isElement = function(y) {
    return typeof y == "object" && y !== null && y.$$typeof === e;
  }, Q.isForwardRef = function(y) {
    return m(y) === u;
  }, Q.isFragment = function(y) {
    return m(y) === t;
  }, Q.isLazy = function(y) {
    return m(y) === h;
  }, Q.isMemo = function(y) {
    return m(y) === p;
  }, Q.isPortal = function(y) {
    return m(y) === r;
  }, Q.isProfiler = function(y) {
    return m(y) === o;
  }, Q.isStrictMode = function(y) {
    return m(y) === n;
  }, Q.isSuspense = function(y) {
    return m(y) === f;
  }, Q.isSuspenseList = function(y) {
    return m(y) === d;
  }, Q.isValidElementType = function(y) {
    return typeof y == "string" || typeof y == "function" || y === t || y === o || y === n || y === f || y === d || y === E || typeof y == "object" && y !== null && (y.$$typeof === h || y.$$typeof === p || y.$$typeof === i || y.$$typeof === a || y.$$typeof === u || y.$$typeof === g || y.getModuleId !== void 0);
  }, Q.typeOf = m, Q;
}
var ee = {};
/**
 * @license React
 * react-is.development.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var On;
function Di() {
  return On || (On = 1, process.env.NODE_ENV !== "production" && function() {
    var e = Symbol.for("react.element"), r = Symbol.for("react.portal"), t = Symbol.for("react.fragment"), n = Symbol.for("react.strict_mode"), o = Symbol.for("react.profiler"), i = Symbol.for("react.provider"), a = Symbol.for("react.context"), c = Symbol.for("react.server_context"), u = Symbol.for("react.forward_ref"), f = Symbol.for("react.suspense"), d = Symbol.for("react.suspense_list"), p = Symbol.for("react.memo"), h = Symbol.for("react.lazy"), E = Symbol.for("react.offscreen"), g = !1, m = !1, y = !1, S = !1, w = !1, C;
    C = Symbol.for("react.module.reference");
    function _($) {
      return !!(typeof $ == "string" || typeof $ == "function" || $ === t || $ === o || w || $ === n || $ === f || $ === d || S || $ === E || g || m || y || typeof $ == "object" && $ !== null && ($.$$typeof === h || $.$$typeof === p || $.$$typeof === i || $.$$typeof === a || $.$$typeof === u || // This needs to include all possible module reference object
      // types supported by any Flight configuration anywhere since
      // we don't know which Flight build this will end up being used
      // with.
      $.$$typeof === C || $.getModuleId !== void 0));
    }
    function l($) {
      if (typeof $ == "object" && $ !== null) {
        var Se = $.$$typeof;
        switch (Se) {
          case e:
            var De = $.type;
            switch (De) {
              case t:
              case o:
              case n:
              case f:
              case d:
                return De;
              default:
                var Ge = De && De.$$typeof;
                switch (Ge) {
                  case c:
                  case a:
                  case u:
                  case h:
                  case p:
                  case i:
                    return Ge;
                  default:
                    return Se;
                }
            }
          case r:
            return Se;
        }
      }
    }
    var k = a, N = i, pe = e, ue = u, I = t, ae = h, me = p, oe = r, ie = o, le = n, se = f, be = d, fe = !1, $e = !1;
    function x($) {
      return fe || (fe = !0, console.warn("The ReactIs.isAsyncMode() alias has been deprecated, and will be removed in React 18+.")), !1;
    }
    function O($) {
      return $e || ($e = !0, console.warn("The ReactIs.isConcurrentMode() alias has been deprecated, and will be removed in React 18+.")), !1;
    }
    function B($) {
      return l($) === a;
    }
    function L($) {
      return l($) === i;
    }
    function A($) {
      return typeof $ == "object" && $ !== null && $.$$typeof === e;
    }
    function Y($) {
      return l($) === u;
    }
    function M($) {
      return l($) === t;
    }
    function F($) {
      return l($) === h;
    }
    function z($) {
      return l($) === p;
    }
    function D($) {
      return l($) === r;
    }
    function U($) {
      return l($) === o;
    }
    function he($) {
      return l($) === n;
    }
    function v($) {
      return l($) === f;
    }
    function Ee($) {
      return l($) === d;
    }
    ee.ContextConsumer = k, ee.ContextProvider = N, ee.Element = pe, ee.ForwardRef = ue, ee.Fragment = I, ee.Lazy = ae, ee.Memo = me, ee.Portal = oe, ee.Profiler = ie, ee.StrictMode = le, ee.Suspense = se, ee.SuspenseList = be, ee.isAsyncMode = x, ee.isConcurrentMode = O, ee.isContextConsumer = B, ee.isContextProvider = L, ee.isElement = A, ee.isForwardRef = Y, ee.isFragment = M, ee.isLazy = F, ee.isMemo = z, ee.isPortal = D, ee.isProfiler = U, ee.isStrictMode = he, ee.isSuspense = v, ee.isSuspenseList = Ee, ee.isValidElementType = _, ee.typeOf = l;
  }()), ee;
}
process.env.NODE_ENV === "production" ? kt.exports = ji() : kt.exports = Di();
var wn = kt.exports;
const Fi = /^\s*function(?:\s|\s*\/\*.*\*\/\s*)+([^(\s/]*)\s*/;
function Vi(e) {
  const r = `${e}`.match(Fi);
  return r && r[1] || "";
}
function go(e, r = "") {
  return e.displayName || e.name || Vi(e) || r;
}
function $n(e, r, t) {
  const n = go(r);
  return e.displayName || (n !== "" ? `${t}(${n})` : t);
}
function Li(e) {
  if (e != null) {
    if (typeof e == "string")
      return e;
    if (typeof e == "function")
      return go(e, "Component");
    if (typeof e == "object")
      switch (e.$$typeof) {
        case wn.ForwardRef:
          return $n(e, e.render, "ForwardRef");
        case wn.Memo:
          return $n(e, e.type, "memo");
        default:
          return;
      }
  }
}
const zi = b.oneOfType([b.func, b.object]), Bi = zi;
function Oe(e) {
  if (typeof e != "string")
    throw new Error(process.env.NODE_ENV !== "production" ? "MUI: `capitalize(string)` expects a string argument." : lr(7));
  return e.charAt(0).toUpperCase() + e.slice(1);
}
function Yi(e, r) {
  typeof e == "function" ? e(r) : e && (e.current = r);
}
const Ui = typeof window < "u" ? P.useLayoutEffect : P.useEffect, Wi = Ui;
function Fr(e) {
  const r = P.useRef(e);
  return Wi(() => {
    r.current = e;
  }), P.useCallback((...t) => (
    // @ts-expect-error hide `this`
    // tslint:disable-next-line:ban-comma-operator
    (0, r.current)(...t)
  ), []);
}
function Pn(...e) {
  return P.useMemo(() => e.every((r) => r == null) ? null : (r) => {
    e.forEach((t) => {
      Yi(t, r);
    });
  }, e);
}
let Jr = !0, At = !1, kn;
const qi = {
  text: !0,
  search: !0,
  url: !0,
  tel: !0,
  email: !0,
  password: !0,
  number: !0,
  date: !0,
  month: !0,
  week: !0,
  time: !0,
  datetime: !0,
  "datetime-local": !0
};
function Ki(e) {
  const {
    type: r,
    tagName: t
  } = e;
  return !!(t === "INPUT" && qi[r] && !e.readOnly || t === "TEXTAREA" && !e.readOnly || e.isContentEditable);
}
function Gi(e) {
  e.metaKey || e.altKey || e.ctrlKey || (Jr = !0);
}
function Ct() {
  Jr = !1;
}
function Hi() {
  this.visibilityState === "hidden" && At && (Jr = !0);
}
function Xi(e) {
  e.addEventListener("keydown", Gi, !0), e.addEventListener("mousedown", Ct, !0), e.addEventListener("pointerdown", Ct, !0), e.addEventListener("touchstart", Ct, !0), e.addEventListener("visibilitychange", Hi, !0);
}
function Ji(e) {
  const {
    target: r
  } = e;
  try {
    return r.matches(":focus-visible");
  } catch {
  }
  return Jr || Ki(r);
}
function Zi() {
  const e = P.useCallback((o) => {
    o != null && Xi(o.ownerDocument);
  }, []), r = P.useRef(!1);
  function t() {
    return r.current ? (At = !0, window.clearTimeout(kn), kn = window.setTimeout(() => {
      At = !1;
    }, 100), r.current = !1, !0) : !1;
  }
  function n(o) {
    return Ji(o) ? (r.current = !0, !0) : !1;
  }
  return {
    isFocusVisibleRef: r,
    onFocus: n,
    onBlur: t,
    ref: e
  };
}
function Lt(e, r) {
  const t = j({}, r);
  return Object.keys(e).forEach((n) => {
    if (n.toString().match(/^(components|slots)$/))
      t[n] = j({}, e[n], t[n]);
    else if (n.toString().match(/^(componentsProps|slotProps)$/)) {
      const o = e[n] || {}, i = r[n];
      t[n] = {}, !i || !Object.keys(i) ? t[n] = o : !o || !Object.keys(o) ? t[n] = i : (t[n] = j({}, i), Object.keys(o).forEach((a) => {
        t[n][a] = Lt(o[a], i[a]);
      }));
    } else
      t[n] === void 0 && (t[n] = e[n]);
  }), t;
}
function bo(e, r, t = void 0) {
  const n = {};
  return Object.keys(e).forEach(
    // `Objet.keys(slots)` can't be wider than `T` because we infer `T` from `slots`.
    // @ts-expect-error https://github.com/microsoft/TypeScript/pull/12253#issuecomment-263132208
    (o) => {
      n[o] = e[o].reduce((i, a) => {
        if (a) {
          const c = r(a);
          c !== "" && i.push(c), t && t[a] && i.push(t[a]);
        }
        return i;
      }, []).join(" ");
    }
  ), n;
}
const An = (e) => e, Qi = () => {
  let e = An;
  return {
    configure(r) {
      e = r;
    },
    generate(r) {
      return e(r);
    },
    reset() {
      e = An;
    }
  };
}, ea = Qi(), ra = ea, ta = {
  active: "active",
  checked: "checked",
  completed: "completed",
  disabled: "disabled",
  readOnly: "readOnly",
  error: "error",
  expanded: "expanded",
  focused: "focused",
  focusVisible: "focusVisible",
  required: "required",
  selected: "selected"
};
function Zr(e, r, t = "Mui") {
  const n = ta[r];
  return n ? `${t}-${n}` : `${ra.generate(e)}-${r}`;
}
function zt(e, r, t = "Mui") {
  const n = {};
  return r.forEach((o) => {
    n[o] = Zr(e, o, t);
  }), n;
}
const Eo = "$$material";
function je(e, r) {
  if (e == null)
    return {};
  var t = {}, n = Object.keys(e), o, i;
  for (i = 0; i < n.length; i++)
    o = n[i], !(r.indexOf(o) >= 0) && (t[o] = e[o]);
  return t;
}
function xo(e) {
  var r = /* @__PURE__ */ Object.create(null);
  return function(t) {
    return r[t] === void 0 && (r[t] = e(t)), r[t];
  };
}
var na = /^((children|dangerouslySetInnerHTML|key|ref|autoFocus|defaultValue|defaultChecked|innerHTML|suppressContentEditableWarning|suppressHydrationWarning|valueLink|abbr|accept|acceptCharset|accessKey|action|allow|allowUserMedia|allowPaymentRequest|allowFullScreen|allowTransparency|alt|async|autoComplete|autoPlay|capture|cellPadding|cellSpacing|challenge|charSet|checked|cite|classID|className|cols|colSpan|content|contentEditable|contextMenu|controls|controlsList|coords|crossOrigin|data|dateTime|decoding|default|defer|dir|disabled|disablePictureInPicture|download|draggable|encType|enterKeyHint|form|formAction|formEncType|formMethod|formNoValidate|formTarget|frameBorder|headers|height|hidden|high|href|hrefLang|htmlFor|httpEquiv|id|inputMode|integrity|is|keyParams|keyType|kind|label|lang|list|loading|loop|low|marginHeight|marginWidth|max|maxLength|media|mediaGroup|method|min|minLength|multiple|muted|name|nonce|noValidate|open|optimum|pattern|placeholder|playsInline|poster|preload|profile|radioGroup|readOnly|referrerPolicy|rel|required|reversed|role|rows|rowSpan|sandbox|scope|scoped|scrolling|seamless|selected|shape|size|sizes|slot|span|spellCheck|src|srcDoc|srcLang|srcSet|start|step|style|summary|tabIndex|target|title|translate|type|useMap|value|width|wmode|wrap|about|datatype|inlist|prefix|property|resource|typeof|vocab|autoCapitalize|autoCorrect|autoSave|color|incremental|fallback|inert|itemProp|itemScope|itemType|itemID|itemRef|on|option|results|security|unselectable|accentHeight|accumulate|additive|alignmentBaseline|allowReorder|alphabetic|amplitude|arabicForm|ascent|attributeName|attributeType|autoReverse|azimuth|baseFrequency|baselineShift|baseProfile|bbox|begin|bias|by|calcMode|capHeight|clip|clipPathUnits|clipPath|clipRule|colorInterpolation|colorInterpolationFilters|colorProfile|colorRendering|contentScriptType|contentStyleType|cursor|cx|cy|d|decelerate|descent|diffuseConstant|direction|display|divisor|dominantBaseline|dur|dx|dy|edgeMode|elevation|enableBackground|end|exponent|externalResourcesRequired|fill|fillOpacity|fillRule|filter|filterRes|filterUnits|floodColor|floodOpacity|focusable|fontFamily|fontSize|fontSizeAdjust|fontStretch|fontStyle|fontVariant|fontWeight|format|from|fr|fx|fy|g1|g2|glyphName|glyphOrientationHorizontal|glyphOrientationVertical|glyphRef|gradientTransform|gradientUnits|hanging|horizAdvX|horizOriginX|ideographic|imageRendering|in|in2|intercept|k|k1|k2|k3|k4|kernelMatrix|kernelUnitLength|kerning|keyPoints|keySplines|keyTimes|lengthAdjust|letterSpacing|lightingColor|limitingConeAngle|local|markerEnd|markerMid|markerStart|markerHeight|markerUnits|markerWidth|mask|maskContentUnits|maskUnits|mathematical|mode|numOctaves|offset|opacity|operator|order|orient|orientation|origin|overflow|overlinePosition|overlineThickness|panose1|paintOrder|pathLength|patternContentUnits|patternTransform|patternUnits|pointerEvents|points|pointsAtX|pointsAtY|pointsAtZ|preserveAlpha|preserveAspectRatio|primitiveUnits|r|radius|refX|refY|renderingIntent|repeatCount|repeatDur|requiredExtensions|requiredFeatures|restart|result|rotate|rx|ry|scale|seed|shapeRendering|slope|spacing|specularConstant|specularExponent|speed|spreadMethod|startOffset|stdDeviation|stemh|stemv|stitchTiles|stopColor|stopOpacity|strikethroughPosition|strikethroughThickness|string|stroke|strokeDasharray|strokeDashoffset|strokeLinecap|strokeLinejoin|strokeMiterlimit|strokeOpacity|strokeWidth|surfaceScale|systemLanguage|tableValues|targetX|targetY|textAnchor|textDecoration|textRendering|textLength|to|transform|u1|u2|underlinePosition|underlineThickness|unicode|unicodeBidi|unicodeRange|unitsPerEm|vAlphabetic|vHanging|vIdeographic|vMathematical|values|vectorEffect|version|vertAdvY|vertOriginX|vertOriginY|viewBox|viewTarget|visibility|widths|wordSpacing|writingMode|x|xHeight|x1|x2|xChannelSelector|xlinkActuate|xlinkArcrole|xlinkHref|xlinkRole|xlinkShow|xlinkTitle|xlinkType|xmlBase|xmlns|xmlnsXlink|xmlLang|xmlSpace|y|y1|y2|yChannelSelector|z|zoomAndPan|for|class|autofocus)|(([Dd][Aa][Tt][Aa]|[Aa][Rr][Ii][Aa]|x)-.*))$/, oa = /* @__PURE__ */ xo(
  function(e) {
    return na.test(e) || e.charCodeAt(0) === 111 && e.charCodeAt(1) === 110 && e.charCodeAt(2) < 91;
  }
  /* Z+1 */
);
function ia(e) {
  if (e.sheet)
    return e.sheet;
  for (var r = 0; r < document.styleSheets.length; r++)
    if (document.styleSheets[r].ownerNode === e)
      return document.styleSheets[r];
}
function aa(e) {
  var r = document.createElement("style");
  return r.setAttribute("data-emotion", e.key), e.nonce !== void 0 && r.setAttribute("nonce", e.nonce), r.appendChild(document.createTextNode("")), r.setAttribute("data-s", ""), r;
}
var sa = /* @__PURE__ */ function() {
  function e(t) {
    var n = this;
    this._insertTag = function(o) {
      var i;
      n.tags.length === 0 ? n.insertionPoint ? i = n.insertionPoint.nextSibling : n.prepend ? i = n.container.firstChild : i = n.before : i = n.tags[n.tags.length - 1].nextSibling, n.container.insertBefore(o, i), n.tags.push(o);
    }, this.isSpeedy = t.speedy === void 0 ? process.env.NODE_ENV === "production" : t.speedy, this.tags = [], this.ctr = 0, this.nonce = t.nonce, this.key = t.key, this.container = t.container, this.prepend = t.prepend, this.insertionPoint = t.insertionPoint, this.before = null;
  }
  var r = e.prototype;
  return r.hydrate = function(n) {
    n.forEach(this._insertTag);
  }, r.insert = function(n) {
    this.ctr % (this.isSpeedy ? 65e3 : 1) === 0 && this._insertTag(aa(this));
    var o = this.tags[this.tags.length - 1];
    if (process.env.NODE_ENV !== "production") {
      var i = n.charCodeAt(0) === 64 && n.charCodeAt(1) === 105;
      i && this._alreadyInsertedOrderInsensitiveRule && console.error(`You're attempting to insert the following rule:
` + n + "\n\n`@import` rules must be before all other types of rules in a stylesheet but other rules have already been inserted. Please ensure that `@import` rules are before all other rules."), this._alreadyInsertedOrderInsensitiveRule = this._alreadyInsertedOrderInsensitiveRule || !i;
    }
    if (this.isSpeedy) {
      var a = ia(o);
      try {
        a.insertRule(n, a.cssRules.length);
      } catch (c) {
        process.env.NODE_ENV !== "production" && !/:(-moz-placeholder|-moz-focus-inner|-moz-focusring|-ms-input-placeholder|-moz-read-write|-moz-read-only|-ms-clear|-ms-expand|-ms-reveal){/.test(n) && console.error('There was a problem inserting the following rule: "' + n + '"', c);
      }
    } else
      o.appendChild(document.createTextNode(n));
    this.ctr++;
  }, r.flush = function() {
    this.tags.forEach(function(n) {
      return n.parentNode && n.parentNode.removeChild(n);
    }), this.tags = [], this.ctr = 0, process.env.NODE_ENV !== "production" && (this._alreadyInsertedOrderInsensitiveRule = !1);
  }, e;
}(), Re = "-ms-", Gr = "-moz-", H = "-webkit-", Bt = "comm", Yt = "rule", Ut = "decl", ca = "@import", To = "@keyframes", ua = "@layer", la = Math.abs, Qr = String.fromCharCode, fa = Object.assign;
function da(e, r) {
  return _e(e, 0) ^ 45 ? (((r << 2 ^ _e(e, 0)) << 2 ^ _e(e, 1)) << 2 ^ _e(e, 2)) << 2 ^ _e(e, 3) : 0;
}
function _o(e) {
  return e.trim();
}
function pa(e, r) {
  return (e = r.exec(e)) ? e[0] : e;
}
function X(e, r, t) {
  return e.replace(r, t);
}
function Nt(e, r) {
  return e.indexOf(r);
}
function _e(e, r) {
  return e.charCodeAt(r) | 0;
}
function wr(e, r, t) {
  return e.slice(r, t);
}
function Ve(e) {
  return e.length;
}
function Wt(e) {
  return e.length;
}
function Vr(e, r) {
  return r.push(e), e;
}
function ma(e, r) {
  return e.map(r).join("");
}
var et = 1, fr = 1, So = 0, we = 0, Te = 0, pr = "";
function rt(e, r, t, n, o, i, a) {
  return { value: e, root: r, parent: t, type: n, props: o, children: i, line: et, column: fr, length: a, return: "" };
}
function Sr(e, r) {
  return fa(rt("", null, null, "", null, null, 0), e, { length: -e.length }, r);
}
function ha() {
  return Te;
}
function ya() {
  return Te = we > 0 ? _e(pr, --we) : 0, fr--, Te === 10 && (fr = 1, et--), Te;
}
function ke() {
  return Te = we < So ? _e(pr, we++) : 0, fr++, Te === 10 && (fr = 1, et++), Te;
}
function ze() {
  return _e(pr, we);
}
function Ur() {
  return we;
}
function kr(e, r) {
  return wr(pr, e, r);
}
function $r(e) {
  switch (e) {
    case 0:
    case 9:
    case 10:
    case 13:
    case 32:
      return 5;
    case 33:
    case 43:
    case 44:
    case 47:
    case 62:
    case 64:
    case 126:
    case 59:
    case 123:
    case 125:
      return 4;
    case 58:
      return 3;
    case 34:
    case 39:
    case 40:
    case 91:
      return 2;
    case 41:
    case 93:
      return 1;
  }
  return 0;
}
function Ro(e) {
  return et = fr = 1, So = Ve(pr = e), we = 0, [];
}
function Co(e) {
  return pr = "", e;
}
function Wr(e) {
  return _o(kr(we - 1, It(e === 91 ? e + 2 : e === 40 ? e + 1 : e)));
}
function va(e) {
  for (; (Te = ze()) && Te < 33; )
    ke();
  return $r(e) > 2 || $r(Te) > 3 ? "" : " ";
}
function ga(e, r) {
  for (; --r && ke() && !(Te < 48 || Te > 102 || Te > 57 && Te < 65 || Te > 70 && Te < 97); )
    ;
  return kr(e, Ur() + (r < 6 && ze() == 32 && ke() == 32));
}
function It(e) {
  for (; ke(); )
    switch (Te) {
      case e:
        return we;
      case 34:
      case 39:
        e !== 34 && e !== 39 && It(Te);
        break;
      case 40:
        e === 41 && It(e);
        break;
      case 92:
        ke();
        break;
    }
  return we;
}
function ba(e, r) {
  for (; ke() && e + Te !== 47 + 10; )
    if (e + Te === 42 + 42 && ze() === 47)
      break;
  return "/*" + kr(r, we - 1) + "*" + Qr(e === 47 ? e : ke());
}
function Ea(e) {
  for (; !$r(ze()); )
    ke();
  return kr(e, we);
}
function xa(e) {
  return Co(qr("", null, null, null, [""], e = Ro(e), 0, [0], e));
}
function qr(e, r, t, n, o, i, a, c, u) {
  for (var f = 0, d = 0, p = a, h = 0, E = 0, g = 0, m = 1, y = 1, S = 1, w = 0, C = "", _ = o, l = i, k = n, N = C; y; )
    switch (g = w, w = ke()) {
      case 40:
        if (g != 108 && _e(N, p - 1) == 58) {
          Nt(N += X(Wr(w), "&", "&\f"), "&\f") != -1 && (S = -1);
          break;
        }
      case 34:
      case 39:
      case 91:
        N += Wr(w);
        break;
      case 9:
      case 10:
      case 13:
      case 32:
        N += va(g);
        break;
      case 92:
        N += ga(Ur() - 1, 7);
        continue;
      case 47:
        switch (ze()) {
          case 42:
          case 47:
            Vr(Ta(ba(ke(), Ur()), r, t), u);
            break;
          default:
            N += "/";
        }
        break;
      case 123 * m:
        c[f++] = Ve(N) * S;
      case 125 * m:
      case 59:
      case 0:
        switch (w) {
          case 0:
          case 125:
            y = 0;
          case 59 + d:
            S == -1 && (N = X(N, /\f/g, "")), E > 0 && Ve(N) - p && Vr(E > 32 ? In(N + ";", n, t, p - 1) : In(X(N, " ", "") + ";", n, t, p - 2), u);
            break;
          case 59:
            N += ";";
          default:
            if (Vr(k = Nn(N, r, t, f, d, o, c, C, _ = [], l = [], p), i), w === 123)
              if (d === 0)
                qr(N, r, k, k, _, i, p, c, l);
              else
                switch (h === 99 && _e(N, 3) === 110 ? 100 : h) {
                  case 100:
                  case 108:
                  case 109:
                  case 115:
                    qr(e, k, k, n && Vr(Nn(e, k, k, 0, 0, o, c, C, o, _ = [], p), l), o, l, p, c, n ? _ : l);
                    break;
                  default:
                    qr(N, k, k, k, [""], l, 0, c, l);
                }
        }
        f = d = E = 0, m = S = 1, C = N = "", p = a;
        break;
      case 58:
        p = 1 + Ve(N), E = g;
      default:
        if (m < 1) {
          if (w == 123)
            --m;
          else if (w == 125 && m++ == 0 && ya() == 125)
            continue;
        }
        switch (N += Qr(w), w * m) {
          case 38:
            S = d > 0 ? 1 : (N += "\f", -1);
            break;
          case 44:
            c[f++] = (Ve(N) - 1) * S, S = 1;
            break;
          case 64:
            ze() === 45 && (N += Wr(ke())), h = ze(), d = p = Ve(C = N += Ea(Ur())), w++;
            break;
          case 45:
            g === 45 && Ve(N) == 2 && (m = 0);
        }
    }
  return i;
}
function Nn(e, r, t, n, o, i, a, c, u, f, d) {
  for (var p = o - 1, h = o === 0 ? i : [""], E = Wt(h), g = 0, m = 0, y = 0; g < n; ++g)
    for (var S = 0, w = wr(e, p + 1, p = la(m = a[g])), C = e; S < E; ++S)
      (C = _o(m > 0 ? h[S] + " " + w : X(w, /&\f/g, h[S]))) && (u[y++] = C);
  return rt(e, r, t, o === 0 ? Yt : c, u, f, d);
}
function Ta(e, r, t) {
  return rt(e, r, t, Bt, Qr(ha()), wr(e, 2, -2), 0);
}
function In(e, r, t, n) {
  return rt(e, r, t, Ut, wr(e, 0, n), wr(e, n + 1, -1), n);
}
function cr(e, r) {
  for (var t = "", n = Wt(e), o = 0; o < n; o++)
    t += r(e[o], o, e, r) || "";
  return t;
}
function _a(e, r, t, n) {
  switch (e.type) {
    case ua:
      if (e.children.length)
        break;
    case ca:
    case Ut:
      return e.return = e.return || e.value;
    case Bt:
      return "";
    case To:
      return e.return = e.value + "{" + cr(e.children, n) + "}";
    case Yt:
      e.value = e.props.join(",");
  }
  return Ve(t = cr(e.children, n)) ? e.return = e.value + "{" + t + "}" : "";
}
function Sa(e) {
  var r = Wt(e);
  return function(t, n, o, i) {
    for (var a = "", c = 0; c < r; c++)
      a += e[c](t, n, o, i) || "";
    return a;
  };
}
function Ra(e) {
  return function(r) {
    r.root || (r = r.return) && e(r);
  };
}
var Ca = function(r, t, n) {
  for (var o = 0, i = 0; o = i, i = ze(), o === 38 && i === 12 && (t[n] = 1), !$r(i); )
    ke();
  return kr(r, we);
}, Oa = function(r, t) {
  var n = -1, o = 44;
  do
    switch ($r(o)) {
      case 0:
        o === 38 && ze() === 12 && (t[n] = 1), r[n] += Ca(we - 1, t, n);
        break;
      case 2:
        r[n] += Wr(o);
        break;
      case 4:
        if (o === 44) {
          r[++n] = ze() === 58 ? "&\f" : "", t[n] = r[n].length;
          break;
        }
      default:
        r[n] += Qr(o);
    }
  while (o = ke());
  return r;
}, wa = function(r, t) {
  return Co(Oa(Ro(r), t));
}, Mn = /* @__PURE__ */ new WeakMap(), $a = function(r) {
  if (!(r.type !== "rule" || !r.parent || // positive .length indicates that this rule contains pseudo
  // negative .length indicates that this rule has been already prefixed
  r.length < 1)) {
    for (var t = r.value, n = r.parent, o = r.column === n.column && r.line === n.line; n.type !== "rule"; )
      if (n = n.parent, !n)
        return;
    if (!(r.props.length === 1 && t.charCodeAt(0) !== 58 && !Mn.get(n)) && !o) {
      Mn.set(r, !0);
      for (var i = [], a = wa(t, i), c = n.props, u = 0, f = 0; u < a.length; u++)
        for (var d = 0; d < c.length; d++, f++)
          r.props[f] = i[u] ? a[u].replace(/&\f/g, c[d]) : c[d] + " " + a[u];
    }
  }
}, Pa = function(r) {
  if (r.type === "decl") {
    var t = r.value;
    // charcode for l
    t.charCodeAt(0) === 108 && // charcode for b
    t.charCodeAt(2) === 98 && (r.return = "", r.value = "");
  }
}, ka = "emotion-disable-server-rendering-unsafe-selector-warning-please-do-not-use-this-the-warning-exists-for-a-reason", Aa = function(r) {
  return r.type === "comm" && r.children.indexOf(ka) > -1;
}, Na = function(r) {
  return function(t, n, o) {
    if (!(t.type !== "rule" || r.compat)) {
      var i = t.value.match(/(:first|:nth|:nth-last)-child/g);
      if (i) {
        for (var a = !!t.parent, c = a ? t.parent.children : (
          // global rule at the root level
          o
        ), u = c.length - 1; u >= 0; u--) {
          var f = c[u];
          if (f.line < t.line)
            break;
          if (f.column < t.column) {
            if (Aa(f))
              return;
            break;
          }
        }
        i.forEach(function(d) {
          console.error('The pseudo class "' + d + '" is potentially unsafe when doing server-side rendering. Try changing it to "' + d.split("-child")[0] + '-of-type".');
        });
      }
    }
  };
}, Oo = function(r) {
  return r.type.charCodeAt(1) === 105 && r.type.charCodeAt(0) === 64;
}, Ia = function(r, t) {
  for (var n = r - 1; n >= 0; n--)
    if (!Oo(t[n]))
      return !0;
  return !1;
}, jn = function(r) {
  r.type = "", r.value = "", r.return = "", r.children = "", r.props = "";
}, Ma = function(r, t, n) {
  Oo(r) && (r.parent ? (console.error("`@import` rules can't be nested inside other rules. Please move it to the top level and put it before regular rules. Keep in mind that they can only be used within global styles."), jn(r)) : Ia(t, n) && (console.error("`@import` rules can't be after other rules. Please put your `@import` rules before your other rules."), jn(r)));
};
function wo(e, r) {
  switch (da(e, r)) {
    case 5103:
      return H + "print-" + e + e;
    case 5737:
    case 4201:
    case 3177:
    case 3433:
    case 1641:
    case 4457:
    case 2921:
    case 5572:
    case 6356:
    case 5844:
    case 3191:
    case 6645:
    case 3005:
    case 6391:
    case 5879:
    case 5623:
    case 6135:
    case 4599:
    case 4855:
    case 4215:
    case 6389:
    case 5109:
    case 5365:
    case 5621:
    case 3829:
      return H + e + e;
    case 5349:
    case 4246:
    case 4810:
    case 6968:
    case 2756:
      return H + e + Gr + e + Re + e + e;
    case 6828:
    case 4268:
      return H + e + Re + e + e;
    case 6165:
      return H + e + Re + "flex-" + e + e;
    case 5187:
      return H + e + X(e, /(\w+).+(:[^]+)/, H + "box-$1$2" + Re + "flex-$1$2") + e;
    case 5443:
      return H + e + Re + "flex-item-" + X(e, /flex-|-self/, "") + e;
    case 4675:
      return H + e + Re + "flex-line-pack" + X(e, /align-content|flex-|-self/, "") + e;
    case 5548:
      return H + e + Re + X(e, "shrink", "negative") + e;
    case 5292:
      return H + e + Re + X(e, "basis", "preferred-size") + e;
    case 6060:
      return H + "box-" + X(e, "-grow", "") + H + e + Re + X(e, "grow", "positive") + e;
    case 4554:
      return H + X(e, /([^-])(transform)/g, "$1" + H + "$2") + e;
    case 6187:
      return X(X(X(e, /(zoom-|grab)/, H + "$1"), /(image-set)/, H + "$1"), e, "") + e;
    case 5495:
    case 3959:
      return X(e, /(image-set\([^]*)/, H + "$1$`$1");
    case 4968:
      return X(X(e, /(.+:)(flex-)?(.*)/, H + "box-pack:$3" + Re + "flex-pack:$3"), /s.+-b[^;]+/, "justify") + H + e + e;
    case 4095:
    case 3583:
    case 4068:
    case 2532:
      return X(e, /(.+)-inline(.+)/, H + "$1$2") + e;
    case 8116:
    case 7059:
    case 5753:
    case 5535:
    case 5445:
    case 5701:
    case 4933:
    case 4677:
    case 5533:
    case 5789:
    case 5021:
    case 4765:
      if (Ve(e) - 1 - r > 6)
        switch (_e(e, r + 1)) {
          case 109:
            if (_e(e, r + 4) !== 45)
              break;
          case 102:
            return X(e, /(.+:)(.+)-([^]+)/, "$1" + H + "$2-$3$1" + Gr + (_e(e, r + 3) == 108 ? "$3" : "$2-$3")) + e;
          case 115:
            return ~Nt(e, "stretch") ? wo(X(e, "stretch", "fill-available"), r) + e : e;
        }
      break;
    case 4949:
      if (_e(e, r + 1) !== 115)
        break;
    case 6444:
      switch (_e(e, Ve(e) - 3 - (~Nt(e, "!important") && 10))) {
        case 107:
          return X(e, ":", ":" + H) + e;
        case 101:
          return X(e, /(.+:)([^;!]+)(;|!.+)?/, "$1" + H + (_e(e, 14) === 45 ? "inline-" : "") + "box$3$1" + H + "$2$3$1" + Re + "$2box$3") + e;
      }
      break;
    case 5936:
      switch (_e(e, r + 11)) {
        case 114:
          return H + e + Re + X(e, /[svh]\w+-[tblr]{2}/, "tb") + e;
        case 108:
          return H + e + Re + X(e, /[svh]\w+-[tblr]{2}/, "tb-rl") + e;
        case 45:
          return H + e + Re + X(e, /[svh]\w+-[tblr]{2}/, "lr") + e;
      }
      return H + e + Re + e + e;
  }
  return e;
}
var ja = function(r, t, n, o) {
  if (r.length > -1 && !r.return)
    switch (r.type) {
      case Ut:
        r.return = wo(r.value, r.length);
        break;
      case To:
        return cr([Sr(r, {
          value: X(r.value, "@", "@" + H)
        })], o);
      case Yt:
        if (r.length)
          return ma(r.props, function(i) {
            switch (pa(i, /(::plac\w+|:read-\w+)/)) {
              case ":read-only":
              case ":read-write":
                return cr([Sr(r, {
                  props: [X(i, /:(read-\w+)/, ":" + Gr + "$1")]
                })], o);
              case "::placeholder":
                return cr([Sr(r, {
                  props: [X(i, /:(plac\w+)/, ":" + H + "input-$1")]
                }), Sr(r, {
                  props: [X(i, /:(plac\w+)/, ":" + Gr + "$1")]
                }), Sr(r, {
                  props: [X(i, /:(plac\w+)/, Re + "input-$1")]
                })], o);
            }
            return "";
          });
    }
}, Da = [ja], Fa = function(r) {
  var t = r.key;
  if (process.env.NODE_ENV !== "production" && !t)
    throw new Error(`You have to configure \`key\` for your cache. Please make sure it's unique (and not equal to 'css') as it's used for linking styles to your cache.
If multiple caches share the same key they might "fight" for each other's style elements.`);
  if (t === "css") {
    var n = document.querySelectorAll("style[data-emotion]:not([data-s])");
    Array.prototype.forEach.call(n, function(m) {
      var y = m.getAttribute("data-emotion");
      y.indexOf(" ") !== -1 && (document.head.appendChild(m), m.setAttribute("data-s", ""));
    });
  }
  var o = r.stylisPlugins || Da;
  if (process.env.NODE_ENV !== "production" && /[^a-z-]/.test(t))
    throw new Error('Emotion key must only contain lower case alphabetical characters and - but "' + t + '" was passed');
  var i = {}, a, c = [];
  a = r.container || document.head, Array.prototype.forEach.call(
    // this means we will ignore elements which don't have a space in them which
    // means that the style elements we're looking at are only Emotion 11 server-rendered style elements
    document.querySelectorAll('style[data-emotion^="' + t + ' "]'),
    function(m) {
      for (var y = m.getAttribute("data-emotion").split(" "), S = 1; S < y.length; S++)
        i[y[S]] = !0;
      c.push(m);
    }
  );
  var u, f = [$a, Pa];
  process.env.NODE_ENV !== "production" && f.push(Na({
    get compat() {
      return g.compat;
    }
  }), Ma);
  {
    var d, p = [_a, process.env.NODE_ENV !== "production" ? function(m) {
      m.root || (m.return ? d.insert(m.return) : m.value && m.type !== Bt && d.insert(m.value + "{}"));
    } : Ra(function(m) {
      d.insert(m);
    })], h = Sa(f.concat(o, p)), E = function(y) {
      return cr(xa(y), h);
    };
    u = function(y, S, w, C) {
      d = w, process.env.NODE_ENV !== "production" && S.map !== void 0 && (d = {
        insert: function(l) {
          w.insert(l + S.map);
        }
      }), E(y ? y + "{" + S.styles + "}" : S.styles), C && (g.inserted[S.name] = !0);
    };
  }
  var g = {
    key: t,
    sheet: new sa({
      key: t,
      container: a,
      nonce: r.nonce,
      speedy: r.speedy,
      prepend: r.prepend,
      insertionPoint: r.insertionPoint
    }),
    nonce: r.nonce,
    inserted: i,
    registered: {},
    insert: u
  };
  return g.sheet.hydrate(c), g;
}, Mt = { exports: {} }, re = {};
/** @license React v16.13.1
 * react-is.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var Dn;
function Va() {
  if (Dn)
    return re;
  Dn = 1;
  var e = typeof Symbol == "function" && Symbol.for, r = e ? Symbol.for("react.element") : 60103, t = e ? Symbol.for("react.portal") : 60106, n = e ? Symbol.for("react.fragment") : 60107, o = e ? Symbol.for("react.strict_mode") : 60108, i = e ? Symbol.for("react.profiler") : 60114, a = e ? Symbol.for("react.provider") : 60109, c = e ? Symbol.for("react.context") : 60110, u = e ? Symbol.for("react.async_mode") : 60111, f = e ? Symbol.for("react.concurrent_mode") : 60111, d = e ? Symbol.for("react.forward_ref") : 60112, p = e ? Symbol.for("react.suspense") : 60113, h = e ? Symbol.for("react.suspense_list") : 60120, E = e ? Symbol.for("react.memo") : 60115, g = e ? Symbol.for("react.lazy") : 60116, m = e ? Symbol.for("react.block") : 60121, y = e ? Symbol.for("react.fundamental") : 60117, S = e ? Symbol.for("react.responder") : 60118, w = e ? Symbol.for("react.scope") : 60119;
  function C(l) {
    if (typeof l == "object" && l !== null) {
      var k = l.$$typeof;
      switch (k) {
        case r:
          switch (l = l.type, l) {
            case u:
            case f:
            case n:
            case i:
            case o:
            case p:
              return l;
            default:
              switch (l = l && l.$$typeof, l) {
                case c:
                case d:
                case g:
                case E:
                case a:
                  return l;
                default:
                  return k;
              }
          }
        case t:
          return k;
      }
    }
  }
  function _(l) {
    return C(l) === f;
  }
  return re.AsyncMode = u, re.ConcurrentMode = f, re.ContextConsumer = c, re.ContextProvider = a, re.Element = r, re.ForwardRef = d, re.Fragment = n, re.Lazy = g, re.Memo = E, re.Portal = t, re.Profiler = i, re.StrictMode = o, re.Suspense = p, re.isAsyncMode = function(l) {
    return _(l) || C(l) === u;
  }, re.isConcurrentMode = _, re.isContextConsumer = function(l) {
    return C(l) === c;
  }, re.isContextProvider = function(l) {
    return C(l) === a;
  }, re.isElement = function(l) {
    return typeof l == "object" && l !== null && l.$$typeof === r;
  }, re.isForwardRef = function(l) {
    return C(l) === d;
  }, re.isFragment = function(l) {
    return C(l) === n;
  }, re.isLazy = function(l) {
    return C(l) === g;
  }, re.isMemo = function(l) {
    return C(l) === E;
  }, re.isPortal = function(l) {
    return C(l) === t;
  }, re.isProfiler = function(l) {
    return C(l) === i;
  }, re.isStrictMode = function(l) {
    return C(l) === o;
  }, re.isSuspense = function(l) {
    return C(l) === p;
  }, re.isValidElementType = function(l) {
    return typeof l == "string" || typeof l == "function" || l === n || l === f || l === i || l === o || l === p || l === h || typeof l == "object" && l !== null && (l.$$typeof === g || l.$$typeof === E || l.$$typeof === a || l.$$typeof === c || l.$$typeof === d || l.$$typeof === y || l.$$typeof === S || l.$$typeof === w || l.$$typeof === m);
  }, re.typeOf = C, re;
}
var te = {};
/** @license React v16.13.1
 * react-is.development.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var Fn;
function La() {
  return Fn || (Fn = 1, process.env.NODE_ENV !== "production" && function() {
    var e = typeof Symbol == "function" && Symbol.for, r = e ? Symbol.for("react.element") : 60103, t = e ? Symbol.for("react.portal") : 60106, n = e ? Symbol.for("react.fragment") : 60107, o = e ? Symbol.for("react.strict_mode") : 60108, i = e ? Symbol.for("react.profiler") : 60114, a = e ? Symbol.for("react.provider") : 60109, c = e ? Symbol.for("react.context") : 60110, u = e ? Symbol.for("react.async_mode") : 60111, f = e ? Symbol.for("react.concurrent_mode") : 60111, d = e ? Symbol.for("react.forward_ref") : 60112, p = e ? Symbol.for("react.suspense") : 60113, h = e ? Symbol.for("react.suspense_list") : 60120, E = e ? Symbol.for("react.memo") : 60115, g = e ? Symbol.for("react.lazy") : 60116, m = e ? Symbol.for("react.block") : 60121, y = e ? Symbol.for("react.fundamental") : 60117, S = e ? Symbol.for("react.responder") : 60118, w = e ? Symbol.for("react.scope") : 60119;
    function C(v) {
      return typeof v == "string" || typeof v == "function" || // Note: its typeof might be other than 'symbol' or 'number' if it's a polyfill.
      v === n || v === f || v === i || v === o || v === p || v === h || typeof v == "object" && v !== null && (v.$$typeof === g || v.$$typeof === E || v.$$typeof === a || v.$$typeof === c || v.$$typeof === d || v.$$typeof === y || v.$$typeof === S || v.$$typeof === w || v.$$typeof === m);
    }
    function _(v) {
      if (typeof v == "object" && v !== null) {
        var Ee = v.$$typeof;
        switch (Ee) {
          case r:
            var $ = v.type;
            switch ($) {
              case u:
              case f:
              case n:
              case i:
              case o:
              case p:
                return $;
              default:
                var Se = $ && $.$$typeof;
                switch (Se) {
                  case c:
                  case d:
                  case g:
                  case E:
                  case a:
                    return Se;
                  default:
                    return Ee;
                }
            }
          case t:
            return Ee;
        }
      }
    }
    var l = u, k = f, N = c, pe = a, ue = r, I = d, ae = n, me = g, oe = E, ie = t, le = i, se = o, be = p, fe = !1;
    function $e(v) {
      return fe || (fe = !0, console.warn("The ReactIs.isAsyncMode() alias has been deprecated, and will be removed in React 17+. Update your code to use ReactIs.isConcurrentMode() instead. It has the exact same API.")), x(v) || _(v) === u;
    }
    function x(v) {
      return _(v) === f;
    }
    function O(v) {
      return _(v) === c;
    }
    function B(v) {
      return _(v) === a;
    }
    function L(v) {
      return typeof v == "object" && v !== null && v.$$typeof === r;
    }
    function A(v) {
      return _(v) === d;
    }
    function Y(v) {
      return _(v) === n;
    }
    function M(v) {
      return _(v) === g;
    }
    function F(v) {
      return _(v) === E;
    }
    function z(v) {
      return _(v) === t;
    }
    function D(v) {
      return _(v) === i;
    }
    function U(v) {
      return _(v) === o;
    }
    function he(v) {
      return _(v) === p;
    }
    te.AsyncMode = l, te.ConcurrentMode = k, te.ContextConsumer = N, te.ContextProvider = pe, te.Element = ue, te.ForwardRef = I, te.Fragment = ae, te.Lazy = me, te.Memo = oe, te.Portal = ie, te.Profiler = le, te.StrictMode = se, te.Suspense = be, te.isAsyncMode = $e, te.isConcurrentMode = x, te.isContextConsumer = O, te.isContextProvider = B, te.isElement = L, te.isForwardRef = A, te.isFragment = Y, te.isLazy = M, te.isMemo = F, te.isPortal = z, te.isProfiler = D, te.isStrictMode = U, te.isSuspense = he, te.isValidElementType = C, te.typeOf = _;
  }()), te;
}
process.env.NODE_ENV === "production" ? Mt.exports = Va() : Mt.exports = La();
var za = Mt.exports, $o = za, Ba = {
  $$typeof: !0,
  render: !0,
  defaultProps: !0,
  displayName: !0,
  propTypes: !0
}, Ya = {
  $$typeof: !0,
  compare: !0,
  defaultProps: !0,
  displayName: !0,
  propTypes: !0,
  type: !0
}, Po = {};
Po[$o.ForwardRef] = Ba;
Po[$o.Memo] = Ya;
var Ua = !0;
function qt(e, r, t) {
  var n = "";
  return t.split(" ").forEach(function(o) {
    e[o] !== void 0 ? r.push(e[o] + ";") : n += o + " ";
  }), n;
}
var tt = function(r, t, n) {
  var o = r.key + "-" + t.name;
  // we only need to add the styles to the registered cache if the
  // class name could be used further down
  // the tree but if it's a string tag, we know it won't
  // so we don't have to add it to registered cache.
  // this improves memory usage since we can avoid storing the whole style string
  (n === !1 || // we need to always store it if we're in compat mode and
  // in node since emotion-server relies on whether a style is in
  // the registered cache to know whether a style is global or not
  // also, note that this check will be dead code eliminated in the browser
  Ua === !1) && r.registered[o] === void 0 && (r.registered[o] = t.styles);
}, nt = function(r, t, n) {
  tt(r, t, n);
  var o = r.key + "-" + t.name;
  if (r.inserted[t.name] === void 0) {
    var i = t;
    do
      r.insert(t === i ? "." + o : "", i, r.sheet, !0), i = i.next;
    while (i !== void 0);
  }
};
function Wa(e) {
  for (var r = 0, t, n = 0, o = e.length; o >= 4; ++n, o -= 4)
    t = e.charCodeAt(n) & 255 | (e.charCodeAt(++n) & 255) << 8 | (e.charCodeAt(++n) & 255) << 16 | (e.charCodeAt(++n) & 255) << 24, t = /* Math.imul(k, m): */
    (t & 65535) * 1540483477 + ((t >>> 16) * 59797 << 16), t ^= /* k >>> r: */
    t >>> 24, r = /* Math.imul(k, m): */
    (t & 65535) * 1540483477 + ((t >>> 16) * 59797 << 16) ^ /* Math.imul(h, m): */
    (r & 65535) * 1540483477 + ((r >>> 16) * 59797 << 16);
  switch (o) {
    case 3:
      r ^= (e.charCodeAt(n + 2) & 255) << 16;
    case 2:
      r ^= (e.charCodeAt(n + 1) & 255) << 8;
    case 1:
      r ^= e.charCodeAt(n) & 255, r = /* Math.imul(h, m): */
      (r & 65535) * 1540483477 + ((r >>> 16) * 59797 << 16);
  }
  return r ^= r >>> 13, r = /* Math.imul(h, m): */
  (r & 65535) * 1540483477 + ((r >>> 16) * 59797 << 16), ((r ^ r >>> 15) >>> 0).toString(36);
}
var qa = {
  animationIterationCount: 1,
  aspectRatio: 1,
  borderImageOutset: 1,
  borderImageSlice: 1,
  borderImageWidth: 1,
  boxFlex: 1,
  boxFlexGroup: 1,
  boxOrdinalGroup: 1,
  columnCount: 1,
  columns: 1,
  flex: 1,
  flexGrow: 1,
  flexPositive: 1,
  flexShrink: 1,
  flexNegative: 1,
  flexOrder: 1,
  gridRow: 1,
  gridRowEnd: 1,
  gridRowSpan: 1,
  gridRowStart: 1,
  gridColumn: 1,
  gridColumnEnd: 1,
  gridColumnSpan: 1,
  gridColumnStart: 1,
  msGridRow: 1,
  msGridRowSpan: 1,
  msGridColumn: 1,
  msGridColumnSpan: 1,
  fontWeight: 1,
  lineHeight: 1,
  opacity: 1,
  order: 1,
  orphans: 1,
  tabSize: 1,
  widows: 1,
  zIndex: 1,
  zoom: 1,
  WebkitLineClamp: 1,
  // SVG-related properties
  fillOpacity: 1,
  floodOpacity: 1,
  stopOpacity: 1,
  strokeDasharray: 1,
  strokeDashoffset: 1,
  strokeMiterlimit: 1,
  strokeOpacity: 1,
  strokeWidth: 1
}, Vn = `You have illegal escape sequence in your template literal, most likely inside content's property value.
Because you write your CSS inside a JavaScript string you actually have to do double escaping, so for example "content: '\\00d7';" should become "content: '\\\\00d7';".
You can read more about this here:
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals#ES2018_revision_of_illegal_escape_sequences`, Ka = "You have passed in falsy value as style object's key (can happen when in example you pass unexported component as computed key).", Ga = /[A-Z]|^ms/g, ko = /_EMO_([^_]+?)_([^]*?)_EMO_/g, Kt = function(r) {
  return r.charCodeAt(1) === 45;
}, Ln = function(r) {
  return r != null && typeof r != "boolean";
}, Ot = /* @__PURE__ */ xo(function(e) {
  return Kt(e) ? e : e.replace(Ga, "-$&").toLowerCase();
}), Hr = function(r, t) {
  switch (r) {
    case "animation":
    case "animationName":
      if (typeof t == "string")
        return t.replace(ko, function(n, o, i) {
          return Fe = {
            name: o,
            styles: i,
            next: Fe
          }, o;
        });
  }
  return qa[r] !== 1 && !Kt(r) && typeof t == "number" && t !== 0 ? t + "px" : t;
};
if (process.env.NODE_ENV !== "production") {
  var Ha = /(var|attr|counters?|url|element|(((repeating-)?(linear|radial))|conic)-gradient)\(|(no-)?(open|close)-quote/, Xa = ["normal", "none", "initial", "inherit", "unset"], Ja = Hr, Za = /^-ms-/, Qa = /-(.)/g, zn = {};
  Hr = function(r, t) {
    if (r === "content" && (typeof t != "string" || Xa.indexOf(t) === -1 && !Ha.test(t) && (t.charAt(0) !== t.charAt(t.length - 1) || t.charAt(0) !== '"' && t.charAt(0) !== "'")))
      throw new Error("You seem to be using a value for 'content' without quotes, try replacing it with `content: '\"" + t + "\"'`");
    var n = Ja(r, t);
    return n !== "" && !Kt(r) && r.indexOf("-") !== -1 && zn[r] === void 0 && (zn[r] = !0, console.error("Using kebab-case for css properties in objects is not supported. Did you mean " + r.replace(Za, "ms-").replace(Qa, function(o, i) {
      return i.toUpperCase();
    }) + "?")), n;
  };
}
var Ao = "Component selectors can only be used in conjunction with @emotion/babel-plugin, the swc Emotion plugin, or another Emotion-aware compiler transform.";
function Pr(e, r, t) {
  if (t == null)
    return "";
  if (t.__emotion_styles !== void 0) {
    if (process.env.NODE_ENV !== "production" && t.toString() === "NO_COMPONENT_SELECTOR")
      throw new Error(Ao);
    return t;
  }
  switch (typeof t) {
    case "boolean":
      return "";
    case "object": {
      if (t.anim === 1)
        return Fe = {
          name: t.name,
          styles: t.styles,
          next: Fe
        }, t.name;
      if (t.styles !== void 0) {
        var n = t.next;
        if (n !== void 0)
          for (; n !== void 0; )
            Fe = {
              name: n.name,
              styles: n.styles,
              next: Fe
            }, n = n.next;
        var o = t.styles + ";";
        return process.env.NODE_ENV !== "production" && t.map !== void 0 && (o += t.map), o;
      }
      return es(e, r, t);
    }
    case "function": {
      if (e !== void 0) {
        var i = Fe, a = t(e);
        return Fe = i, Pr(e, r, a);
      } else
        process.env.NODE_ENV !== "production" && console.error("Functions that are interpolated in css calls will be stringified.\nIf you want to have a css call based on props, create a function that returns a css call like this\nlet dynamicStyle = (props) => css`color: ${props.color}`\nIt can be called directly with props or interpolated in a styled call like this\nlet SomeComponent = styled('div')`${dynamicStyle}`");
      break;
    }
    case "string":
      if (process.env.NODE_ENV !== "production") {
        var c = [], u = t.replace(ko, function(d, p, h) {
          var E = "animation" + c.length;
          return c.push("const " + E + " = keyframes`" + h.replace(/^@keyframes animation-\w+/, "") + "`"), "${" + E + "}";
        });
        c.length && console.error("`keyframes` output got interpolated into plain string, please wrap it with `css`.\n\nInstead of doing this:\n\n" + [].concat(c, ["`" + u + "`"]).join(`
`) + `

You should wrap it with \`css\` like this:

` + ("css`" + u + "`"));
      }
      break;
  }
  if (r == null)
    return t;
  var f = r[t];
  return f !== void 0 ? f : t;
}
function es(e, r, t) {
  var n = "";
  if (Array.isArray(t))
    for (var o = 0; o < t.length; o++)
      n += Pr(e, r, t[o]) + ";";
  else
    for (var i in t) {
      var a = t[i];
      if (typeof a != "object")
        r != null && r[a] !== void 0 ? n += i + "{" + r[a] + "}" : Ln(a) && (n += Ot(i) + ":" + Hr(i, a) + ";");
      else {
        if (i === "NO_COMPONENT_SELECTOR" && process.env.NODE_ENV !== "production")
          throw new Error(Ao);
        if (Array.isArray(a) && typeof a[0] == "string" && (r == null || r[a[0]] === void 0))
          for (var c = 0; c < a.length; c++)
            Ln(a[c]) && (n += Ot(i) + ":" + Hr(i, a[c]) + ";");
        else {
          var u = Pr(e, r, a);
          switch (i) {
            case "animation":
            case "animationName": {
              n += Ot(i) + ":" + u + ";";
              break;
            }
            default:
              process.env.NODE_ENV !== "production" && i === "undefined" && console.error(Ka), n += i + "{" + u + "}";
          }
        }
      }
    }
  return n;
}
var Bn = /label:\s*([^\s;\n{]+)\s*(;|$)/g, No;
process.env.NODE_ENV !== "production" && (No = /\/\*#\ssourceMappingURL=data:application\/json;\S+\s+\*\//g);
var Fe, dr = function(r, t, n) {
  if (r.length === 1 && typeof r[0] == "object" && r[0] !== null && r[0].styles !== void 0)
    return r[0];
  var o = !0, i = "";
  Fe = void 0;
  var a = r[0];
  a == null || a.raw === void 0 ? (o = !1, i += Pr(n, t, a)) : (process.env.NODE_ENV !== "production" && a[0] === void 0 && console.error(Vn), i += a[0]);
  for (var c = 1; c < r.length; c++)
    i += Pr(n, t, r[c]), o && (process.env.NODE_ENV !== "production" && a[c] === void 0 && console.error(Vn), i += a[c]);
  var u;
  process.env.NODE_ENV !== "production" && (i = i.replace(No, function(h) {
    return u = h, "";
  })), Bn.lastIndex = 0;
  for (var f = "", d; (d = Bn.exec(i)) !== null; )
    f += "-" + // $FlowFixMe we know it's not null
    d[1];
  var p = Wa(i) + f;
  return process.env.NODE_ENV !== "production" ? {
    name: p,
    styles: i,
    map: u,
    next: Fe,
    toString: function() {
      return "You have tried to stringify object returned from `css` function. It isn't supposed to be used directly (e.g. as value of the `className` prop), but rather handed to emotion so it can handle it (e.g. as value of `css` prop).";
    }
  } : {
    name: p,
    styles: i,
    next: Fe
  };
}, rs = function(r) {
  return r();
}, Io = P["useInsertionEffect"] ? P["useInsertionEffect"] : !1, Gt = Io || rs, Yn = Io || P.useLayoutEffect, ts = {}.hasOwnProperty, Ht = /* @__PURE__ */ P.createContext(
  // we're doing this to avoid preconstruct's dead code elimination in this one case
  // because this module is primarily intended for the browser and node
  // but it's also required in react native and similar environments sometimes
  // and we could have a special build just for that
  // but this is much easier and the native packages
  // might use a different theme context in the future anyway
  typeof HTMLElement < "u" ? /* @__PURE__ */ Fa({
    key: "css"
  }) : null
);
process.env.NODE_ENV !== "production" && (Ht.displayName = "EmotionCacheContext");
Ht.Provider;
var ot = function(r) {
  return /* @__PURE__ */ si(function(t, n) {
    var o = ci(Ht);
    return r(t, o, n);
  });
}, mr = /* @__PURE__ */ P.createContext({});
process.env.NODE_ENV !== "production" && (mr.displayName = "EmotionThemeContext");
var Un = "__EMOTION_TYPE_PLEASE_DO_NOT_USE__", Wn = "__EMOTION_LABEL_PLEASE_DO_NOT_USE__", ns = function(r) {
  var t = r.cache, n = r.serialized, o = r.isStringTag;
  return tt(t, n, o), Gt(function() {
    return nt(t, n, o);
  }), null;
}, os = /* @__PURE__ */ ot(function(e, r, t) {
  var n = e.css;
  typeof n == "string" && r.registered[n] !== void 0 && (n = r.registered[n]);
  var o = e[Un], i = [n], a = "";
  typeof e.className == "string" ? a = qt(r.registered, i, e.className) : e.className != null && (a = e.className + " ");
  var c = dr(i, void 0, P.useContext(mr));
  if (process.env.NODE_ENV !== "production" && c.name.indexOf("-") === -1) {
    var u = e[Wn];
    u && (c = dr([c, "label:" + u + ";"]));
  }
  a += r.key + "-" + c.name;
  var f = {};
  for (var d in e)
    ts.call(e, d) && d !== "css" && d !== Un && (process.env.NODE_ENV === "production" || d !== Wn) && (f[d] = e[d]);
  return f.ref = t, f.className = a, /* @__PURE__ */ P.createElement(P.Fragment, null, /* @__PURE__ */ P.createElement(ns, {
    cache: r,
    serialized: c,
    isStringTag: typeof o == "string"
  }), /* @__PURE__ */ P.createElement(o, f));
});
process.env.NODE_ENV !== "production" && (os.displayName = "EmotionCssPropInternal");
var is = {
  name: "@emotion/react",
  version: "11.11.1",
  main: "dist/emotion-react.cjs.js",
  module: "dist/emotion-react.esm.js",
  browser: {
    "./dist/emotion-react.esm.js": "./dist/emotion-react.browser.esm.js"
  },
  exports: {
    ".": {
      module: {
        worker: "./dist/emotion-react.worker.esm.js",
        browser: "./dist/emotion-react.browser.esm.js",
        default: "./dist/emotion-react.esm.js"
      },
      import: "./dist/emotion-react.cjs.mjs",
      default: "./dist/emotion-react.cjs.js"
    },
    "./jsx-runtime": {
      module: {
        worker: "./jsx-runtime/dist/emotion-react-jsx-runtime.worker.esm.js",
        browser: "./jsx-runtime/dist/emotion-react-jsx-runtime.browser.esm.js",
        default: "./jsx-runtime/dist/emotion-react-jsx-runtime.esm.js"
      },
      import: "./jsx-runtime/dist/emotion-react-jsx-runtime.cjs.mjs",
      default: "./jsx-runtime/dist/emotion-react-jsx-runtime.cjs.js"
    },
    "./_isolated-hnrs": {
      module: {
        worker: "./_isolated-hnrs/dist/emotion-react-_isolated-hnrs.worker.esm.js",
        browser: "./_isolated-hnrs/dist/emotion-react-_isolated-hnrs.browser.esm.js",
        default: "./_isolated-hnrs/dist/emotion-react-_isolated-hnrs.esm.js"
      },
      import: "./_isolated-hnrs/dist/emotion-react-_isolated-hnrs.cjs.mjs",
      default: "./_isolated-hnrs/dist/emotion-react-_isolated-hnrs.cjs.js"
    },
    "./jsx-dev-runtime": {
      module: {
        worker: "./jsx-dev-runtime/dist/emotion-react-jsx-dev-runtime.worker.esm.js",
        browser: "./jsx-dev-runtime/dist/emotion-react-jsx-dev-runtime.browser.esm.js",
        default: "./jsx-dev-runtime/dist/emotion-react-jsx-dev-runtime.esm.js"
      },
      import: "./jsx-dev-runtime/dist/emotion-react-jsx-dev-runtime.cjs.mjs",
      default: "./jsx-dev-runtime/dist/emotion-react-jsx-dev-runtime.cjs.js"
    },
    "./package.json": "./package.json",
    "./types/css-prop": "./types/css-prop.d.ts",
    "./macro": {
      types: {
        import: "./macro.d.mts",
        default: "./macro.d.ts"
      },
      default: "./macro.js"
    }
  },
  types: "types/index.d.ts",
  files: [
    "src",
    "dist",
    "jsx-runtime",
    "jsx-dev-runtime",
    "_isolated-hnrs",
    "types/*.d.ts",
    "macro.*"
  ],
  sideEffects: !1,
  author: "Emotion Contributors",
  license: "MIT",
  scripts: {
    "test:typescript": "dtslint types"
  },
  dependencies: {
    "@babel/runtime": "^7.18.3",
    "@emotion/babel-plugin": "^11.11.0",
    "@emotion/cache": "^11.11.0",
    "@emotion/serialize": "^1.1.2",
    "@emotion/use-insertion-effect-with-fallbacks": "^1.0.1",
    "@emotion/utils": "^1.2.1",
    "@emotion/weak-memoize": "^0.3.1",
    "hoist-non-react-statics": "^3.3.1"
  },
  peerDependencies: {
    react: ">=16.8.0"
  },
  peerDependenciesMeta: {
    "@types/react": {
      optional: !0
    }
  },
  devDependencies: {
    "@definitelytyped/dtslint": "0.0.112",
    "@emotion/css": "11.11.0",
    "@emotion/css-prettifier": "1.1.3",
    "@emotion/server": "11.11.0",
    "@emotion/styled": "11.11.0",
    "html-tag-names": "^1.1.2",
    react: "16.14.0",
    "svg-tag-names": "^1.1.1",
    typescript: "^4.5.5"
  },
  repository: "https://github.com/emotion-js/emotion/tree/main/packages/react",
  publishConfig: {
    access: "public"
  },
  "umd:main": "dist/emotion-react.umd.min.js",
  preconstruct: {
    entrypoints: [
      "./index.js",
      "./jsx-runtime.js",
      "./jsx-dev-runtime.js",
      "./_isolated-hnrs.js"
    ],
    umdName: "emotionReact",
    exports: {
      envConditions: [
        "browser",
        "worker"
      ],
      extra: {
        "./types/css-prop": "./types/css-prop.d.ts",
        "./macro": {
          types: {
            import: "./macro.d.mts",
            default: "./macro.d.ts"
          },
          default: "./macro.js"
        }
      }
    }
  }
}, qn = !1, as = /* @__PURE__ */ ot(function(e, r) {
  process.env.NODE_ENV !== "production" && !qn && // check for className as well since the user is
  // probably using the custom createElement which
  // means it will be turned into a className prop
  // $FlowFixMe I don't really want to add it to the type since it shouldn't be used
  (e.className || e.css) && (console.error("It looks like you're using the css prop on Global, did you mean to use the styles prop instead?"), qn = !0);
  var t = e.styles, n = dr([t], void 0, P.useContext(mr)), o = P.useRef();
  return Yn(function() {
    var i = r.key + "-global", a = new r.sheet.constructor({
      key: i,
      nonce: r.sheet.nonce,
      container: r.sheet.container,
      speedy: r.sheet.isSpeedy
    }), c = !1, u = document.querySelector('style[data-emotion="' + i + " " + n.name + '"]');
    return r.sheet.tags.length && (a.before = r.sheet.tags[0]), u !== null && (c = !0, u.setAttribute("data-emotion", i), a.hydrate([u])), o.current = [a, c], function() {
      a.flush();
    };
  }, [r]), Yn(function() {
    var i = o.current, a = i[0], c = i[1];
    if (c) {
      i[1] = !1;
      return;
    }
    if (n.next !== void 0 && nt(r, n.next, !0), a.tags.length) {
      var u = a.tags[a.tags.length - 1].nextElementSibling;
      a.before = u, a.flush();
    }
    r.insert("", n, a, !1);
  }, [r, n.name]), null;
});
process.env.NODE_ENV !== "production" && (as.displayName = "EmotionGlobal");
function ss() {
  for (var e = arguments.length, r = new Array(e), t = 0; t < e; t++)
    r[t] = arguments[t];
  return dr(r);
}
var Xt = function() {
  var r = ss.apply(void 0, arguments), t = "animation-" + r.name;
  return {
    name: t,
    styles: "@keyframes " + t + "{" + r.styles + "}",
    anim: 1,
    toString: function() {
      return "_EMO_" + this.name + "_" + this.styles + "_EMO_";
    }
  };
}, cs = function e(r) {
  for (var t = r.length, n = 0, o = ""; n < t; n++) {
    var i = r[n];
    if (i != null) {
      var a = void 0;
      switch (typeof i) {
        case "boolean":
          break;
        case "object": {
          if (Array.isArray(i))
            a = e(i);
          else {
            process.env.NODE_ENV !== "production" && i.styles !== void 0 && i.name !== void 0 && console.error("You have passed styles created with `css` from `@emotion/react` package to the `cx`.\n`cx` is meant to compose class names (strings) so you should convert those styles to a class name by passing them to the `css` received from <ClassNames/> component."), a = "";
            for (var c in i)
              i[c] && c && (a && (a += " "), a += c);
          }
          break;
        }
        default:
          a = i;
      }
      a && (o && (o += " "), o += a);
    }
  }
  return o;
};
function us(e, r, t) {
  var n = [], o = qt(e, n, t);
  return n.length < 2 ? t : o + r(n);
}
var ls = function(r) {
  var t = r.cache, n = r.serializedArr;
  return Gt(function() {
    for (var o = 0; o < n.length; o++)
      nt(t, n[o], !1);
  }), null;
}, fs = /* @__PURE__ */ ot(function(e, r) {
  var t = !1, n = [], o = function() {
    if (t && process.env.NODE_ENV !== "production")
      throw new Error("css can only be used during render");
    for (var f = arguments.length, d = new Array(f), p = 0; p < f; p++)
      d[p] = arguments[p];
    var h = dr(d, r.registered);
    return n.push(h), tt(r, h, !1), r.key + "-" + h.name;
  }, i = function() {
    if (t && process.env.NODE_ENV !== "production")
      throw new Error("cx can only be used during render");
    for (var f = arguments.length, d = new Array(f), p = 0; p < f; p++)
      d[p] = arguments[p];
    return us(r.registered, o, cs(d));
  }, a = {
    css: o,
    cx: i,
    theme: P.useContext(mr)
  }, c = e.children(a);
  return t = !0, /* @__PURE__ */ P.createElement(P.Fragment, null, /* @__PURE__ */ P.createElement(ls, {
    cache: r,
    serializedArr: n
  }), c);
});
process.env.NODE_ENV !== "production" && (fs.displayName = "EmotionClassNames");
if (process.env.NODE_ENV !== "production") {
  var Kn = !0, ds = typeof jest < "u" || typeof vi < "u";
  if (Kn && !ds) {
    var Gn = (
      // $FlowIgnore
      typeof globalThis < "u" ? globalThis : Kn ? window : global
    ), Hn = "__EMOTION_REACT_" + is.version.split(".")[0] + "__";
    Gn[Hn] && console.warn("You are loading @emotion/react when it is already loaded. Running multiple instances may cause problems. This can happen if multiple versions are used, or if multiple builds of the same version are used."), Gn[Hn] = !0;
  }
}
var ps = oa, ms = function(r) {
  return r !== "theme";
}, Xn = function(r) {
  return typeof r == "string" && // 96 is one less than the char code
  // for "a" so this is checking that
  // it's a lowercase character
  r.charCodeAt(0) > 96 ? ps : ms;
}, Jn = function(r, t, n) {
  var o;
  if (t) {
    var i = t.shouldForwardProp;
    o = r.__emotion_forwardProp && i ? function(a) {
      return r.__emotion_forwardProp(a) && i(a);
    } : i;
  }
  return typeof o != "function" && n && (o = r.__emotion_forwardProp), o;
}, Zn = `You have illegal escape sequence in your template literal, most likely inside content's property value.
Because you write your CSS inside a JavaScript string you actually have to do double escaping, so for example "content: '\\00d7';" should become "content: '\\\\00d7';".
You can read more about this here:
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals#ES2018_revision_of_illegal_escape_sequences`, hs = function(r) {
  var t = r.cache, n = r.serialized, o = r.isStringTag;
  return tt(t, n, o), Gt(function() {
    return nt(t, n, o);
  }), null;
}, ys = function e(r, t) {
  if (process.env.NODE_ENV !== "production" && r === void 0)
    throw new Error(`You are trying to create a styled element with an undefined component.
You may have forgotten to import it.`);
  var n = r.__emotion_real === r, o = n && r.__emotion_base || r, i, a;
  t !== void 0 && (i = t.label, a = t.target);
  var c = Jn(r, t, n), u = c || Xn(o), f = !u("as");
  return function() {
    var d = arguments, p = n && r.__emotion_styles !== void 0 ? r.__emotion_styles.slice(0) : [];
    if (i !== void 0 && p.push("label:" + i + ";"), d[0] == null || d[0].raw === void 0)
      p.push.apply(p, d);
    else {
      process.env.NODE_ENV !== "production" && d[0][0] === void 0 && console.error(Zn), p.push(d[0][0]);
      for (var h = d.length, E = 1; E < h; E++)
        process.env.NODE_ENV !== "production" && d[0][E] === void 0 && console.error(Zn), p.push(d[E], d[0][E]);
    }
    var g = ot(function(m, y, S) {
      var w = f && m.as || o, C = "", _ = [], l = m;
      if (m.theme == null) {
        l = {};
        for (var k in m)
          l[k] = m[k];
        l.theme = P.useContext(mr);
      }
      typeof m.className == "string" ? C = qt(y.registered, _, m.className) : m.className != null && (C = m.className + " ");
      var N = dr(p.concat(_), y.registered, l);
      C += y.key + "-" + N.name, a !== void 0 && (C += " " + a);
      var pe = f && c === void 0 ? Xn(w) : u, ue = {};
      for (var I in m)
        f && I === "as" || // $FlowFixMe
        pe(I) && (ue[I] = m[I]);
      return ue.className = C, ue.ref = S, /* @__PURE__ */ P.createElement(P.Fragment, null, /* @__PURE__ */ P.createElement(hs, {
        cache: y,
        serialized: N,
        isStringTag: typeof w == "string"
      }), /* @__PURE__ */ P.createElement(w, ue));
    });
    return g.displayName = i !== void 0 ? i : "Styled(" + (typeof o == "string" ? o : o.displayName || o.name || "Component") + ")", g.defaultProps = r.defaultProps, g.__emotion_real = g, g.__emotion_base = o, g.__emotion_styles = p, g.__emotion_forwardProp = c, Object.defineProperty(g, "toString", {
      value: function() {
        return a === void 0 && process.env.NODE_ENV !== "production" ? "NO_COMPONENT_SELECTOR" : "." + a;
      }
    }), g.withComponent = function(m, y) {
      return e(m, j({}, t, y, {
        shouldForwardProp: Jn(g, y, !0)
      })).apply(void 0, p);
    }, g;
  };
}, vs = [
  "a",
  "abbr",
  "address",
  "area",
  "article",
  "aside",
  "audio",
  "b",
  "base",
  "bdi",
  "bdo",
  "big",
  "blockquote",
  "body",
  "br",
  "button",
  "canvas",
  "caption",
  "cite",
  "code",
  "col",
  "colgroup",
  "data",
  "datalist",
  "dd",
  "del",
  "details",
  "dfn",
  "dialog",
  "div",
  "dl",
  "dt",
  "em",
  "embed",
  "fieldset",
  "figcaption",
  "figure",
  "footer",
  "form",
  "h1",
  "h2",
  "h3",
  "h4",
  "h5",
  "h6",
  "head",
  "header",
  "hgroup",
  "hr",
  "html",
  "i",
  "iframe",
  "img",
  "input",
  "ins",
  "kbd",
  "keygen",
  "label",
  "legend",
  "li",
  "link",
  "main",
  "map",
  "mark",
  "marquee",
  "menu",
  "menuitem",
  "meta",
  "meter",
  "nav",
  "noscript",
  "object",
  "ol",
  "optgroup",
  "option",
  "output",
  "p",
  "param",
  "picture",
  "pre",
  "progress",
  "q",
  "rp",
  "rt",
  "ruby",
  "s",
  "samp",
  "script",
  "section",
  "select",
  "small",
  "source",
  "span",
  "strong",
  "style",
  "sub",
  "summary",
  "sup",
  "table",
  "tbody",
  "td",
  "textarea",
  "tfoot",
  "th",
  "thead",
  "time",
  "title",
  "tr",
  "track",
  "u",
  "ul",
  "var",
  "video",
  "wbr",
  // SVG
  "circle",
  "clipPath",
  "defs",
  "ellipse",
  "foreignObject",
  "g",
  "image",
  "line",
  "linearGradient",
  "mask",
  "path",
  "pattern",
  "polygon",
  "polyline",
  "radialGradient",
  "rect",
  "stop",
  "svg",
  "text",
  "tspan"
], jt = ys.bind();
vs.forEach(function(e) {
  jt[e] = jt(e);
});
/**
 * @mui/styled-engine v5.13.2
 *
 * @license MIT
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
function gs(e, r) {
  const t = jt(e, r);
  return process.env.NODE_ENV !== "production" ? (...n) => {
    const o = typeof e == "string" ? `"${e}"` : "component";
    return n.length === 0 ? console.error([`MUI: Seems like you called \`styled(${o})()\` without a \`style\` argument.`, 'You must provide a `styles` argument: `styled("div")(styleYouForgotToPass)`.'].join(`
`)) : n.some((i) => i === void 0) && console.error(`MUI: the styled(${o})(...args) API requires all its args to be defined.`), t(...n);
  } : t;
}
const bs = (e, r) => {
  Array.isArray(e.__emotion_styles) && (e.__emotion_styles = r(e.__emotion_styles));
}, Es = ["values", "unit", "step"], xs = (e) => {
  const r = Object.keys(e).map((t) => ({
    key: t,
    val: e[t]
  })) || [];
  return r.sort((t, n) => t.val - n.val), r.reduce((t, n) => j({}, t, {
    [n.key]: n.val
  }), {});
};
function Ts(e) {
  const {
    // The breakpoint **start** at this value.
    // For instance with the first breakpoint xs: [xs, sm).
    values: r = {
      xs: 0,
      // phone
      sm: 600,
      // tablet
      md: 900,
      // small laptop
      lg: 1200,
      // desktop
      xl: 1536
      // large screen
    },
    unit: t = "px",
    step: n = 5
  } = e, o = je(e, Es), i = xs(r), a = Object.keys(i);
  function c(h) {
    return `@media (min-width:${typeof r[h] == "number" ? r[h] : h}${t})`;
  }
  function u(h) {
    return `@media (max-width:${(typeof r[h] == "number" ? r[h] : h) - n / 100}${t})`;
  }
  function f(h, E) {
    const g = a.indexOf(E);
    return `@media (min-width:${typeof r[h] == "number" ? r[h] : h}${t}) and (max-width:${(g !== -1 && typeof r[a[g]] == "number" ? r[a[g]] : E) - n / 100}${t})`;
  }
  function d(h) {
    return a.indexOf(h) + 1 < a.length ? f(h, a[a.indexOf(h) + 1]) : c(h);
  }
  function p(h) {
    const E = a.indexOf(h);
    return E === 0 ? c(a[1]) : E === a.length - 1 ? u(a[E]) : f(h, a[a.indexOf(h) + 1]).replace("@media", "@media not all and");
  }
  return j({
    keys: a,
    values: i,
    up: c,
    down: u,
    between: f,
    only: d,
    not: p,
    unit: t
  }, o);
}
const _s = {
  borderRadius: 4
}, Ss = _s, Rs = process.env.NODE_ENV !== "production" ? b.oneOfType([b.number, b.string, b.object, b.array]) : {}, Ke = Rs;
function Cr(e, r) {
  return r ? Ye(e, r, {
    clone: !1
    // No need to clone deep, it's way faster.
  }) : e;
}
const Jt = {
  xs: 0,
  // phone
  sm: 600,
  // tablet
  md: 900,
  // small laptop
  lg: 1200,
  // desktop
  xl: 1536
  // large screen
}, Qn = {
  // Sorted ASC by size. That's important.
  // It can't be configured as it's used statically for propTypes.
  keys: ["xs", "sm", "md", "lg", "xl"],
  up: (e) => `@media (min-width:${Jt[e]}px)`
};
function Ue(e, r, t) {
  const n = e.theme || {};
  if (Array.isArray(r)) {
    const i = n.breakpoints || Qn;
    return r.reduce((a, c, u) => (a[i.up(i.keys[u])] = t(r[u]), a), {});
  }
  if (typeof r == "object") {
    const i = n.breakpoints || Qn;
    return Object.keys(r).reduce((a, c) => {
      if (Object.keys(i.values || Jt).indexOf(c) !== -1) {
        const u = i.up(c);
        a[u] = t(r[c], c);
      } else {
        const u = c;
        a[u] = r[u];
      }
      return a;
    }, {});
  }
  return t(r);
}
function Cs(e = {}) {
  var r;
  return ((r = e.keys) == null ? void 0 : r.reduce((n, o) => {
    const i = e.up(o);
    return n[i] = {}, n;
  }, {})) || {};
}
function Os(e, r) {
  return e.reduce((t, n) => {
    const o = t[n];
    return (!o || Object.keys(o).length === 0) && delete t[n], t;
  }, r);
}
function it(e, r, t = !0) {
  if (!r || typeof r != "string")
    return null;
  if (e && e.vars && t) {
    const n = `vars.${r}`.split(".").reduce((o, i) => o && o[i] ? o[i] : null, e);
    if (n != null)
      return n;
  }
  return r.split(".").reduce((n, o) => n && n[o] != null ? n[o] : null, e);
}
function Xr(e, r, t, n = t) {
  let o;
  return typeof e == "function" ? o = e(t) : Array.isArray(e) ? o = e[t] || n : o = it(e, t) || n, r && (o = r(o, n, e)), o;
}
function ne(e) {
  const {
    prop: r,
    cssProperty: t = e.prop,
    themeKey: n,
    transform: o
  } = e, i = (a) => {
    if (a[r] == null)
      return null;
    const c = a[r], u = a.theme, f = it(u, n) || {};
    return Ue(a, c, (p) => {
      let h = Xr(f, o, p);
      return p === h && typeof p == "string" && (h = Xr(f, o, `${r}${p === "default" ? "" : Oe(p)}`, p)), t === !1 ? h : {
        [t]: h
      };
    });
  };
  return i.propTypes = process.env.NODE_ENV !== "production" ? {
    [r]: Ke
  } : {}, i.filterProps = [r], i;
}
function ws(e) {
  const r = {};
  return (t) => (r[t] === void 0 && (r[t] = e(t)), r[t]);
}
const $s = {
  m: "margin",
  p: "padding"
}, Ps = {
  t: "Top",
  r: "Right",
  b: "Bottom",
  l: "Left",
  x: ["Left", "Right"],
  y: ["Top", "Bottom"]
}, eo = {
  marginX: "mx",
  marginY: "my",
  paddingX: "px",
  paddingY: "py"
}, ks = ws((e) => {
  if (e.length > 2)
    if (eo[e])
      e = eo[e];
    else
      return [e];
  const [r, t] = e.split(""), n = $s[r], o = Ps[t] || "";
  return Array.isArray(o) ? o.map((i) => n + i) : [n + o];
}), at = ["m", "mt", "mr", "mb", "ml", "mx", "my", "margin", "marginTop", "marginRight", "marginBottom", "marginLeft", "marginX", "marginY", "marginInline", "marginInlineStart", "marginInlineEnd", "marginBlock", "marginBlockStart", "marginBlockEnd"], st = ["p", "pt", "pr", "pb", "pl", "px", "py", "padding", "paddingTop", "paddingRight", "paddingBottom", "paddingLeft", "paddingX", "paddingY", "paddingInline", "paddingInlineStart", "paddingInlineEnd", "paddingBlock", "paddingBlockStart", "paddingBlockEnd"], As = [...at, ...st];
function Ar(e, r, t, n) {
  var o;
  const i = (o = it(e, r, !1)) != null ? o : t;
  return typeof i == "number" ? (a) => typeof a == "string" ? a : (process.env.NODE_ENV !== "production" && typeof a != "number" && console.error(`MUI: Expected ${n} argument to be a number or a string, got ${a}.`), i * a) : Array.isArray(i) ? (a) => typeof a == "string" ? a : (process.env.NODE_ENV !== "production" && (Number.isInteger(a) ? a > i.length - 1 && console.error([`MUI: The value provided (${a}) overflows.`, `The supported values are: ${JSON.stringify(i)}.`, `${a} > ${i.length - 1}, you need to add the missing values.`].join(`
`)) : console.error([`MUI: The \`theme.${r}\` array type cannot be combined with non integer values.You should either use an integer value that can be used as index, or define the \`theme.${r}\` as a number.`].join(`
`))), i[a]) : typeof i == "function" ? i : (process.env.NODE_ENV !== "production" && console.error([`MUI: The \`theme.${r}\` value (${i}) is invalid.`, "It should be a number, an array or a function."].join(`
`)), () => {
  });
}
function Mo(e) {
  return Ar(e, "spacing", 8, "spacing");
}
function Nr(e, r) {
  if (typeof r == "string" || r == null)
    return r;
  const t = Math.abs(r), n = e(t);
  return r >= 0 ? n : typeof n == "number" ? -n : `-${n}`;
}
function Ns(e, r) {
  return (t) => e.reduce((n, o) => (n[o] = Nr(r, t), n), {});
}
function Is(e, r, t, n) {
  if (r.indexOf(t) === -1)
    return null;
  const o = ks(t), i = Ns(o, n), a = e[t];
  return Ue(e, a, i);
}
function jo(e, r) {
  const t = Mo(e.theme);
  return Object.keys(e).map((n) => Is(e, r, n, t)).reduce(Cr, {});
}
function ye(e) {
  return jo(e, at);
}
ye.propTypes = process.env.NODE_ENV !== "production" ? at.reduce((e, r) => (e[r] = Ke, e), {}) : {};
ye.filterProps = at;
function ve(e) {
  return jo(e, st);
}
ve.propTypes = process.env.NODE_ENV !== "production" ? st.reduce((e, r) => (e[r] = Ke, e), {}) : {};
ve.filterProps = st;
process.env.NODE_ENV !== "production" && As.reduce((e, r) => (e[r] = Ke, e), {});
function Ms(e = 8) {
  if (e.mui)
    return e;
  const r = Mo({
    spacing: e
  }), t = (...n) => (process.env.NODE_ENV !== "production" && (n.length <= 4 || console.error(`MUI: Too many arguments provided, expected between 0 and 4, got ${n.length}`)), (n.length === 0 ? [1] : n).map((i) => {
    const a = r(i);
    return typeof a == "number" ? `${a}px` : a;
  }).join(" "));
  return t.mui = !0, t;
}
function ct(...e) {
  const r = e.reduce((n, o) => (o.filterProps.forEach((i) => {
    n[i] = o;
  }), n), {}), t = (n) => Object.keys(n).reduce((o, i) => r[i] ? Cr(o, r[i](n)) : o, {});
  return t.propTypes = process.env.NODE_ENV !== "production" ? e.reduce((n, o) => Object.assign(n, o.propTypes), {}) : {}, t.filterProps = e.reduce((n, o) => n.concat(o.filterProps), []), t;
}
function Le(e) {
  return typeof e != "number" ? e : `${e}px solid`;
}
const js = ne({
  prop: "border",
  themeKey: "borders",
  transform: Le
}), Ds = ne({
  prop: "borderTop",
  themeKey: "borders",
  transform: Le
}), Fs = ne({
  prop: "borderRight",
  themeKey: "borders",
  transform: Le
}), Vs = ne({
  prop: "borderBottom",
  themeKey: "borders",
  transform: Le
}), Ls = ne({
  prop: "borderLeft",
  themeKey: "borders",
  transform: Le
}), zs = ne({
  prop: "borderColor",
  themeKey: "palette"
}), Bs = ne({
  prop: "borderTopColor",
  themeKey: "palette"
}), Ys = ne({
  prop: "borderRightColor",
  themeKey: "palette"
}), Us = ne({
  prop: "borderBottomColor",
  themeKey: "palette"
}), Ws = ne({
  prop: "borderLeftColor",
  themeKey: "palette"
}), ut = (e) => {
  if (e.borderRadius !== void 0 && e.borderRadius !== null) {
    const r = Ar(e.theme, "shape.borderRadius", 4, "borderRadius"), t = (n) => ({
      borderRadius: Nr(r, n)
    });
    return Ue(e, e.borderRadius, t);
  }
  return null;
};
ut.propTypes = process.env.NODE_ENV !== "production" ? {
  borderRadius: Ke
} : {};
ut.filterProps = ["borderRadius"];
ct(js, Ds, Fs, Vs, Ls, zs, Bs, Ys, Us, Ws, ut);
const lt = (e) => {
  if (e.gap !== void 0 && e.gap !== null) {
    const r = Ar(e.theme, "spacing", 8, "gap"), t = (n) => ({
      gap: Nr(r, n)
    });
    return Ue(e, e.gap, t);
  }
  return null;
};
lt.propTypes = process.env.NODE_ENV !== "production" ? {
  gap: Ke
} : {};
lt.filterProps = ["gap"];
const ft = (e) => {
  if (e.columnGap !== void 0 && e.columnGap !== null) {
    const r = Ar(e.theme, "spacing", 8, "columnGap"), t = (n) => ({
      columnGap: Nr(r, n)
    });
    return Ue(e, e.columnGap, t);
  }
  return null;
};
ft.propTypes = process.env.NODE_ENV !== "production" ? {
  columnGap: Ke
} : {};
ft.filterProps = ["columnGap"];
const dt = (e) => {
  if (e.rowGap !== void 0 && e.rowGap !== null) {
    const r = Ar(e.theme, "spacing", 8, "rowGap"), t = (n) => ({
      rowGap: Nr(r, n)
    });
    return Ue(e, e.rowGap, t);
  }
  return null;
};
dt.propTypes = process.env.NODE_ENV !== "production" ? {
  rowGap: Ke
} : {};
dt.filterProps = ["rowGap"];
const qs = ne({
  prop: "gridColumn"
}), Ks = ne({
  prop: "gridRow"
}), Gs = ne({
  prop: "gridAutoFlow"
}), Hs = ne({
  prop: "gridAutoColumns"
}), Xs = ne({
  prop: "gridAutoRows"
}), Js = ne({
  prop: "gridTemplateColumns"
}), Zs = ne({
  prop: "gridTemplateRows"
}), Qs = ne({
  prop: "gridTemplateAreas"
}), ec = ne({
  prop: "gridArea"
});
ct(lt, ft, dt, qs, Ks, Gs, Hs, Xs, Js, Zs, Qs, ec);
function ur(e, r) {
  return r === "grey" ? r : e;
}
const rc = ne({
  prop: "color",
  themeKey: "palette",
  transform: ur
}), tc = ne({
  prop: "bgcolor",
  cssProperty: "backgroundColor",
  themeKey: "palette",
  transform: ur
}), nc = ne({
  prop: "backgroundColor",
  themeKey: "palette",
  transform: ur
});
ct(rc, tc, nc);
function Pe(e) {
  return e <= 1 && e !== 0 ? `${e * 100}%` : e;
}
const oc = ne({
  prop: "width",
  transform: Pe
}), Zt = (e) => {
  if (e.maxWidth !== void 0 && e.maxWidth !== null) {
    const r = (t) => {
      var n, o, i;
      return {
        maxWidth: ((n = e.theme) == null || (o = n.breakpoints) == null || (i = o.values) == null ? void 0 : i[t]) || Jt[t] || Pe(t)
      };
    };
    return Ue(e, e.maxWidth, r);
  }
  return null;
};
Zt.filterProps = ["maxWidth"];
const ic = ne({
  prop: "minWidth",
  transform: Pe
}), ac = ne({
  prop: "height",
  transform: Pe
}), sc = ne({
  prop: "maxHeight",
  transform: Pe
}), cc = ne({
  prop: "minHeight",
  transform: Pe
});
ne({
  prop: "size",
  cssProperty: "width",
  transform: Pe
});
ne({
  prop: "size",
  cssProperty: "height",
  transform: Pe
});
const uc = ne({
  prop: "boxSizing"
});
ct(oc, Zt, ic, ac, sc, cc, uc);
const lc = {
  // borders
  border: {
    themeKey: "borders",
    transform: Le
  },
  borderTop: {
    themeKey: "borders",
    transform: Le
  },
  borderRight: {
    themeKey: "borders",
    transform: Le
  },
  borderBottom: {
    themeKey: "borders",
    transform: Le
  },
  borderLeft: {
    themeKey: "borders",
    transform: Le
  },
  borderColor: {
    themeKey: "palette"
  },
  borderTopColor: {
    themeKey: "palette"
  },
  borderRightColor: {
    themeKey: "palette"
  },
  borderBottomColor: {
    themeKey: "palette"
  },
  borderLeftColor: {
    themeKey: "palette"
  },
  borderRadius: {
    themeKey: "shape.borderRadius",
    style: ut
  },
  // palette
  color: {
    themeKey: "palette",
    transform: ur
  },
  bgcolor: {
    themeKey: "palette",
    cssProperty: "backgroundColor",
    transform: ur
  },
  backgroundColor: {
    themeKey: "palette",
    transform: ur
  },
  // spacing
  p: {
    style: ve
  },
  pt: {
    style: ve
  },
  pr: {
    style: ve
  },
  pb: {
    style: ve
  },
  pl: {
    style: ve
  },
  px: {
    style: ve
  },
  py: {
    style: ve
  },
  padding: {
    style: ve
  },
  paddingTop: {
    style: ve
  },
  paddingRight: {
    style: ve
  },
  paddingBottom: {
    style: ve
  },
  paddingLeft: {
    style: ve
  },
  paddingX: {
    style: ve
  },
  paddingY: {
    style: ve
  },
  paddingInline: {
    style: ve
  },
  paddingInlineStart: {
    style: ve
  },
  paddingInlineEnd: {
    style: ve
  },
  paddingBlock: {
    style: ve
  },
  paddingBlockStart: {
    style: ve
  },
  paddingBlockEnd: {
    style: ve
  },
  m: {
    style: ye
  },
  mt: {
    style: ye
  },
  mr: {
    style: ye
  },
  mb: {
    style: ye
  },
  ml: {
    style: ye
  },
  mx: {
    style: ye
  },
  my: {
    style: ye
  },
  margin: {
    style: ye
  },
  marginTop: {
    style: ye
  },
  marginRight: {
    style: ye
  },
  marginBottom: {
    style: ye
  },
  marginLeft: {
    style: ye
  },
  marginX: {
    style: ye
  },
  marginY: {
    style: ye
  },
  marginInline: {
    style: ye
  },
  marginInlineStart: {
    style: ye
  },
  marginInlineEnd: {
    style: ye
  },
  marginBlock: {
    style: ye
  },
  marginBlockStart: {
    style: ye
  },
  marginBlockEnd: {
    style: ye
  },
  // display
  displayPrint: {
    cssProperty: !1,
    transform: (e) => ({
      "@media print": {
        display: e
      }
    })
  },
  display: {},
  overflow: {},
  textOverflow: {},
  visibility: {},
  whiteSpace: {},
  // flexbox
  flexBasis: {},
  flexDirection: {},
  flexWrap: {},
  justifyContent: {},
  alignItems: {},
  alignContent: {},
  order: {},
  flex: {},
  flexGrow: {},
  flexShrink: {},
  alignSelf: {},
  justifyItems: {},
  justifySelf: {},
  // grid
  gap: {
    style: lt
  },
  rowGap: {
    style: dt
  },
  columnGap: {
    style: ft
  },
  gridColumn: {},
  gridRow: {},
  gridAutoFlow: {},
  gridAutoColumns: {},
  gridAutoRows: {},
  gridTemplateColumns: {},
  gridTemplateRows: {},
  gridTemplateAreas: {},
  gridArea: {},
  // positions
  position: {},
  zIndex: {
    themeKey: "zIndex"
  },
  top: {},
  right: {},
  bottom: {},
  left: {},
  // shadows
  boxShadow: {
    themeKey: "shadows"
  },
  // sizing
  width: {
    transform: Pe
  },
  maxWidth: {
    style: Zt
  },
  minWidth: {
    transform: Pe
  },
  height: {
    transform: Pe
  },
  maxHeight: {
    transform: Pe
  },
  minHeight: {
    transform: Pe
  },
  boxSizing: {},
  // typography
  fontFamily: {
    themeKey: "typography"
  },
  fontSize: {
    themeKey: "typography"
  },
  fontStyle: {
    themeKey: "typography"
  },
  fontWeight: {
    themeKey: "typography"
  },
  letterSpacing: {},
  textTransform: {},
  lineHeight: {},
  textAlign: {},
  typography: {
    cssProperty: !1,
    themeKey: "typography"
  }
}, Qt = lc;
function fc(...e) {
  const r = e.reduce((n, o) => n.concat(Object.keys(o)), []), t = new Set(r);
  return e.every((n) => t.size === Object.keys(n).length);
}
function dc(e, r) {
  return typeof e == "function" ? e(r) : e;
}
function pc() {
  function e(t, n, o, i) {
    const a = {
      [t]: n,
      theme: o
    }, c = i[t];
    if (!c)
      return {
        [t]: n
      };
    const {
      cssProperty: u = t,
      themeKey: f,
      transform: d,
      style: p
    } = c;
    if (n == null)
      return null;
    if (f === "typography" && n === "inherit")
      return {
        [t]: n
      };
    const h = it(o, f) || {};
    return p ? p(a) : Ue(a, n, (g) => {
      let m = Xr(h, d, g);
      return g === m && typeof g == "string" && (m = Xr(h, d, `${t}${g === "default" ? "" : Oe(g)}`, g)), u === !1 ? m : {
        [u]: m
      };
    });
  }
  function r(t) {
    var n;
    const {
      sx: o,
      theme: i = {}
    } = t || {};
    if (!o)
      return null;
    const a = (n = i.unstable_sxConfig) != null ? n : Qt;
    function c(u) {
      let f = u;
      if (typeof u == "function")
        f = u(i);
      else if (typeof u != "object")
        return u;
      if (!f)
        return null;
      const d = Cs(i.breakpoints), p = Object.keys(d);
      let h = d;
      return Object.keys(f).forEach((E) => {
        const g = dc(f[E], i);
        if (g != null)
          if (typeof g == "object")
            if (a[E])
              h = Cr(h, e(E, g, i, a));
            else {
              const m = Ue({
                theme: i
              }, g, (y) => ({
                [E]: y
              }));
              fc(m, g) ? h[E] = r({
                sx: g,
                theme: i
              }) : h = Cr(h, m);
            }
          else
            h = Cr(h, e(E, g, i, a));
      }), Os(p, h);
    }
    return Array.isArray(o) ? o.map(c) : c(o);
  }
  return r;
}
const Do = pc();
Do.filterProps = ["sx"];
const en = Do, mc = ["breakpoints", "palette", "spacing", "shape"];
function rn(e = {}, ...r) {
  const {
    breakpoints: t = {},
    palette: n = {},
    spacing: o,
    shape: i = {}
  } = e, a = je(e, mc), c = Ts(t), u = Ms(o);
  let f = Ye({
    breakpoints: c,
    direction: "ltr",
    components: {},
    // Inject component definitions.
    palette: j({
      mode: "light"
    }, n),
    spacing: u,
    shape: j({}, Ss, i)
  }, a);
  return f = r.reduce((d, p) => Ye(d, p), f), f.unstable_sxConfig = j({}, Qt, a == null ? void 0 : a.unstable_sxConfig), f.unstable_sx = function(p) {
    return en({
      sx: p,
      theme: this
    });
  }, f;
}
function hc(e) {
  return Object.keys(e).length === 0;
}
function yc(e = null) {
  const r = P.useContext(mr);
  return !r || hc(r) ? e : r;
}
const vc = rn();
function gc(e = vc) {
  return yc(e);
}
function Fo(e) {
  var r, t, n = "";
  if (typeof e == "string" || typeof e == "number")
    n += e;
  else if (typeof e == "object")
    if (Array.isArray(e))
      for (r = 0; r < e.length; r++)
        e[r] && (t = Fo(e[r])) && (n && (n += " "), n += t);
    else
      for (r in e)
        e[r] && (n && (n += " "), n += r);
  return n;
}
function Ne() {
  for (var e, r, t = 0, n = ""; t < arguments.length; )
    (e = arguments[t++]) && (r = Fo(e)) && (n && (n += " "), n += r);
  return n;
}
const bc = ["variant"];
function ro(e) {
  return e.length === 0;
}
function Vo(e) {
  const {
    variant: r
  } = e, t = je(e, bc);
  let n = r || "";
  return Object.keys(t).sort().forEach((o) => {
    o === "color" ? n += ro(n) ? e[o] : Oe(e[o]) : n += `${ro(n) ? o : Oe(o)}${Oe(e[o].toString())}`;
  }), n;
}
const Ec = ["name", "slot", "skipVariantsResolver", "skipSx", "overridesResolver"];
function xc(e) {
  return Object.keys(e).length === 0;
}
function Tc(e) {
  return typeof e == "string" && // 96 is one less than the char code
  // for "a" so this is checking that
  // it's a lowercase character
  e.charCodeAt(0) > 96;
}
const _c = (e, r) => r.components && r.components[e] && r.components[e].styleOverrides ? r.components[e].styleOverrides : null, Sc = (e, r) => {
  let t = [];
  r && r.components && r.components[e] && r.components[e].variants && (t = r.components[e].variants);
  const n = {};
  return t.forEach((o) => {
    const i = Vo(o.props);
    n[i] = o.style;
  }), n;
}, Rc = (e, r, t, n) => {
  var o, i;
  const {
    ownerState: a = {}
  } = e, c = [], u = t == null || (o = t.components) == null || (i = o[n]) == null ? void 0 : i.variants;
  return u && u.forEach((f) => {
    let d = !0;
    Object.keys(f.props).forEach((p) => {
      a[p] !== f.props[p] && e[p] !== f.props[p] && (d = !1);
    }), d && c.push(r[Vo(f.props)]);
  }), c;
};
function Kr(e) {
  return e !== "ownerState" && e !== "theme" && e !== "sx" && e !== "as";
}
const Cc = rn(), Oc = (e) => e.charAt(0).toLowerCase() + e.slice(1);
function Rr({
  defaultTheme: e,
  theme: r,
  themeId: t
}) {
  return xc(r) ? e : r[t] || r;
}
function wc(e = {}) {
  const {
    themeId: r,
    defaultTheme: t = Cc,
    rootShouldForwardProp: n = Kr,
    slotShouldForwardProp: o = Kr
  } = e, i = (a) => en(j({}, a, {
    theme: Rr(j({}, a, {
      defaultTheme: t,
      themeId: r
    }))
  }));
  return i.__mui_systemSx = !0, (a, c = {}) => {
    bs(a, (_) => _.filter((l) => !(l != null && l.__mui_systemSx)));
    const {
      name: u,
      slot: f,
      skipVariantsResolver: d,
      skipSx: p,
      overridesResolver: h
    } = c, E = je(c, Ec), g = d !== void 0 ? d : f && f !== "Root" || !1, m = p || !1;
    let y;
    process.env.NODE_ENV !== "production" && u && (y = `${u}-${Oc(f || "Root")}`);
    let S = Kr;
    f === "Root" ? S = n : f ? S = o : Tc(a) && (S = void 0);
    const w = gs(a, j({
      shouldForwardProp: S,
      label: y
    }, E)), C = (_, ...l) => {
      const k = l ? l.map((I) => typeof I == "function" && I.__emotion_real !== I ? (ae) => I(j({}, ae, {
        theme: Rr(j({}, ae, {
          defaultTheme: t,
          themeId: r
        }))
      })) : I) : [];
      let N = _;
      u && h && k.push((I) => {
        const ae = Rr(j({}, I, {
          defaultTheme: t,
          themeId: r
        })), me = _c(u, ae);
        if (me) {
          const oe = {};
          return Object.entries(me).forEach(([ie, le]) => {
            oe[ie] = typeof le == "function" ? le(j({}, I, {
              theme: ae
            })) : le;
          }), h(I, oe);
        }
        return null;
      }), u && !g && k.push((I) => {
        const ae = Rr(j({}, I, {
          defaultTheme: t,
          themeId: r
        }));
        return Rc(I, Sc(u, ae), ae, u);
      }), m || k.push(i);
      const pe = k.length - l.length;
      if (Array.isArray(_) && pe > 0) {
        const I = new Array(pe).fill("");
        N = [..._, ...I], N.raw = [..._.raw, ...I];
      } else
        typeof _ == "function" && // On the server Emotion doesn't use React.forwardRef for creating components, so the created
        // component stays as a function. This condition makes sure that we do not interpolate functions
        // which are basically components used as a selectors.
        _.__emotion_real !== _ && (N = (I) => _(j({}, I, {
          theme: Rr(j({}, I, {
            defaultTheme: t,
            themeId: r
          }))
        })));
      const ue = w(N, ...k);
      if (process.env.NODE_ENV !== "production") {
        let I;
        u && (I = `${u}${f || ""}`), I === void 0 && (I = `Styled(${Li(a)})`), ue.displayName = I;
      }
      return a.muiName && (ue.muiName = a.muiName), ue;
    };
    return w.withConfig && (C.withConfig = w.withConfig), C;
  };
}
function $c(e) {
  const {
    theme: r,
    name: t,
    props: n
  } = e;
  return !r || !r.components || !r.components[t] || !r.components[t].defaultProps ? n : Lt(r.components[t].defaultProps, n);
}
function Pc({
  props: e,
  name: r,
  defaultTheme: t,
  themeId: n
}) {
  let o = gc(t);
  return n && (o = o[n] || o), $c({
    theme: o,
    name: r,
    props: e
  });
}
function tn(e, r = 0, t = 1) {
  return process.env.NODE_ENV !== "production" && (e < r || e > t) && console.error(`MUI: The value provided ${e} is out of range [${r}, ${t}].`), Math.min(Math.max(r, e), t);
}
function kc(e) {
  e = e.slice(1);
  const r = new RegExp(`.{1,${e.length >= 6 ? 2 : 1}}`, "g");
  let t = e.match(r);
  return t && t[0].length === 1 && (t = t.map((n) => n + n)), t ? `rgb${t.length === 4 ? "a" : ""}(${t.map((n, o) => o < 3 ? parseInt(n, 16) : Math.round(parseInt(n, 16) / 255 * 1e3) / 1e3).join(", ")})` : "";
}
function Je(e) {
  if (e.type)
    return e;
  if (e.charAt(0) === "#")
    return Je(kc(e));
  const r = e.indexOf("("), t = e.substring(0, r);
  if (["rgb", "rgba", "hsl", "hsla", "color"].indexOf(t) === -1)
    throw new Error(process.env.NODE_ENV !== "production" ? `MUI: Unsupported \`${e}\` color.
The following formats are supported: #nnn, #nnnnnn, rgb(), rgba(), hsl(), hsla(), color().` : lr(9, e));
  let n = e.substring(r + 1, e.length - 1), o;
  if (t === "color") {
    if (n = n.split(" "), o = n.shift(), n.length === 4 && n[3].charAt(0) === "/" && (n[3] = n[3].slice(1)), ["srgb", "display-p3", "a98-rgb", "prophoto-rgb", "rec-2020"].indexOf(o) === -1)
      throw new Error(process.env.NODE_ENV !== "production" ? `MUI: unsupported \`${o}\` color space.
The following color spaces are supported: srgb, display-p3, a98-rgb, prophoto-rgb, rec-2020.` : lr(10, o));
  } else
    n = n.split(",");
  return n = n.map((i) => parseFloat(i)), {
    type: t,
    values: n,
    colorSpace: o
  };
}
function pt(e) {
  const {
    type: r,
    colorSpace: t
  } = e;
  let {
    values: n
  } = e;
  return r.indexOf("rgb") !== -1 ? n = n.map((o, i) => i < 3 ? parseInt(o, 10) : o) : r.indexOf("hsl") !== -1 && (n[1] = `${n[1]}%`, n[2] = `${n[2]}%`), r.indexOf("color") !== -1 ? n = `${t} ${n.join(" ")}` : n = `${n.join(", ")}`, `${r}(${n})`;
}
function Ac(e) {
  e = Je(e);
  const {
    values: r
  } = e, t = r[0], n = r[1] / 100, o = r[2] / 100, i = n * Math.min(o, 1 - o), a = (f, d = (f + t / 30) % 12) => o - i * Math.max(Math.min(d - 3, 9 - d, 1), -1);
  let c = "rgb";
  const u = [Math.round(a(0) * 255), Math.round(a(8) * 255), Math.round(a(4) * 255)];
  return e.type === "hsla" && (c += "a", u.push(r[3])), pt({
    type: c,
    values: u
  });
}
function to(e) {
  e = Je(e);
  let r = e.type === "hsl" || e.type === "hsla" ? Je(Ac(e)).values : e.values;
  return r = r.map((t) => (e.type !== "color" && (t /= 255), t <= 0.03928 ? t / 12.92 : ((t + 0.055) / 1.055) ** 2.4)), Number((0.2126 * r[0] + 0.7152 * r[1] + 0.0722 * r[2]).toFixed(3));
}
function no(e, r) {
  const t = to(e), n = to(r);
  return (Math.max(t, n) + 0.05) / (Math.min(t, n) + 0.05);
}
function Lr(e, r) {
  return e = Je(e), r = tn(r), (e.type === "rgb" || e.type === "hsl") && (e.type += "a"), e.type === "color" ? e.values[3] = `/${r}` : e.values[3] = r, pt(e);
}
function Nc(e, r) {
  if (e = Je(e), r = tn(r), e.type.indexOf("hsl") !== -1)
    e.values[2] *= 1 - r;
  else if (e.type.indexOf("rgb") !== -1 || e.type.indexOf("color") !== -1)
    for (let t = 0; t < 3; t += 1)
      e.values[t] *= 1 - r;
  return pt(e);
}
function Ic(e, r) {
  if (e = Je(e), r = tn(r), e.type.indexOf("hsl") !== -1)
    e.values[2] += (100 - e.values[2]) * r;
  else if (e.type.indexOf("rgb") !== -1)
    for (let t = 0; t < 3; t += 1)
      e.values[t] += (255 - e.values[t]) * r;
  else if (e.type.indexOf("color") !== -1)
    for (let t = 0; t < 3; t += 1)
      e.values[t] += (1 - e.values[t]) * r;
  return pt(e);
}
function Mc(e, r) {
  return j({
    toolbar: {
      minHeight: 56,
      [e.up("xs")]: {
        "@media (orientation: landscape)": {
          minHeight: 48
        }
      },
      [e.up("sm")]: {
        minHeight: 64
      }
    }
  }, r);
}
const jc = ["mode", "contrastThreshold", "tonalOffset"], oo = {
  // The colors used to style the text.
  text: {
    // The most important text.
    primary: "rgba(0, 0, 0, 0.87)",
    // Secondary text.
    secondary: "rgba(0, 0, 0, 0.6)",
    // Disabled text have even lower visual prominence.
    disabled: "rgba(0, 0, 0, 0.38)"
  },
  // The color used to divide different elements.
  divider: "rgba(0, 0, 0, 0.12)",
  // The background colors used to style the surfaces.
  // Consistency between these values is important.
  background: {
    paper: Or.white,
    default: Or.white
  },
  // The colors used to style the action elements.
  action: {
    // The color of an active action like an icon button.
    active: "rgba(0, 0, 0, 0.54)",
    // The color of an hovered action.
    hover: "rgba(0, 0, 0, 0.04)",
    hoverOpacity: 0.04,
    // The color of a selected action.
    selected: "rgba(0, 0, 0, 0.08)",
    selectedOpacity: 0.08,
    // The color of a disabled action.
    disabled: "rgba(0, 0, 0, 0.26)",
    // The background color of a disabled action.
    disabledBackground: "rgba(0, 0, 0, 0.12)",
    disabledOpacity: 0.38,
    focus: "rgba(0, 0, 0, 0.12)",
    focusOpacity: 0.12,
    activatedOpacity: 0.12
  }
}, wt = {
  text: {
    primary: Or.white,
    secondary: "rgba(255, 255, 255, 0.7)",
    disabled: "rgba(255, 255, 255, 0.5)",
    icon: "rgba(255, 255, 255, 0.5)"
  },
  divider: "rgba(255, 255, 255, 0.12)",
  background: {
    paper: "#121212",
    default: "#121212"
  },
  action: {
    active: Or.white,
    hover: "rgba(255, 255, 255, 0.08)",
    hoverOpacity: 0.08,
    selected: "rgba(255, 255, 255, 0.16)",
    selectedOpacity: 0.16,
    disabled: "rgba(255, 255, 255, 0.3)",
    disabledBackground: "rgba(255, 255, 255, 0.12)",
    disabledOpacity: 0.38,
    focus: "rgba(255, 255, 255, 0.12)",
    focusOpacity: 0.12,
    activatedOpacity: 0.24
  }
};
function io(e, r, t, n) {
  const o = n.light || n, i = n.dark || n * 1.5;
  e[r] || (e.hasOwnProperty(t) ? e[r] = e[t] : r === "light" ? e.light = Ic(e.main, o) : r === "dark" && (e.dark = Nc(e.main, i)));
}
function Dc(e = "light") {
  return e === "dark" ? {
    main: or[200],
    light: or[50],
    dark: or[400]
  } : {
    main: or[700],
    light: or[400],
    dark: or[800]
  };
}
function Fc(e = "light") {
  return e === "dark" ? {
    main: nr[200],
    light: nr[50],
    dark: nr[400]
  } : {
    main: nr[500],
    light: nr[300],
    dark: nr[700]
  };
}
function Vc(e = "light") {
  return e === "dark" ? {
    main: tr[500],
    light: tr[300],
    dark: tr[700]
  } : {
    main: tr[700],
    light: tr[400],
    dark: tr[800]
  };
}
function Lc(e = "light") {
  return e === "dark" ? {
    main: ir[400],
    light: ir[300],
    dark: ir[700]
  } : {
    main: ir[700],
    light: ir[500],
    dark: ir[900]
  };
}
function zc(e = "light") {
  return e === "dark" ? {
    main: ar[400],
    light: ar[300],
    dark: ar[700]
  } : {
    main: ar[800],
    light: ar[500],
    dark: ar[900]
  };
}
function Bc(e = "light") {
  return e === "dark" ? {
    main: _r[400],
    light: _r[300],
    dark: _r[700]
  } : {
    main: "#ed6c02",
    // closest to orange[800] that pass 3:1.
    light: _r[500],
    dark: _r[900]
  };
}
function Yc(e) {
  const {
    mode: r = "light",
    contrastThreshold: t = 3,
    tonalOffset: n = 0.2
  } = e, o = je(e, jc), i = e.primary || Dc(r), a = e.secondary || Fc(r), c = e.error || Vc(r), u = e.info || Lc(r), f = e.success || zc(r), d = e.warning || Bc(r);
  function p(m) {
    const y = no(m, wt.text.primary) >= t ? wt.text.primary : oo.text.primary;
    if (process.env.NODE_ENV !== "production") {
      const S = no(m, y);
      S < 3 && console.error([`MUI: The contrast ratio of ${S}:1 for ${y} on ${m}`, "falls below the WCAG recommended absolute minimum contrast ratio of 3:1.", "https://www.w3.org/TR/2008/REC-WCAG20-20081211/#visual-audio-contrast-contrast"].join(`
`));
    }
    return y;
  }
  const h = ({
    color: m,
    name: y,
    mainShade: S = 500,
    lightShade: w = 300,
    darkShade: C = 700
  }) => {
    if (m = j({}, m), !m.main && m[S] && (m.main = m[S]), !m.hasOwnProperty("main"))
      throw new Error(process.env.NODE_ENV !== "production" ? `MUI: The color${y ? ` (${y})` : ""} provided to augmentColor(color) is invalid.
The color object needs to have a \`main\` property or a \`${S}\` property.` : lr(11, y ? ` (${y})` : "", S));
    if (typeof m.main != "string")
      throw new Error(process.env.NODE_ENV !== "production" ? `MUI: The color${y ? ` (${y})` : ""} provided to augmentColor(color) is invalid.
\`color.main\` should be a string, but \`${JSON.stringify(m.main)}\` was provided instead.

Did you intend to use one of the following approaches?

import { green } from "@mui/material/colors";

const theme1 = createTheme({ palette: {
  primary: green,
} });

const theme2 = createTheme({ palette: {
  primary: { main: green[500] },
} });` : lr(12, y ? ` (${y})` : "", JSON.stringify(m.main)));
    return io(m, "light", w, n), io(m, "dark", C, n), m.contrastText || (m.contrastText = p(m.main)), m;
  }, E = {
    dark: wt,
    light: oo
  };
  return process.env.NODE_ENV !== "production" && (E[r] || console.error(`MUI: The palette mode \`${r}\` is not supported.`)), Ye(j({
    // A collection of common colors.
    common: j({}, Or),
    // prevent mutable object.
    // The palette mode, can be light or dark.
    mode: r,
    // The colors used to represent primary interface elements for a user.
    primary: h({
      color: i,
      name: "primary"
    }),
    // The colors used to represent secondary interface elements for a user.
    secondary: h({
      color: a,
      name: "secondary",
      mainShade: "A400",
      lightShade: "A200",
      darkShade: "A700"
    }),
    // The colors used to represent interface elements that the user should be made aware of.
    error: h({
      color: c,
      name: "error"
    }),
    // The colors used to represent potentially dangerous actions or important messages.
    warning: h({
      color: d,
      name: "warning"
    }),
    // The colors used to present information to the user that is neutral and not necessarily important.
    info: h({
      color: u,
      name: "info"
    }),
    // The colors used to indicate the successful completion of an action that user triggered.
    success: h({
      color: f,
      name: "success"
    }),
    // The grey colors.
    grey: Ti,
    // Used by `getContrastText()` to maximize the contrast between
    // the background and the text.
    contrastThreshold: t,
    // Takes a background color and returns the text color that maximizes the contrast.
    getContrastText: p,
    // Generate a rich color object.
    augmentColor: h,
    // Used by the functions below to shift a color's luminance by approximately
    // two indexes within its tonal palette.
    // E.g., shift from Red 500 to Red 300 or Red 700.
    tonalOffset: n
  }, E[r]), o);
}
const Uc = ["fontFamily", "fontSize", "fontWeightLight", "fontWeightRegular", "fontWeightMedium", "fontWeightBold", "htmlFontSize", "allVariants", "pxToRem"];
function Wc(e) {
  return Math.round(e * 1e5) / 1e5;
}
const ao = {
  textTransform: "uppercase"
}, so = '"Roboto", "Helvetica", "Arial", sans-serif';
function qc(e, r) {
  const t = typeof r == "function" ? r(e) : r, {
    fontFamily: n = so,
    // The default font size of the Material Specification.
    fontSize: o = 14,
    // px
    fontWeightLight: i = 300,
    fontWeightRegular: a = 400,
    fontWeightMedium: c = 500,
    fontWeightBold: u = 700,
    // Tell MUI what's the font-size on the html element.
    // 16px is the default font-size used by browsers.
    htmlFontSize: f = 16,
    // Apply the CSS properties to all the variants.
    allVariants: d,
    pxToRem: p
  } = t, h = je(t, Uc);
  process.env.NODE_ENV !== "production" && (typeof o != "number" && console.error("MUI: `fontSize` is required to be a number."), typeof f != "number" && console.error("MUI: `htmlFontSize` is required to be a number."));
  const E = o / 14, g = p || ((S) => `${S / f * E}rem`), m = (S, w, C, _, l) => j({
    fontFamily: n,
    fontWeight: S,
    fontSize: g(w),
    // Unitless following https://meyerweb.com/eric/thoughts/2006/02/08/unitless-line-heights/
    lineHeight: C
  }, n === so ? {
    letterSpacing: `${Wc(_ / w)}em`
  } : {}, l, d), y = {
    h1: m(i, 96, 1.167, -1.5),
    h2: m(i, 60, 1.2, -0.5),
    h3: m(a, 48, 1.167, 0),
    h4: m(a, 34, 1.235, 0.25),
    h5: m(a, 24, 1.334, 0),
    h6: m(c, 20, 1.6, 0.15),
    subtitle1: m(a, 16, 1.75, 0.15),
    subtitle2: m(c, 14, 1.57, 0.1),
    body1: m(a, 16, 1.5, 0.15),
    body2: m(a, 14, 1.43, 0.15),
    button: m(c, 14, 1.75, 0.4, ao),
    caption: m(a, 12, 1.66, 0.4),
    overline: m(a, 12, 2.66, 1, ao),
    inherit: {
      fontFamily: "inherit",
      fontWeight: "inherit",
      fontSize: "inherit",
      lineHeight: "inherit",
      letterSpacing: "inherit"
    }
  };
  return Ye(j({
    htmlFontSize: f,
    pxToRem: g,
    fontFamily: n,
    fontSize: o,
    fontWeightLight: i,
    fontWeightRegular: a,
    fontWeightMedium: c,
    fontWeightBold: u
  }, y), h, {
    clone: !1
    // No need to clone deep
  });
}
const Kc = 0.2, Gc = 0.14, Hc = 0.12;
function de(...e) {
  return [`${e[0]}px ${e[1]}px ${e[2]}px ${e[3]}px rgba(0,0,0,${Kc})`, `${e[4]}px ${e[5]}px ${e[6]}px ${e[7]}px rgba(0,0,0,${Gc})`, `${e[8]}px ${e[9]}px ${e[10]}px ${e[11]}px rgba(0,0,0,${Hc})`].join(",");
}
const Xc = ["none", de(0, 2, 1, -1, 0, 1, 1, 0, 0, 1, 3, 0), de(0, 3, 1, -2, 0, 2, 2, 0, 0, 1, 5, 0), de(0, 3, 3, -2, 0, 3, 4, 0, 0, 1, 8, 0), de(0, 2, 4, -1, 0, 4, 5, 0, 0, 1, 10, 0), de(0, 3, 5, -1, 0, 5, 8, 0, 0, 1, 14, 0), de(0, 3, 5, -1, 0, 6, 10, 0, 0, 1, 18, 0), de(0, 4, 5, -2, 0, 7, 10, 1, 0, 2, 16, 1), de(0, 5, 5, -3, 0, 8, 10, 1, 0, 3, 14, 2), de(0, 5, 6, -3, 0, 9, 12, 1, 0, 3, 16, 2), de(0, 6, 6, -3, 0, 10, 14, 1, 0, 4, 18, 3), de(0, 6, 7, -4, 0, 11, 15, 1, 0, 4, 20, 3), de(0, 7, 8, -4, 0, 12, 17, 2, 0, 5, 22, 4), de(0, 7, 8, -4, 0, 13, 19, 2, 0, 5, 24, 4), de(0, 7, 9, -4, 0, 14, 21, 2, 0, 5, 26, 4), de(0, 8, 9, -5, 0, 15, 22, 2, 0, 6, 28, 5), de(0, 8, 10, -5, 0, 16, 24, 2, 0, 6, 30, 5), de(0, 8, 11, -5, 0, 17, 26, 2, 0, 6, 32, 5), de(0, 9, 11, -5, 0, 18, 28, 2, 0, 7, 34, 6), de(0, 9, 12, -6, 0, 19, 29, 2, 0, 7, 36, 6), de(0, 10, 13, -6, 0, 20, 31, 3, 0, 8, 38, 7), de(0, 10, 13, -6, 0, 21, 33, 3, 0, 8, 40, 7), de(0, 10, 14, -6, 0, 22, 35, 3, 0, 8, 42, 7), de(0, 11, 14, -7, 0, 23, 36, 3, 0, 9, 44, 8), de(0, 11, 15, -7, 0, 24, 38, 3, 0, 9, 46, 8)], Jc = Xc, Zc = ["duration", "easing", "delay"], Qc = {
  // This is the most common easing curve.
  easeInOut: "cubic-bezier(0.4, 0, 0.2, 1)",
  // Objects enter the screen at full velocity from off-screen and
  // slowly decelerate to a resting point.
  easeOut: "cubic-bezier(0.0, 0, 0.2, 1)",
  // Objects leave the screen at full velocity. They do not decelerate when off-screen.
  easeIn: "cubic-bezier(0.4, 0, 1, 1)",
  // The sharp curve is used by objects that may return to the screen at any time.
  sharp: "cubic-bezier(0.4, 0, 0.6, 1)"
}, eu = {
  shortest: 150,
  shorter: 200,
  short: 250,
  // most basic recommended timing
  standard: 300,
  // this is to be used in complex animations
  complex: 375,
  // recommended when something is entering screen
  enteringScreen: 225,
  // recommended when something is leaving screen
  leavingScreen: 195
};
function co(e) {
  return `${Math.round(e)}ms`;
}
function ru(e) {
  if (!e)
    return 0;
  const r = e / 36;
  return Math.round((4 + 15 * r ** 0.25 + r / 5) * 10);
}
function tu(e) {
  const r = j({}, Qc, e.easing), t = j({}, eu, e.duration);
  return j({
    getAutoHeightDuration: ru,
    create: (o = ["all"], i = {}) => {
      const {
        duration: a = t.standard,
        easing: c = r.easeInOut,
        delay: u = 0
      } = i, f = je(i, Zc);
      if (process.env.NODE_ENV !== "production") {
        const d = (h) => typeof h == "string", p = (h) => !isNaN(parseFloat(h));
        !d(o) && !Array.isArray(o) && console.error('MUI: Argument "props" must be a string or Array.'), !p(a) && !d(a) && console.error(`MUI: Argument "duration" must be a number or a string but found ${a}.`), d(c) || console.error('MUI: Argument "easing" must be a string.'), !p(u) && !d(u) && console.error('MUI: Argument "delay" must be a number or a string.'), Object.keys(f).length !== 0 && console.error(`MUI: Unrecognized argument(s) [${Object.keys(f).join(",")}].`);
      }
      return (Array.isArray(o) ? o : [o]).map((d) => `${d} ${typeof a == "string" ? a : co(a)} ${c} ${typeof u == "string" ? u : co(u)}`).join(",");
    }
  }, e, {
    easing: r,
    duration: t
  });
}
const nu = {
  mobileStepper: 1e3,
  fab: 1050,
  speedDial: 1050,
  appBar: 1100,
  drawer: 1200,
  modal: 1300,
  snackbar: 1400,
  tooltip: 1500
}, ou = nu, iu = ["breakpoints", "mixins", "spacing", "palette", "transitions", "typography", "shape"];
function au(e = {}, ...r) {
  const {
    mixins: t = {},
    palette: n = {},
    transitions: o = {},
    typography: i = {}
  } = e, a = je(e, iu);
  if (e.vars)
    throw new Error(process.env.NODE_ENV !== "production" ? "MUI: `vars` is a private field used for CSS variables support.\nPlease use another name." : lr(18));
  const c = Yc(n), u = rn(e);
  let f = Ye(u, {
    mixins: Mc(u.breakpoints, t),
    palette: c,
    // Don't use [...shadows] until you've verified its transpiled code is not invoking the iterator protocol.
    shadows: Jc.slice(),
    typography: qc(c, i),
    transitions: tu(o),
    zIndex: j({}, ou)
  });
  if (f = Ye(f, a), f = r.reduce((d, p) => Ye(d, p), f), process.env.NODE_ENV !== "production") {
    const d = ["active", "checked", "completed", "disabled", "error", "expanded", "focused", "focusVisible", "required", "selected"], p = (h, E) => {
      let g;
      for (g in h) {
        const m = h[g];
        if (d.indexOf(g) !== -1 && Object.keys(m).length > 0) {
          if (process.env.NODE_ENV !== "production") {
            const y = Zr("", g);
            console.error([`MUI: The \`${E}\` component increases the CSS specificity of the \`${g}\` internal state.`, "You can not override it like this: ", JSON.stringify(h, null, 2), "", `Instead, you need to use the '&.${y}' syntax:`, JSON.stringify({
              root: {
                [`&.${y}`]: m
              }
            }, null, 2), "", "https://mui.com/r/state-classes-guide"].join(`
`));
          }
          h[g] = {};
        }
      }
    };
    Object.keys(f.components).forEach((h) => {
      const E = f.components[h].styleOverrides;
      E && h.indexOf("Mui") === 0 && p(E, h);
    });
  }
  return f.unstable_sxConfig = j({}, Qt, a == null ? void 0 : a.unstable_sxConfig), f.unstable_sx = function(p) {
    return en({
      sx: p,
      theme: this
    });
  }, f;
}
const su = au(), Lo = su;
function nn({
  props: e,
  name: r
}) {
  return Pc({
    props: e,
    name: r,
    defaultTheme: Lo,
    themeId: Eo
  });
}
const zo = (e) => Kr(e) && e !== "classes", cu = wc({
  themeId: Eo,
  defaultTheme: Lo,
  rootShouldForwardProp: zo
}), hr = cu;
function Dt(e, r) {
  return Dt = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function(n, o) {
    return n.__proto__ = o, n;
  }, Dt(e, r);
}
function uu(e, r) {
  e.prototype = Object.create(r.prototype), e.prototype.constructor = e, Dt(e, r);
}
const uo = He.createContext(null);
function lu(e) {
  if (e === void 0)
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  return e;
}
function on(e, r) {
  var t = function(i) {
    return r && Br(i) ? r(i) : i;
  }, n = /* @__PURE__ */ Object.create(null);
  return e && ui.map(e, function(o) {
    return o;
  }).forEach(function(o) {
    n[o.key] = t(o);
  }), n;
}
function fu(e, r) {
  e = e || {}, r = r || {};
  function t(d) {
    return d in r ? r[d] : e[d];
  }
  var n = /* @__PURE__ */ Object.create(null), o = [];
  for (var i in e)
    i in r ? o.length && (n[i] = o, o = []) : o.push(i);
  var a, c = {};
  for (var u in r) {
    if (n[u])
      for (a = 0; a < n[u].length; a++) {
        var f = n[u][a];
        c[n[u][a]] = t(f);
      }
    c[u] = t(u);
  }
  for (a = 0; a < o.length; a++)
    c[o[a]] = t(o[a]);
  return c;
}
function Xe(e, r, t) {
  return t[r] != null ? t[r] : e.props[r];
}
function du(e, r) {
  return on(e.children, function(t) {
    return Yr(t, {
      onExited: r.bind(null, t),
      in: !0,
      appear: Xe(t, "appear", e),
      enter: Xe(t, "enter", e),
      exit: Xe(t, "exit", e)
    });
  });
}
function pu(e, r, t) {
  var n = on(e.children), o = fu(r, n);
  return Object.keys(o).forEach(function(i) {
    var a = o[i];
    if (Br(a)) {
      var c = i in r, u = i in n, f = r[i], d = Br(f) && !f.props.in;
      u && (!c || d) ? o[i] = Yr(a, {
        onExited: t.bind(null, a),
        in: !0,
        exit: Xe(a, "exit", e),
        enter: Xe(a, "enter", e)
      }) : !u && c && !d ? o[i] = Yr(a, {
        in: !1
      }) : u && c && Br(f) && (o[i] = Yr(a, {
        onExited: t.bind(null, a),
        in: f.props.in,
        exit: Xe(a, "exit", e),
        enter: Xe(a, "enter", e)
      }));
    }
  }), o;
}
var mu = Object.values || function(e) {
  return Object.keys(e).map(function(r) {
    return e[r];
  });
}, hu = {
  component: "div",
  childFactory: function(r) {
    return r;
  }
}, an = /* @__PURE__ */ function(e) {
  uu(r, e);
  function r(n, o) {
    var i;
    i = e.call(this, n, o) || this;
    var a = i.handleExited.bind(lu(i));
    return i.state = {
      contextValue: {
        isMounting: !0
      },
      handleExited: a,
      firstRender: !0
    }, i;
  }
  var t = r.prototype;
  return t.componentDidMount = function() {
    this.mounted = !0, this.setState({
      contextValue: {
        isMounting: !1
      }
    });
  }, t.componentWillUnmount = function() {
    this.mounted = !1;
  }, r.getDerivedStateFromProps = function(o, i) {
    var a = i.children, c = i.handleExited, u = i.firstRender;
    return {
      children: u ? du(o, c) : pu(o, a, c),
      firstRender: !1
    };
  }, t.handleExited = function(o, i) {
    var a = on(this.props.children);
    o.key in a || (o.props.onExited && o.props.onExited(i), this.mounted && this.setState(function(c) {
      var u = j({}, c.children);
      return delete u[o.key], {
        children: u
      };
    }));
  }, t.render = function() {
    var o = this.props, i = o.component, a = o.childFactory, c = je(o, ["component", "childFactory"]), u = this.state.contextValue, f = mu(this.state.children).map(a);
    return delete c.appear, delete c.enter, delete c.exit, i === null ? /* @__PURE__ */ He.createElement(uo.Provider, {
      value: u
    }, f) : /* @__PURE__ */ He.createElement(uo.Provider, {
      value: u
    }, /* @__PURE__ */ He.createElement(i, c, f));
  }, r;
}(He.Component);
an.propTypes = process.env.NODE_ENV !== "production" ? {
  /**
   * `<TransitionGroup>` renders a `<div>` by default. You can change this
   * behavior by providing a `component` prop.
   * If you use React v16+ and would like to avoid a wrapping `<div>` element
   * you can pass in `component={null}`. This is useful if the wrapping div
   * borks your css styles.
   */
  component: b.any,
  /**
   * A set of `<Transition>` components, that are toggled `in` and out as they
   * leave. the `<TransitionGroup>` will inject specific transition props, so
   * remember to spread them through if you are wrapping the `<Transition>` as
   * with our `<Fade>` example.
   *
   * While this component is meant for multiple `Transition` or `CSSTransition`
   * children, sometimes you may want to have a single transition child with
   * content that you want to be transitioned out and in when you change it
   * (e.g. routes, images etc.) In that case you can change the `key` prop of
   * the transition child as you change its content, this will cause
   * `TransitionGroup` to transition the child out and back in.
   */
  children: b.node,
  /**
   * A convenience prop that enables or disables appear animations
   * for all children. Note that specifying this will override any defaults set
   * on individual children Transitions.
   */
  appear: b.bool,
  /**
   * A convenience prop that enables or disables enter animations
   * for all children. Note that specifying this will override any defaults set
   * on individual children Transitions.
   */
  enter: b.bool,
  /**
   * A convenience prop that enables or disables exit animations
   * for all children. Note that specifying this will override any defaults set
   * on individual children Transitions.
   */
  exit: b.bool,
  /**
   * You may need to apply reactive updates to a child as it is exiting.
   * This is generally done by using `cloneElement` however in the case of an exiting
   * child the element has already been removed and not accessible to the consumer.
   *
   * If you do need to update a child as it leaves you can provide a `childFactory`
   * to wrap every child, even the ones that are leaving.
   *
   * @type Function(child: ReactElement) -> ReactElement
   */
  childFactory: b.func
} : {};
an.defaultProps = hu;
const yu = an;
function Bo(e) {
  const {
    className: r,
    classes: t,
    pulsate: n = !1,
    rippleX: o,
    rippleY: i,
    rippleSize: a,
    in: c,
    onExited: u,
    timeout: f
  } = e, [d, p] = P.useState(!1), h = Ne(r, t.ripple, t.rippleVisible, n && t.ripplePulsate), E = {
    width: a,
    height: a,
    top: -(a / 2) + i,
    left: -(a / 2) + o
  }, g = Ne(t.child, d && t.childLeaving, n && t.childPulsate);
  return !c && !d && p(!0), P.useEffect(() => {
    if (!c && u != null) {
      const m = setTimeout(u, f);
      return () => {
        clearTimeout(m);
      };
    }
  }, [u, c, f]), /* @__PURE__ */ Me.jsx("span", {
    className: h,
    style: E,
    children: /* @__PURE__ */ Me.jsx("span", {
      className: g
    })
  });
}
process.env.NODE_ENV !== "production" && (Bo.propTypes = {
  /**
   * Override or extend the styles applied to the component.
   * See [CSS API](#css) below for more details.
   */
  classes: b.object.isRequired,
  className: b.string,
  /**
   * @ignore - injected from TransitionGroup
   */
  in: b.bool,
  /**
   * @ignore - injected from TransitionGroup
   */
  onExited: b.func,
  /**
   * If `true`, the ripple pulsates, typically indicating the keyboard focus state of an element.
   */
  pulsate: b.bool,
  /**
   * Diameter of the ripple.
   */
  rippleSize: b.number,
  /**
   * Horizontal position of the ripple center.
   */
  rippleX: b.number,
  /**
   * Vertical position of the ripple center.
   */
  rippleY: b.number,
  /**
   * exit delay
   */
  timeout: b.number.isRequired
});
const vu = zt("MuiTouchRipple", ["root", "ripple", "rippleVisible", "ripplePulsate", "child", "childLeaving", "childPulsate"]), Ie = vu, gu = ["center", "classes", "className"];
let mt = (e) => e, lo, fo, po, mo;
const Ft = 550, bu = 80, Eu = Xt(lo || (lo = mt`
  0% {
    transform: scale(0);
    opacity: 0.1;
  }

  100% {
    transform: scale(1);
    opacity: 0.3;
  }
`)), xu = Xt(fo || (fo = mt`
  0% {
    opacity: 1;
  }

  100% {
    opacity: 0;
  }
`)), Tu = Xt(po || (po = mt`
  0% {
    transform: scale(1);
  }

  50% {
    transform: scale(0.92);
  }

  100% {
    transform: scale(1);
  }
`)), _u = hr("span", {
  name: "MuiTouchRipple",
  slot: "Root"
})({
  overflow: "hidden",
  pointerEvents: "none",
  position: "absolute",
  zIndex: 0,
  top: 0,
  right: 0,
  bottom: 0,
  left: 0,
  borderRadius: "inherit"
}), Su = hr(Bo, {
  name: "MuiTouchRipple",
  slot: "Ripple"
})(mo || (mo = mt`
  opacity: 0;
  position: absolute;

  &.${0} {
    opacity: 0.3;
    transform: scale(1);
    animation-name: ${0};
    animation-duration: ${0}ms;
    animation-timing-function: ${0};
  }

  &.${0} {
    animation-duration: ${0}ms;
  }

  & .${0} {
    opacity: 1;
    display: block;
    width: 100%;
    height: 100%;
    border-radius: 50%;
    background-color: currentColor;
  }

  & .${0} {
    opacity: 0;
    animation-name: ${0};
    animation-duration: ${0}ms;
    animation-timing-function: ${0};
  }

  & .${0} {
    position: absolute;
    /* @noflip */
    left: 0px;
    top: 0;
    animation-name: ${0};
    animation-duration: 2500ms;
    animation-timing-function: ${0};
    animation-iteration-count: infinite;
    animation-delay: 200ms;
  }
`), Ie.rippleVisible, Eu, Ft, ({
  theme: e
}) => e.transitions.easing.easeInOut, Ie.ripplePulsate, ({
  theme: e
}) => e.transitions.duration.shorter, Ie.child, Ie.childLeaving, xu, Ft, ({
  theme: e
}) => e.transitions.easing.easeInOut, Ie.childPulsate, Tu, ({
  theme: e
}) => e.transitions.easing.easeInOut), Yo = /* @__PURE__ */ P.forwardRef(function(r, t) {
  const n = nn({
    props: r,
    name: "MuiTouchRipple"
  }), {
    center: o = !1,
    classes: i = {},
    className: a
  } = n, c = je(n, gu), [u, f] = P.useState([]), d = P.useRef(0), p = P.useRef(null);
  P.useEffect(() => {
    p.current && (p.current(), p.current = null);
  }, [u]);
  const h = P.useRef(!1), E = P.useRef(0), g = P.useRef(null), m = P.useRef(null);
  P.useEffect(() => () => {
    E.current && clearTimeout(E.current);
  }, []);
  const y = P.useCallback((_) => {
    const {
      pulsate: l,
      rippleX: k,
      rippleY: N,
      rippleSize: pe,
      cb: ue
    } = _;
    f((I) => [...I, /* @__PURE__ */ Me.jsx(Su, {
      classes: {
        ripple: Ne(i.ripple, Ie.ripple),
        rippleVisible: Ne(i.rippleVisible, Ie.rippleVisible),
        ripplePulsate: Ne(i.ripplePulsate, Ie.ripplePulsate),
        child: Ne(i.child, Ie.child),
        childLeaving: Ne(i.childLeaving, Ie.childLeaving),
        childPulsate: Ne(i.childPulsate, Ie.childPulsate)
      },
      timeout: Ft,
      pulsate: l,
      rippleX: k,
      rippleY: N,
      rippleSize: pe
    }, d.current)]), d.current += 1, p.current = ue;
  }, [i]), S = P.useCallback((_ = {}, l = {}, k = () => {
  }) => {
    const {
      pulsate: N = !1,
      center: pe = o || l.pulsate,
      fakeElement: ue = !1
      // For test purposes
    } = l;
    if ((_ == null ? void 0 : _.type) === "mousedown" && h.current) {
      h.current = !1;
      return;
    }
    (_ == null ? void 0 : _.type) === "touchstart" && (h.current = !0);
    const I = ue ? null : m.current, ae = I ? I.getBoundingClientRect() : {
      width: 0,
      height: 0,
      left: 0,
      top: 0
    };
    let me, oe, ie;
    if (pe || _ === void 0 || _.clientX === 0 && _.clientY === 0 || !_.clientX && !_.touches)
      me = Math.round(ae.width / 2), oe = Math.round(ae.height / 2);
    else {
      const {
        clientX: le,
        clientY: se
      } = _.touches && _.touches.length > 0 ? _.touches[0] : _;
      me = Math.round(le - ae.left), oe = Math.round(se - ae.top);
    }
    if (pe)
      ie = Math.sqrt((2 * ae.width ** 2 + ae.height ** 2) / 3), ie % 2 === 0 && (ie += 1);
    else {
      const le = Math.max(Math.abs((I ? I.clientWidth : 0) - me), me) * 2 + 2, se = Math.max(Math.abs((I ? I.clientHeight : 0) - oe), oe) * 2 + 2;
      ie = Math.sqrt(le ** 2 + se ** 2);
    }
    _ != null && _.touches ? g.current === null && (g.current = () => {
      y({
        pulsate: N,
        rippleX: me,
        rippleY: oe,
        rippleSize: ie,
        cb: k
      });
    }, E.current = setTimeout(() => {
      g.current && (g.current(), g.current = null);
    }, bu)) : y({
      pulsate: N,
      rippleX: me,
      rippleY: oe,
      rippleSize: ie,
      cb: k
    });
  }, [o, y]), w = P.useCallback(() => {
    S({}, {
      pulsate: !0
    });
  }, [S]), C = P.useCallback((_, l) => {
    if (clearTimeout(E.current), (_ == null ? void 0 : _.type) === "touchend" && g.current) {
      g.current(), g.current = null, E.current = setTimeout(() => {
        C(_, l);
      });
      return;
    }
    g.current = null, f((k) => k.length > 0 ? k.slice(1) : k), p.current = l;
  }, []);
  return P.useImperativeHandle(t, () => ({
    pulsate: w,
    start: S,
    stop: C
  }), [w, S, C]), /* @__PURE__ */ Me.jsx(_u, j({
    className: Ne(Ie.root, i.root, a),
    ref: m
  }, c, {
    children: /* @__PURE__ */ Me.jsx(yu, {
      component: null,
      exit: !0,
      children: u
    })
  }));
});
process.env.NODE_ENV !== "production" && (Yo.propTypes = {
  /**
   * If `true`, the ripple starts at the center of the component
   * rather than at the point of interaction.
   */
  center: b.bool,
  /**
   * Override or extend the styles applied to the component.
   * See [CSS API](#css) below for more details.
   */
  classes: b.object,
  /**
   * @ignore
   */
  className: b.string
});
const Ru = Yo;
function Cu(e) {
  return Zr("MuiButtonBase", e);
}
const Ou = zt("MuiButtonBase", ["root", "disabled", "focusVisible"]), wu = Ou, $u = ["action", "centerRipple", "children", "className", "component", "disabled", "disableRipple", "disableTouchRipple", "focusRipple", "focusVisibleClassName", "LinkComponent", "onBlur", "onClick", "onContextMenu", "onDragLeave", "onFocus", "onFocusVisible", "onKeyDown", "onKeyUp", "onMouseDown", "onMouseLeave", "onMouseUp", "onTouchEnd", "onTouchMove", "onTouchStart", "tabIndex", "TouchRippleProps", "touchRippleRef", "type"], Pu = (e) => {
  const {
    disabled: r,
    focusVisible: t,
    focusVisibleClassName: n,
    classes: o
  } = e, a = bo({
    root: ["root", r && "disabled", t && "focusVisible"]
  }, Cu, o);
  return t && n && (a.root += ` ${n}`), a;
}, ku = hr("button", {
  name: "MuiButtonBase",
  slot: "Root",
  overridesResolver: (e, r) => r.root
})({
  display: "inline-flex",
  alignItems: "center",
  justifyContent: "center",
  position: "relative",
  boxSizing: "border-box",
  WebkitTapHighlightColor: "transparent",
  backgroundColor: "transparent",
  // Reset default value
  // We disable the focus ring for mouse, touch and keyboard users.
  outline: 0,
  border: 0,
  margin: 0,
  // Remove the margin in Safari
  borderRadius: 0,
  padding: 0,
  // Remove the padding in Firefox
  cursor: "pointer",
  userSelect: "none",
  verticalAlign: "middle",
  MozAppearance: "none",
  // Reset
  WebkitAppearance: "none",
  // Reset
  textDecoration: "none",
  // So we take precedent over the style of a native <a /> element.
  color: "inherit",
  "&::-moz-focus-inner": {
    borderStyle: "none"
    // Remove Firefox dotted outline.
  },
  [`&.${wu.disabled}`]: {
    pointerEvents: "none",
    // Disable link interactions
    cursor: "default"
  },
  "@media print": {
    colorAdjust: "exact"
  }
}), Uo = /* @__PURE__ */ P.forwardRef(function(r, t) {
  const n = nn({
    props: r,
    name: "MuiButtonBase"
  }), {
    action: o,
    centerRipple: i = !1,
    children: a,
    className: c,
    component: u = "button",
    disabled: f = !1,
    disableRipple: d = !1,
    disableTouchRipple: p = !1,
    focusRipple: h = !1,
    LinkComponent: E = "a",
    onBlur: g,
    onClick: m,
    onContextMenu: y,
    onDragLeave: S,
    onFocus: w,
    onFocusVisible: C,
    onKeyDown: _,
    onKeyUp: l,
    onMouseDown: k,
    onMouseLeave: N,
    onMouseUp: pe,
    onTouchEnd: ue,
    onTouchMove: I,
    onTouchStart: ae,
    tabIndex: me = 0,
    TouchRippleProps: oe,
    touchRippleRef: ie,
    type: le
  } = n, se = je(n, $u), be = P.useRef(null), fe = P.useRef(null), $e = Pn(fe, ie), {
    isFocusVisibleRef: x,
    onFocus: O,
    onBlur: B,
    ref: L
  } = Zi(), [A, Y] = P.useState(!1);
  f && A && Y(!1), P.useImperativeHandle(o, () => ({
    focusVisible: () => {
      Y(!0), be.current.focus();
    }
  }), []);
  const [M, F] = P.useState(!1);
  P.useEffect(() => {
    F(!0);
  }, []);
  const z = M && !d && !f;
  P.useEffect(() => {
    A && h && !d && M && fe.current.pulsate();
  }, [d, h, A, M]);
  function D(W, br, Er = p) {
    return Fr((jr) => (br && br(jr), !Er && fe.current && fe.current[W](jr), !0));
  }
  const U = D("start", k), he = D("stop", y), v = D("stop", S), Ee = D("stop", pe), $ = D("stop", (W) => {
    A && W.preventDefault(), N && N(W);
  }), Se = D("start", ae), De = D("stop", ue), Ge = D("stop", I), Ir = D("stop", (W) => {
    B(W), x.current === !1 && Y(!1), g && g(W);
  }, !1), Ze = Fr((W) => {
    be.current || (be.current = W.currentTarget), O(W), x.current === !0 && (Y(!0), C && C(W)), w && w(W);
  }), yr = () => {
    const W = be.current;
    return u && u !== "button" && !(W.tagName === "A" && W.href);
  }, vr = P.useRef(!1), gr = Fr((W) => {
    h && !vr.current && A && fe.current && W.key === " " && (vr.current = !0, fe.current.stop(W, () => {
      fe.current.start(W);
    })), W.target === W.currentTarget && yr() && W.key === " " && W.preventDefault(), _ && _(W), W.target === W.currentTarget && yr() && W.key === "Enter" && !f && (W.preventDefault(), m && m(W));
  }), ht = Fr((W) => {
    h && W.key === " " && fe.current && A && !W.defaultPrevented && (vr.current = !1, fe.current.stop(W, () => {
      fe.current.pulsate(W);
    })), l && l(W), m && W.target === W.currentTarget && yr() && W.key === " " && !W.defaultPrevented && m(W);
  });
  let Qe = u;
  Qe === "button" && (se.href || se.to) && (Qe = E);
  const We = {};
  Qe === "button" ? (We.type = le === void 0 ? "button" : le, We.disabled = f) : (!se.href && !se.to && (We.role = "button"), f && (We["aria-disabled"] = f));
  const Mr = Pn(t, L, be);
  process.env.NODE_ENV !== "production" && P.useEffect(() => {
    z && !fe.current && console.error(["MUI: The `component` prop provided to ButtonBase is invalid.", "Please make sure the children prop is rendered in this custom component."].join(`
`));
  }, [z]);
  const qe = j({}, n, {
    centerRipple: i,
    component: u,
    disabled: f,
    disableRipple: d,
    disableTouchRipple: p,
    focusRipple: h,
    tabIndex: me,
    focusVisible: A
  }), yt = Pu(qe);
  return /* @__PURE__ */ Me.jsxs(ku, j({
    as: Qe,
    className: Ne(yt.root, c),
    ownerState: qe,
    onBlur: Ir,
    onClick: m,
    onContextMenu: he,
    onFocus: Ze,
    onKeyDown: gr,
    onKeyUp: ht,
    onMouseDown: U,
    onMouseLeave: $,
    onMouseUp: Ee,
    onDragLeave: v,
    onTouchEnd: De,
    onTouchMove: Ge,
    onTouchStart: Se,
    ref: Mr,
    tabIndex: f ? -1 : me,
    type: le
  }, We, se, {
    children: [a, z ? (
      /* TouchRipple is only needed client-side, x2 boost on the server. */
      /* @__PURE__ */ Me.jsx(Ru, j({
        ref: $e,
        center: i
      }, oe))
    ) : null]
  }));
});
process.env.NODE_ENV !== "production" && (Uo.propTypes = {
  // ----------------------------- Warning --------------------------------
  // | These PropTypes are generated from the TypeScript type definitions |
  // |     To update them edit the d.ts file and run "yarn proptypes"     |
  // ----------------------------------------------------------------------
  /**
   * A ref for imperative actions.
   * It currently only supports `focusVisible()` action.
   */
  action: Bi,
  /**
   * If `true`, the ripples are centered.
   * They won't start at the cursor interaction position.
   * @default false
   */
  centerRipple: b.bool,
  /**
   * The content of the component.
   */
  children: b.node,
  /**
   * Override or extend the styles applied to the component.
   */
  classes: b.object,
  /**
   * @ignore
   */
  className: b.string,
  /**
   * The component used for the root node.
   * Either a string to use a HTML element or a component.
   */
  component: Mi,
  /**
   * If `true`, the component is disabled.
   * @default false
   */
  disabled: b.bool,
  /**
   * If `true`, the ripple effect is disabled.
   *
   * ⚠️ Without a ripple there is no styling for :focus-visible by default. Be sure
   * to highlight the element by applying separate styles with the `.Mui-focusVisible` class.
   * @default false
   */
  disableRipple: b.bool,
  /**
   * If `true`, the touch ripple effect is disabled.
   * @default false
   */
  disableTouchRipple: b.bool,
  /**
   * If `true`, the base button will have a keyboard focus ripple.
   * @default false
   */
  focusRipple: b.bool,
  /**
   * This prop can help identify which element has keyboard focus.
   * The class name will be applied when the element gains the focus through keyboard interaction.
   * It's a polyfill for the [CSS :focus-visible selector](https://drafts.csswg.org/selectors-4/#the-focus-visible-pseudo).
   * The rationale for using this feature [is explained here](https://github.com/WICG/focus-visible/blob/HEAD/explainer.md).
   * A [polyfill can be used](https://github.com/WICG/focus-visible) to apply a `focus-visible` class to other components
   * if needed.
   */
  focusVisibleClassName: b.string,
  /**
   * @ignore
   */
  href: b.any,
  /**
   * The component used to render a link when the `href` prop is provided.
   * @default 'a'
   */
  LinkComponent: b.elementType,
  /**
   * @ignore
   */
  onBlur: b.func,
  /**
   * @ignore
   */
  onClick: b.func,
  /**
   * @ignore
   */
  onContextMenu: b.func,
  /**
   * @ignore
   */
  onDragLeave: b.func,
  /**
   * @ignore
   */
  onFocus: b.func,
  /**
   * Callback fired when the component is focused with a keyboard.
   * We trigger a `onFocus` callback too.
   */
  onFocusVisible: b.func,
  /**
   * @ignore
   */
  onKeyDown: b.func,
  /**
   * @ignore
   */
  onKeyUp: b.func,
  /**
   * @ignore
   */
  onMouseDown: b.func,
  /**
   * @ignore
   */
  onMouseLeave: b.func,
  /**
   * @ignore
   */
  onMouseUp: b.func,
  /**
   * @ignore
   */
  onTouchEnd: b.func,
  /**
   * @ignore
   */
  onTouchMove: b.func,
  /**
   * @ignore
   */
  onTouchStart: b.func,
  /**
   * The system prop that allows defining system overrides as well as additional CSS styles.
   */
  sx: b.oneOfType([b.arrayOf(b.oneOfType([b.func, b.object, b.bool])), b.func, b.object]),
  /**
   * @default 0
   */
  tabIndex: b.number,
  /**
   * Props applied to the `TouchRipple` element.
   */
  TouchRippleProps: b.object,
  /**
   * A ref that points to the `TouchRipple` element.
   */
  touchRippleRef: b.oneOfType([b.func, b.shape({
    current: b.shape({
      pulsate: b.func.isRequired,
      start: b.func.isRequired,
      stop: b.func.isRequired
    })
  })]),
  /**
   * @ignore
   */
  type: b.oneOfType([b.oneOf(["button", "reset", "submit"]), b.string])
});
const Au = Uo;
function Nu(e) {
  return Zr("MuiButton", e);
}
const Iu = zt("MuiButton", ["root", "text", "textInherit", "textPrimary", "textSecondary", "textSuccess", "textError", "textInfo", "textWarning", "outlined", "outlinedInherit", "outlinedPrimary", "outlinedSecondary", "outlinedSuccess", "outlinedError", "outlinedInfo", "outlinedWarning", "contained", "containedInherit", "containedPrimary", "containedSecondary", "containedSuccess", "containedError", "containedInfo", "containedWarning", "disableElevation", "focusVisible", "disabled", "colorInherit", "textSizeSmall", "textSizeMedium", "textSizeLarge", "outlinedSizeSmall", "outlinedSizeMedium", "outlinedSizeLarge", "containedSizeSmall", "containedSizeMedium", "containedSizeLarge", "sizeMedium", "sizeSmall", "sizeLarge", "fullWidth", "startIcon", "endIcon", "iconSizeSmall", "iconSizeMedium", "iconSizeLarge"]), zr = Iu, Wo = /* @__PURE__ */ P.createContext({});
process.env.NODE_ENV !== "production" && (Wo.displayName = "ButtonGroupContext");
const Mu = Wo, ju = ["children", "color", "component", "className", "disabled", "disableElevation", "disableFocusRipple", "endIcon", "focusVisibleClassName", "fullWidth", "size", "startIcon", "type", "variant"], Du = (e) => {
  const {
    color: r,
    disableElevation: t,
    fullWidth: n,
    size: o,
    variant: i,
    classes: a
  } = e, c = {
    root: ["root", i, `${i}${Oe(r)}`, `size${Oe(o)}`, `${i}Size${Oe(o)}`, r === "inherit" && "colorInherit", t && "disableElevation", n && "fullWidth"],
    label: ["label"],
    startIcon: ["startIcon", `iconSize${Oe(o)}`],
    endIcon: ["endIcon", `iconSize${Oe(o)}`]
  }, u = bo(c, Nu, a);
  return j({}, a, u);
}, qo = (e) => j({}, e.size === "small" && {
  "& > *:nth-of-type(1)": {
    fontSize: 18
  }
}, e.size === "medium" && {
  "& > *:nth-of-type(1)": {
    fontSize: 20
  }
}, e.size === "large" && {
  "& > *:nth-of-type(1)": {
    fontSize: 22
  }
}), Fu = hr(Au, {
  shouldForwardProp: (e) => zo(e) || e === "classes",
  name: "MuiButton",
  slot: "Root",
  overridesResolver: (e, r) => {
    const {
      ownerState: t
    } = e;
    return [r.root, r[t.variant], r[`${t.variant}${Oe(t.color)}`], r[`size${Oe(t.size)}`], r[`${t.variant}Size${Oe(t.size)}`], t.color === "inherit" && r.colorInherit, t.disableElevation && r.disableElevation, t.fullWidth && r.fullWidth];
  }
})(({
  theme: e,
  ownerState: r
}) => {
  var t, n;
  const o = e.palette.mode === "light" ? e.palette.grey[300] : e.palette.grey[800], i = e.palette.mode === "light" ? e.palette.grey.A100 : e.palette.grey[700];
  return j({}, e.typography.button, {
    minWidth: 64,
    padding: "6px 16px",
    borderRadius: (e.vars || e).shape.borderRadius,
    transition: e.transitions.create(["background-color", "box-shadow", "border-color", "color"], {
      duration: e.transitions.duration.short
    }),
    "&:hover": j({
      textDecoration: "none",
      backgroundColor: e.vars ? `rgba(${e.vars.palette.text.primaryChannel} / ${e.vars.palette.action.hoverOpacity})` : Lr(e.palette.text.primary, e.palette.action.hoverOpacity),
      // Reset on touch devices, it doesn't add specificity
      "@media (hover: none)": {
        backgroundColor: "transparent"
      }
    }, r.variant === "text" && r.color !== "inherit" && {
      backgroundColor: e.vars ? `rgba(${e.vars.palette[r.color].mainChannel} / ${e.vars.palette.action.hoverOpacity})` : Lr(e.palette[r.color].main, e.palette.action.hoverOpacity),
      // Reset on touch devices, it doesn't add specificity
      "@media (hover: none)": {
        backgroundColor: "transparent"
      }
    }, r.variant === "outlined" && r.color !== "inherit" && {
      border: `1px solid ${(e.vars || e).palette[r.color].main}`,
      backgroundColor: e.vars ? `rgba(${e.vars.palette[r.color].mainChannel} / ${e.vars.palette.action.hoverOpacity})` : Lr(e.palette[r.color].main, e.palette.action.hoverOpacity),
      // Reset on touch devices, it doesn't add specificity
      "@media (hover: none)": {
        backgroundColor: "transparent"
      }
    }, r.variant === "contained" && {
      backgroundColor: e.vars ? e.vars.palette.Button.inheritContainedHoverBg : i,
      boxShadow: (e.vars || e).shadows[4],
      // Reset on touch devices, it doesn't add specificity
      "@media (hover: none)": {
        boxShadow: (e.vars || e).shadows[2],
        backgroundColor: (e.vars || e).palette.grey[300]
      }
    }, r.variant === "contained" && r.color !== "inherit" && {
      backgroundColor: (e.vars || e).palette[r.color].dark,
      // Reset on touch devices, it doesn't add specificity
      "@media (hover: none)": {
        backgroundColor: (e.vars || e).palette[r.color].main
      }
    }),
    "&:active": j({}, r.variant === "contained" && {
      boxShadow: (e.vars || e).shadows[8]
    }),
    [`&.${zr.focusVisible}`]: j({}, r.variant === "contained" && {
      boxShadow: (e.vars || e).shadows[6]
    }),
    [`&.${zr.disabled}`]: j({
      color: (e.vars || e).palette.action.disabled
    }, r.variant === "outlined" && {
      border: `1px solid ${(e.vars || e).palette.action.disabledBackground}`
    }, r.variant === "contained" && {
      color: (e.vars || e).palette.action.disabled,
      boxShadow: (e.vars || e).shadows[0],
      backgroundColor: (e.vars || e).palette.action.disabledBackground
    })
  }, r.variant === "text" && {
    padding: "6px 8px"
  }, r.variant === "text" && r.color !== "inherit" && {
    color: (e.vars || e).palette[r.color].main
  }, r.variant === "outlined" && {
    padding: "5px 15px",
    border: "1px solid currentColor"
  }, r.variant === "outlined" && r.color !== "inherit" && {
    color: (e.vars || e).palette[r.color].main,
    border: e.vars ? `1px solid rgba(${e.vars.palette[r.color].mainChannel} / 0.5)` : `1px solid ${Lr(e.palette[r.color].main, 0.5)}`
  }, r.variant === "contained" && {
    color: e.vars ? (
      // this is safe because grey does not change between default light/dark mode
      e.vars.palette.text.primary
    ) : (t = (n = e.palette).getContrastText) == null ? void 0 : t.call(n, e.palette.grey[300]),
    backgroundColor: e.vars ? e.vars.palette.Button.inheritContainedBg : o,
    boxShadow: (e.vars || e).shadows[2]
  }, r.variant === "contained" && r.color !== "inherit" && {
    color: (e.vars || e).palette[r.color].contrastText,
    backgroundColor: (e.vars || e).palette[r.color].main
  }, r.color === "inherit" && {
    color: "inherit",
    borderColor: "currentColor"
  }, r.size === "small" && r.variant === "text" && {
    padding: "4px 5px",
    fontSize: e.typography.pxToRem(13)
  }, r.size === "large" && r.variant === "text" && {
    padding: "8px 11px",
    fontSize: e.typography.pxToRem(15)
  }, r.size === "small" && r.variant === "outlined" && {
    padding: "3px 9px",
    fontSize: e.typography.pxToRem(13)
  }, r.size === "large" && r.variant === "outlined" && {
    padding: "7px 21px",
    fontSize: e.typography.pxToRem(15)
  }, r.size === "small" && r.variant === "contained" && {
    padding: "4px 10px",
    fontSize: e.typography.pxToRem(13)
  }, r.size === "large" && r.variant === "contained" && {
    padding: "8px 22px",
    fontSize: e.typography.pxToRem(15)
  }, r.fullWidth && {
    width: "100%"
  });
}, ({
  ownerState: e
}) => e.disableElevation && {
  boxShadow: "none",
  "&:hover": {
    boxShadow: "none"
  },
  [`&.${zr.focusVisible}`]: {
    boxShadow: "none"
  },
  "&:active": {
    boxShadow: "none"
  },
  [`&.${zr.disabled}`]: {
    boxShadow: "none"
  }
}), Vu = hr("span", {
  name: "MuiButton",
  slot: "StartIcon",
  overridesResolver: (e, r) => {
    const {
      ownerState: t
    } = e;
    return [r.startIcon, r[`iconSize${Oe(t.size)}`]];
  }
})(({
  ownerState: e
}) => j({
  display: "inherit",
  marginRight: 8,
  marginLeft: -4
}, e.size === "small" && {
  marginLeft: -2
}, qo(e))), Lu = hr("span", {
  name: "MuiButton",
  slot: "EndIcon",
  overridesResolver: (e, r) => {
    const {
      ownerState: t
    } = e;
    return [r.endIcon, r[`iconSize${Oe(t.size)}`]];
  }
})(({
  ownerState: e
}) => j({
  display: "inherit",
  marginRight: -4,
  marginLeft: 8
}, e.size === "small" && {
  marginRight: -2
}, qo(e))), Ko = /* @__PURE__ */ P.forwardRef(function(r, t) {
  const n = P.useContext(Mu), o = Lt(n, r), i = nn({
    props: o,
    name: "MuiButton"
  }), {
    children: a,
    color: c = "primary",
    component: u = "button",
    className: f,
    disabled: d = !1,
    disableElevation: p = !1,
    disableFocusRipple: h = !1,
    endIcon: E,
    focusVisibleClassName: g,
    fullWidth: m = !1,
    size: y = "medium",
    startIcon: S,
    type: w,
    variant: C = "text"
  } = i, _ = je(i, ju), l = j({}, i, {
    color: c,
    component: u,
    disabled: d,
    disableElevation: p,
    disableFocusRipple: h,
    fullWidth: m,
    size: y,
    type: w,
    variant: C
  }), k = Du(l), N = S && /* @__PURE__ */ Me.jsx(Vu, {
    className: k.startIcon,
    ownerState: l,
    children: S
  }), pe = E && /* @__PURE__ */ Me.jsx(Lu, {
    className: k.endIcon,
    ownerState: l,
    children: E
  });
  return /* @__PURE__ */ Me.jsxs(Fu, j({
    ownerState: l,
    className: Ne(n.className, k.root, f),
    component: u,
    disabled: d,
    focusRipple: !h,
    focusVisibleClassName: Ne(k.focusVisible, g),
    ref: t,
    type: w
  }, _, {
    classes: k,
    children: [N, a, pe]
  }));
});
process.env.NODE_ENV !== "production" && (Ko.propTypes = {
  // ----------------------------- Warning --------------------------------
  // | These PropTypes are generated from the TypeScript type definitions |
  // |     To update them edit the d.ts file and run "yarn proptypes"     |
  // ----------------------------------------------------------------------
  /**
   * The content of the component.
   */
  children: b.node,
  /**
   * Override or extend the styles applied to the component.
   */
  classes: b.object,
  /**
   * @ignore
   */
  className: b.string,
  /**
   * The color of the component.
   * It supports both default and custom theme colors, which can be added as shown in the
   * [palette customization guide](https://mui.com/material-ui/customization/palette/#adding-new-colors).
   * @default 'primary'
   */
  color: b.oneOfType([b.oneOf(["inherit", "primary", "secondary", "success", "error", "info", "warning"]), b.string]),
  /**
   * The component used for the root node.
   * Either a string to use a HTML element or a component.
   */
  component: b.elementType,
  /**
   * If `true`, the component is disabled.
   * @default false
   */
  disabled: b.bool,
  /**
   * If `true`, no elevation is used.
   * @default false
   */
  disableElevation: b.bool,
  /**
   * If `true`, the  keyboard focus ripple is disabled.
   * @default false
   */
  disableFocusRipple: b.bool,
  /**
   * If `true`, the ripple effect is disabled.
   *
   * ⚠️ Without a ripple there is no styling for :focus-visible by default. Be sure
   * to highlight the element by applying separate styles with the `.Mui-focusVisible` class.
   * @default false
   */
  disableRipple: b.bool,
  /**
   * Element placed after the children.
   */
  endIcon: b.node,
  /**
   * @ignore
   */
  focusVisibleClassName: b.string,
  /**
   * If `true`, the button will take up the full width of its container.
   * @default false
   */
  fullWidth: b.bool,
  /**
   * The URL to link to when the button is clicked.
   * If defined, an `a` element will be used as the root node.
   */
  href: b.string,
  /**
   * The size of the component.
   * `small` is equivalent to the dense button styling.
   * @default 'medium'
   */
  size: b.oneOfType([b.oneOf(["small", "medium", "large"]), b.string]),
  /**
   * Element placed before the children.
   */
  startIcon: b.node,
  /**
   * The system prop that allows defining system overrides as well as additional CSS styles.
   */
  sx: b.oneOfType([b.arrayOf(b.oneOfType([b.func, b.object, b.bool])), b.func, b.object]),
  /**
   * @ignore
   */
  type: b.oneOfType([b.oneOf(["button", "reset", "submit"]), b.string]),
  /**
   * The variant to use.
   * @default 'text'
   */
  variant: b.oneOfType([b.oneOf(["contained", "outlined", "text"]), b.string])
});
const zu = Ko, Uu = (e) => /* @__PURE__ */ Me.jsx(zu, { onClick: e.onClick, children: e.caption });
export {
  Uu as SimpleButton,
  Yu as SimpleSpan
};
