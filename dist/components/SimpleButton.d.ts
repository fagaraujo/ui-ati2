export interface SimpleButtonProps {
    caption: string;
    onClick?: () => void;
}
export declare const SimpleButton: (props: SimpleButtonProps) => import("react/jsx-runtime").JSX.Element;
