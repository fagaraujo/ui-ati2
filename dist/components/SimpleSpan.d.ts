export interface SimpleSpanProps {
    text: string;
}
export declare const SimpleSpan: (props: SimpleSpanProps) => import("react/jsx-runtime").JSX.Element;
